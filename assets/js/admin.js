// add new role 
var addRoleObj = {};
$('#privileges').multiselect({
	includeSelectAllOption : true
});

$(document).on(
		'click',
		"#addRole",
		function(e) {

			addRoleObj.title = $.trim($("#title").val());
			addRoleObj.description = $.trim($("#description").val());
			addRoleObj.status = $.trim($(".status:checked").val());
			var message = [];
			var selected = $("#privileges option:selected");
			selected.each(function() {
				message.push($(this).val());

			});
			addRoleObj.privilege = message;
			$.ajax({
				type : "POST",
				url : base_url + 'admin/new_role',
				data : {
					user : addRoleObj
				},
				dataType : "json",
				success : function(data) {
					if (data.status == 1) {
						$(".search-error").html(
								"<span style='color:green'>" + data.msg
										+ "</span>");
						window.location.href = base_url + 'admin/view_roles';
					} else if (data.status == 0) {
						$(".search-error").html(
								"<span style='color:red'>" + data.msg
										+ "</span>");

					} else if (data.status == 2) {
						$(".search-error").html(
								"<span style='color:red'>" + data.msg
										+ "</span>");
					}
				},
				error : function(e) {
					console.log(e)
					$('#loader').hide();
				},
				complete : function() {
					$('#loader').hide();
				}
			});

		});
// add new privilege
var addprivilegeObj = {};
$(document).on(
		'click',
		"#addPrivilege",
		function(e) {
			addprivilegeObj.title = $.trim($("#title").val());
			addprivilegeObj.description = $.trim($("#description").val());
			addprivilegeObj.status = $.trim($(".status:checked").val());
			$.ajax({
				type : "POST",
				url : base_url + 'admin/admin/new_privilege',
				beforeSend : function() {
					$('#loader').show();
				},
				data : {
					user : addprivilegeObj
				},
				dataType : "json",
				success : function(data) {

					if (data.status == 1) {
						$(".search-error").html(
								"<span style='color:green'>" + data.msg
										+ "</span>");
						window.location.href = base_url
								+ 'admin/admin/view_privileges';

					} else if (data.status == 0) {
						$(".search-error").html(
								"<span style='color:red'>" + data.msg
										+ "</span>");

					} else if (data.status == 2) {
						$(".search-error").html(
								"<span style='color:red'>" + data.msg
										+ "</span>");
					}
				},
				error : function(e) {
					$('#loader').hide();
				},
				complete : function() {
					$('#loader').hide();
				}
			});

		});

// upload and view image in gallery
$(document)
		.on(
				'click',
				"#submit",
				function(e) {
					e.preventDefault();
					var description = $("#description").val();
					$
							.ajaxFileUpload({
								url : base_url + 'admin/admin/upload_image',
								secureuri : false,
								fileElementId : 'fileToUpload',
								dataType : 'json',
								data : {
									description : description
								},
								success : function(data) {
									if (data.status == 1) {
										if (!confirm("Image uploaded successfully. Do you want to upload another image?")) {
											$('#myModal').modal("hide");
											$(".search-error").html(
													"<span style='color:green'>"
															+ data.msg
															+ "</span>");
											e.preventDefault();
											window.location.href = base_url
													+ 'admin/admin/view_image';
										}
									} else if (data.status == 0) {
										$(".search-error").html(
												"<span style='color:red'>"
														+ data.msg + "</span>");
									} else if (data.status == 2) {
										$(".search-error").html(
												"<span style='color:red'>"
														+ data.msg + "</span>");

									}
								}
							});
				});
$('#dob').daterangepicker({
	locale: {
        format: 'YYYY-MM-DD'
		
    },
	  singleDatePicker: true,
	  calender_style: "picker_4",
	  showDropdowns: true
    }, function(start, end, label) {
      console.log(start.toISOString(), end.toISOString(), label);
    });

/*$('form').submit(function(e) {
    
    var submit = true;

    // evaluate the form using generic validaing
    if (!validator.checkAll($(this))) {
	e.preventDefault();
      submit = false;
	  return;
    }   
});*/