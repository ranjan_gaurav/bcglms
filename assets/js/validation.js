 var validator = new FormValidator();
      //validator.message.date = 'not a real date';

      // validate a field on "blur" event, a 'select' on 'change' event & a '.reuired' classed multifield on 'keyup':
      $('form')
        .on('blur', 'input[required], input.optional, select.required', validator.checkField)
        .on('change', 'select.required', validator.checkField)
        .on('keypress', 'input[required][pattern]', validator.keypress);

      $('.multi.required').on('keyup blur', 'input', function() {
        validator.checkField.apply($(this).siblings().last()[0]);
      });

      $('form').submit(function(e) {
        e.preventDefault();
        var submit = true;

        // evaluate the form using generic validaing
        if (!validator.checkAll($(this))) {
          submit = false;
        }
        if (submit)
		var Obj = {};
        newUserObj.firstname = $.trim($("#firstname").val());
		newUserObj.lastname = $.trim($("#lastname").val());
		newUserObj.email = $.trim($("#email").val());
		newUserObj.password = $.trim($("#password").val());
		newUserObj.gender = $.trim($(".gender:checked").val());
		newUserObj.role_id = $.trim($(".role_Id:checked").val());
		newUserObj.status = $.trim($(".status:checked").val());
		newUserObj.dob = $.trim($("#dob").val());
		newUserObj.primaryContact = $.trim($("#primaryContact").val());
		newUserObj.address = $.trim($("#address").val());
		newUserObj.pincode = $.trim($("#pincode").val());
		newUserObj.city = $.trim($("#city").val());
		newUserObj.state = $.trim($("#state").val());
		newUserObj.country = $.trim($("#city").val());
		
		/*rel.firstname = $("#parentname").val();
		rel.contact = $("#telephone").val();
		rel.relation_id = $("#relation").val();
		  if(Obj.role_id == 4)
		  {
			  var URL = base_url+'admin/Add_student';
		  }
		  
		   if(Obj.role_id == 3)
		  {
			  var URL = base_url+'admin/Add_teacher';
		  }
		  */
		  
			$.ajax({
			  type: "POST",
			  url: base_url + 'admin/admin/new_user',
			  beforeSend : function(){
				  $('#loader').show();
			  },
			  data: {user: Obj},
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				  if(data.status == 1){
					  $.ajaxFileUpload({
							url : base_url + 'admin/admin/picture',
							secureuri : false,
							fileElementId : 'fileToUpload',
							dataType : 'JSON',
							success : function(data) 
							{
								
							}
						});	  
			  $('#search-error').html("<span style='color:green'>" +data.msg+"</span>");
			  $('form')[0].reset();
			  
		  }
		  else if(data.status == 0){
			  $('#search-error').html("<span style='color:red'>" +data.msg+"</span>");
			   $('form')[0].reset();
			  
		  }
		  else{
			  
			  $('#search-error').html("<span style='color:red'>Server error.</span>");
			  //$this.button('reset');
		  }
			  },
			  error: function(e){
				  console.log(e)
				  $('#loader').hide();
			  },
			  complete: function(){
				  $('#loader').hide();
			  }
			});
		

        return false;
      });