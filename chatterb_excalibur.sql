-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 05, 2017 at 09:23 AM
-- Server version: 5.6.32-78.1
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `chatterb_excalibur`
--

-- --------------------------------------------------------

--
-- Table structure for table `category_products`
--

CREATE TABLE IF NOT EXISTS `category_products` (
  `id` int(11) NOT NULL,
  `product` varchar(150) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `status` int(2) NOT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category_products`
--

INSERT INTO `category_products` (`id`, `product`, `description`, `category_id`, `status`, `modificationDate`, `creationDate`) VALUES
(1, 'CASA_1', 'CASA_1', 1, 1, '2016-11-12 15:56:19', '2016-11-12 10:26:19'),
(2, 'CASA_2', 'CASA_2', 1, 1, '2016-11-15 00:00:00', '2016-11-14 18:30:00'),
(3, 'CASA_3', 'CASA_3', 1, 1, '2016-11-15 00:00:00', '2016-11-14 18:30:00'),
(4, 'CASA_4', 'CASA_4', 1, 1, '2016-11-15 11:47:51', '2016-11-15 06:17:51'),
(5, 'Retail Assets_1', 'Retail Assets', 2, 1, '2016-11-15 12:15:21', '2016-11-15 06:45:21'),
(6, 'Retail Assets_2', 'Retail Assets', 2, 1, '2016-11-15 12:12:20', '2016-11-15 06:42:20'),
(7, 'Retail Assets_3', 'Retail Assets', 2, 1, '2016-11-15 12:18:11', '2016-11-15 06:48:11'),
(8, 'Retail Assets_4', 'Retail Assets', 2, 1, '2016-11-15 12:18:28', '2016-11-15 06:48:28'),
(9, 'MSME Advances_1', 'MSME Advances', 3, 1, '2016-11-15 12:18:40', '2016-11-15 06:48:40'),
(10, 'MSME Advances_2', 'MSME Advances', 3, 1, '2016-11-15 12:27:21', '2016-11-15 06:57:21'),
(11, 'MSME Advances_3', 'MSME Advances', 3, 1, '2016-11-15 12:19:24', '2016-11-15 06:49:24'),
(12, 'MSME Advances_4', 'MSME Advances', 3, 1, '2016-11-15 12:19:24', '2016-11-15 06:49:24'),
(13, 'TPP_1', 'TPP', 4, 1, '2016-11-15 12:22:17', '2016-11-15 06:52:17'),
(14, 'TPP_2', 'TPP', 4, 1, '2016-11-15 12:23:12', '2016-11-15 06:53:12'),
(15, 'TPP_3', 'TPP', 4, 1, '2016-11-15 12:23:13', '2016-11-15 06:53:13'),
(16, 'TPP_4', 'TPP', 4, 1, '2016-11-15 12:23:16', '2016-11-15 06:53:16');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `device_info`
--

CREATE TABLE IF NOT EXISTS `device_info` (
  `deviceId` varchar(250) NOT NULL,
  `deviceType` varchar(150) DEFAULT NULL,
  `fcmId` varchar(250) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `latitude` varchar(250) DEFAULT NULL,
  `longitude` varchar(250) DEFAULT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `device_info`
--

INSERT INTO `device_info` (`deviceId`, `deviceType`, `fcmId`, `user_id`, `latitude`, `longitude`, `modificationDate`, `creationDate`) VALUES
('121212', 'ios', '21212', 2, NULL, NULL, '2017-03-29 16:07:36', '2017-03-29 08:08:58'),
('123', 'ios', NULL, 2, NULL, NULL, '2017-03-31 12:51:06', '2017-03-29 10:43:33'),
('123abc123', 'Android', 'asdasdasdasd', 2, NULL, NULL, NULL, '2016-11-10 10:53:45'),
('123abc1234', 'Android', 'aaaaaaaa', 1, NULL, NULL, '2016-11-12 15:01:24', '2016-11-10 10:54:03'),
('2332424234234', 'ios', NULL, 2, NULL, NULL, '2017-04-05 09:13:57', '2017-03-31 06:55:50'),
('beab68c59e2c30d1', 'android', 'eetet', 309, NULL, NULL, '2016-11-26 12:59:05', '2016-11-26 07:27:27'),
('efwe', 'android', 'fef', 1, NULL, NULL, '2017-03-28 16:01:07', '2017-03-28 10:06:56'),
('efwe765768', 'android', 'efwe765768', 2, NULL, NULL, '2017-04-05 08:30:56', '2017-03-28 10:37:49'),
('gjkdojghikdsrjghksrg', 'android', 'ddd', 306, NULL, NULL, NULL, '2016-11-26 07:26:20');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE IF NOT EXISTS `gallery` (
  `id` int(11) NOT NULL,
  `picture` varchar(100) NOT NULL,
  `description` text,
  `added_by` int(11) NOT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gallery`
--

INSERT INTO `gallery` (`id`, `picture`, `description`, `added_by`, `modificationDate`, `creationDate`) VALUES
(108, 'Tulips.jpg', 'jgfuuy', 305, NULL, '2016-12-10 08:43:40'),
(109, 'Hydrangeas.jpg', 'dfsd', 305, NULL, '2016-12-12 06:20:40'),
(110, 'Chrysanthemum.jpg', 'rose', 305, NULL, '2016-12-27 10:26:22'),
(111, 'Lighthouse.jpg', 'guyhfgu', 305, NULL, '2016-12-29 07:37:19'),
(112, 'Koala.jpg', 'oh9', 305, NULL, '2016-12-29 07:37:53'),
(113, 'Penguins.jpg', 'tuy7', 305, NULL, '2016-12-29 07:38:06'),
(114, 'river.jpg', '', 305, NULL, '2016-12-29 07:38:43'),
(115, 'nasa.jpg', 'zsdfasfgv', 305, NULL, '2016-12-29 09:57:49'),
(116, 'download_(4).jpg', 'dgry', 305, NULL, '2017-01-04 13:17:25'),
(117, 'Tulips1.jpg', 'try6', 305, NULL, '2017-01-04 13:20:18'),
(118, 'Jellyfish.jpg', 'eter', 305, NULL, '2017-01-04 13:39:21'),
(119, '178440.jpg', 'ni', 305, NULL, '2017-02-22 10:02:27');

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE IF NOT EXISTS `keys` (
  `id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) DEFAULT NULL,
  `ignore_limits` tinyint(1) DEFAULT NULL,
  `date_created` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `key`, `level`, `ignore_limits`, `date_created`) VALUES
(1, 'edq7A6x99YfceKFWS7Sw2Yi1f5DMy9n6', NULL, NULL, '2016-11-12');

-- --------------------------------------------------------

--
-- Table structure for table `leads`
--

CREATE TABLE IF NOT EXISTS `leads` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `district` varchar(255) NOT NULL,
  `branch` varchar(255) NOT NULL,
  `sourcedBy` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `emailId` varchar(255) NOT NULL,
  `isExistingCustomer` int(1) NOT NULL,
  `leadValue` bigint(255) NOT NULL,
  `cutomerId` varchar(255) DEFAULT NULL,
  `businessCard` varchar(255) DEFAULT NULL,
  `Note` varchar(250) NOT NULL,
  `added_by` int(11) NOT NULL,
  `follower_id` int(11) DEFAULT NULL,
  `status` int(2) NOT NULL,
  `target_month` int(11) DEFAULT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leads`
--

INSERT INTO `leads` (`id`, `name`, `mobile`, `category_id`, `product_id`, `district`, `branch`, `sourcedBy`, `address`, `emailId`, `isExistingCustomer`, `leadValue`, `cutomerId`, `businessCard`, `Note`, `added_by`, `follower_id`, `status`, `target_month`, `modificationDate`, `creationDate`) VALUES
(1, 'test', '1234567890', 4, 16, '4', '6', '1', 'dhhddj', 'abc@abc.com', 0, 5000, '', NULL, '', 1, NULL, 3, 4, NULL, '2017-04-05 13:53:22'),
(2, '2ndtest', '9638227110', 2, 6, '1', '2', '1', 'fdhj', 'ttt@ttt.com', 0, 5000, '', NULL, '', 1, NULL, 3, 4, NULL, '2017-04-05 13:59:36'),
(3, 'vikas', '9015448978', 1, 1, '1', '2', 'abc', 'vikas', 'vikbhatt6689@gmail.com', 0, 0, '', NULL, '', 1, NULL, 3, 4, NULL, '2017-04-05 14:14:50');

-- --------------------------------------------------------

--
-- Table structure for table `meetings`
--

CREATE TABLE IF NOT EXISTS `meetings` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `meetingTime` datetime NOT NULL,
  `description` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `status` int(2) NOT NULL,
  `close_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `latitude` varchar(250) DEFAULT NULL,
  `longitude` varchar(250) DEFAULT NULL,
  `address` varchar(1000) DEFAULT NULL,
  `Meeting_action_point` varchar(250) NOT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `meetings`
--

INSERT INTO `meetings` (`id`, `title`, `meetingTime`, `description`, `user_id`, `lead_id`, `status`, `close_time`, `latitude`, `longitude`, `address`, `Meeting_action_point`, `modificationDate`, `creationDate`) VALUES
(1, 'hshshs', '2017-04-05 19:43:43', 'hshshs', 1, 2, 7, '2017-04-05 14:14:19', NULL, NULL, NULL, '', NULL, '2017-04-05 14:14:19');

-- --------------------------------------------------------

--
-- Table structure for table `m_branch`
--

CREATE TABLE IF NOT EXISTS `m_branch` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `district_id` int(11) DEFAULT NULL,
  `status` int(2) NOT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_branch`
--

INSERT INTO `m_branch` (`id`, `title`, `district_id`, `status`, `modificationDate`, `creationDate`) VALUES
(2, 'Main Road', 1, 1, NULL, '2016-11-12 08:47:12'),
(3, 'Station Raod', 2, 1, NULL, '2016-11-12 08:47:40'),
(5, 'Main branch', 3, 1, NULL, '2017-04-03 14:04:54'),
(6, 'extension branch', 4, 1, NULL, '2017-04-03 14:05:12');

-- --------------------------------------------------------

--
-- Table structure for table `m_category`
--

CREATE TABLE IF NOT EXISTS `m_category` (
  `id` int(11) NOT NULL,
  `category` varchar(150) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_category`
--

INSERT INTO `m_category` (`id`, `category`, `description`, `status`, `modificationDate`, `creationDate`) VALUES
(1, 'Agricultural loans', 'Agricultural loans', 1, NULL, '2016-11-12 10:25:45'),
(2, 'Current account', 'Current account', 1, NULL, '2016-11-15 06:41:44'),
(3, 'Deposites', 'Deposites', 1, NULL, '2016-11-14 18:30:00'),
(4, 'TPP', 'TPP', 1, NULL, '2016-11-15 06:43:20');

-- --------------------------------------------------------

--
-- Table structure for table `m_district`
--

CREATE TABLE IF NOT EXISTS `m_district` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `status` int(2) NOT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_district`
--

INSERT INTO `m_district` (`id`, `title`, `status`, `modificationDate`, `creationDate`) VALUES
(1, 'Bhojpur', 1, NULL, '2016-11-12 08:47:12'),
(2, 'Rohtas', 1, NULL, '2016-11-12 08:47:40'),
(3, 'Kaimoor', 1, NULL, '2017-03-28 11:32:10'),
(4, 'Patna', 1, NULL, '2017-03-28 11:32:21');

-- --------------------------------------------------------

--
-- Table structure for table `m_privileges`
--

CREATE TABLE IF NOT EXISTS `m_privileges` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_privileges`
--

INSERT INTO `m_privileges` (`id`, `title`, `description`, `status`, `creationDate`) VALUES
(1, 'all', 'all', 1, '2016-11-10 10:00:34'),
(2, 'add', 'add', 1, '2016-11-10 10:00:34'),
(3, 'edit', 'edit', 1, '2016-11-10 10:00:51'),
(4, 'update', 'update', 1, '2016-11-10 10:00:51'),
(5, 'delete', 'delete', 1, '2016-11-10 10:01:13'),
(6, 'view', 'view', 1, '2016-11-10 10:01:13');

-- --------------------------------------------------------

--
-- Table structure for table `m_roles`
--

CREATE TABLE IF NOT EXISTS `m_roles` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_roles`
--

INSERT INTO `m_roles` (`id`, `parent_id`, `title`, `description`, `status`, `creationDate`) VALUES
(1, 0, 'admin', 'admin', 1, '2016-11-10 09:56:28'),
(2, 1, 'RO-Manager ', 'RO-Manager ', 1, '2016-11-10 09:56:28'),
(3, 2, 'Branch-Manager', 'Branch-Manager', 1, '2016-11-14 13:34:20'),
(4, 3, 'RO-Teller', 'RO-Teller', 1, '2016-11-14 13:34:20'),
(5, 4, 'SO-Retail', 'SO-Retail', 1, '2017-03-28 09:55:17');

-- --------------------------------------------------------

--
-- Table structure for table `m_status`
--

CREATE TABLE IF NOT EXISTS `m_status` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_status`
--

INSERT INTO `m_status` (`id`, `title`, `description`, `creationDate`) VALUES
(1, 'active', 'active', '2016-11-10 09:57:01'),
(2, 'inactive', 'inactive', '2016-11-10 09:57:01'),
(3, 'New', 'New', '2016-11-25 10:56:29'),
(4, 'Contacted', 'Contacted', '2016-11-25 10:56:29'),
(5, 'In progress', 'In progress', '2016-11-25 10:56:32'),
(6, 'Pending for approval', 'Pending for approval', '2016-11-25 10:56:32'),
(7, 'Converted', 'Converted', '2016-11-25 11:43:23'),
(8, 'Closed', 'Closed', '2016-11-25 11:44:17');

-- --------------------------------------------------------

--
-- Table structure for table `status_log`
--

CREATE TABLE IF NOT EXISTS `status_log` (
  `id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `note` varchar(1000) NOT NULL,
  `changed_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_log`
--

INSERT INTO `status_log` (`id`, `lead_id`, `status`, `note`, `changed_by`, `created_date`) VALUES
(1, 42, 3, 'niotyefdytwefdywefdyw', 2, '2017-03-31 11:25:10'),
(3, 42, 5, 'sample note', 2, '2017-03-31 11:28:28'),
(4, 41, 3, 'sample note', 2, '2017-03-31 11:58:59'),
(5, 4, 6, 'djdjshjs', 1, '2017-03-31 18:59:49'),
(6, 7, 4, 'new', 1, '2017-04-03 10:38:29'),
(7, 6, 3, 'we er', 1, '2017-04-03 10:44:44'),
(8, 20, 4, 'h', 2, '2017-04-03 10:48:12'),
(9, 20, 5, 'gfi', 2, '2017-04-03 10:50:24'),
(10, 20, 5, 'madhu', 2, '2017-04-03 10:52:04'),
(11, 41, 3, 'sample note', 2, '2017-04-04 10:47:02'),
(12, 41, 3, 'sample note', 2, '2017-04-04 10:47:39'),
(13, 41, 3, 'sample note', 2, '2017-04-04 12:42:51'),
(14, 4, 7, 'fse', 2, '2017-04-04 15:33:25'),
(15, 4, 7, 'duplicate', 2, '2017-04-04 15:36:04'),
(16, 0, 3, 'test', 2, '2017-04-04 16:13:03'),
(17, 0, 3, 'test', 2, '2017-04-04 16:13:32'),
(18, 0, 5, 'test', 2, '2017-04-04 16:13:41'),
(19, 0, 5, 'test', 2, '2017-04-04 16:15:22'),
(20, 0, 3, 'test', 2, '2017-04-04 16:23:25'),
(21, 0, 3, 'test', 2, '2017-04-04 16:23:43'),
(22, 0, 5, 'test', 2, '2017-04-04 16:25:34'),
(23, 0, 3, 'test', 2, '2017-04-04 16:26:24'),
(24, 0, 3, 'test', 2, '2017-04-04 16:27:17'),
(25, 0, 3, 'test', 2, '2017-04-04 16:30:48'),
(26, 4, 5, 'yyy', 2, '2017-04-04 18:17:56'),
(27, 18, 5, 'fff', 2, '2017-04-04 18:19:47'),
(28, 4, 5, 'Iyes', 2, '2017-04-04 18:35:43'),
(29, 4, 3, 'vvvv', 2, '2017-04-04 19:00:25'),
(30, 4, 3, 'test', 2, '2017-04-05 11:12:10'),
(31, 18, 3, 'test', 2, '2017-04-05 11:12:32'),
(32, 1, 3, 'test', 1, '2017-04-05 08:34:11'),
(33, 1, 6, 'test', 1, '2017-04-05 08:34:49'),
(34, 1, 8, 'test', 1, '2017-04-05 08:35:08');

-- --------------------------------------------------------

--
-- Table structure for table `transfer_log`
--

CREATE TABLE IF NOT EXISTS `transfer_log` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transfer_log`
--

INSERT INTO `transfer_log` (`id`, `sender_id`, `receiver_id`, `lead_id`, `created_date`) VALUES
(5, 2, 1, 42, '2017-03-31 12:48:19'),
(6, 1, 2, 42, '2017-03-31 12:50:21'),
(7, 1, 2, 42, '2017-03-31 15:10:52'),
(8, 1, 2, 42, '2017-03-31 15:12:54'),
(9, 2, 1, 4, '2017-03-31 15:36:13'),
(10, 2, 1, 18, '2017-03-31 15:41:37'),
(11, 2, 310, 18, '2017-03-31 15:41:58'),
(12, 1, 2, 7, '2017-04-03 10:38:55'),
(13, 2, 309, 5, '2017-04-03 10:47:42'),
(14, 1, 2, 42, '2017-04-03 14:43:17'),
(15, 1, 2, 42, '2017-04-03 14:43:31'),
(16, 1, 2, 42, '2017-04-03 14:43:53'),
(17, 1, 2, 42, '2017-04-03 14:44:31'),
(18, 1, 2, 42, '2017-04-03 14:51:06'),
(19, 1, 2, 42, '2017-04-03 14:51:18'),
(20, 1, 2, 42, '2017-04-04 15:38:33'),
(21, 1, 2, 42, '2017-04-04 15:42:10'),
(22, 1, 2, 42, '2017-04-04 15:58:28'),
(23, 2, 310, 0, '2017-04-04 18:11:58'),
(24, 2, 310, 4, '2017-04-04 18:16:38'),
(25, 2, 310, 18, '2017-04-04 18:19:29'),
(26, 2, 310, 4, '2017-04-04 18:35:33'),
(27, 2, 1, 4, '2017-04-04 18:57:06'),
(28, 2, 310, 4, '2017-04-04 19:00:34'),
(29, 1, 1, 1, '2017-04-05 08:35:54');

-- --------------------------------------------------------

--
-- Table structure for table `trans_role_privileges`
--

CREATE TABLE IF NOT EXISTS `trans_role_privileges` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `privilege_id` int(11) NOT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=303 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_role_privileges`
--

INSERT INTO `trans_role_privileges` (`id`, `role_id`, `privilege_id`, `modificationDate`, `creationDate`) VALUES
(54, 2, 2, NULL, '2017-01-04 05:19:41'),
(55, 2, 3, NULL, '2017-01-04 05:19:41'),
(56, 2, 4, NULL, '2017-01-04 05:19:41'),
(57, 2, 5, NULL, '2017-01-04 05:19:41'),
(109, 3, 1, NULL, '2017-01-04 05:29:02'),
(110, 3, 2, NULL, '2017-01-04 05:29:02'),
(111, 3, 4, NULL, '2017-01-04 05:29:02'),
(153, 4, 3, NULL, '2017-01-04 05:44:32'),
(154, 4, 5, NULL, '2017-01-04 05:44:32'),
(155, 4, 6, NULL, '2017-01-04 05:44:32'),
(297, 1, 1, NULL, '2017-01-05 05:38:36'),
(298, 1, 2, NULL, '2017-01-05 05:38:36'),
(299, 1, 3, NULL, '2017-01-05 05:38:36'),
(300, 1, 4, NULL, '2017-01-05 05:38:36'),
(301, 1, 5, NULL, '2017-01-05 05:38:36'),
(302, 1, 6, NULL, '2017-01-05 05:38:36');

-- --------------------------------------------------------

--
-- Table structure for table `trans_user_category`
--

CREATE TABLE IF NOT EXISTS `trans_user_category` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `leadsTarget` varchar(255) NOT NULL,
  `leadsValue` varchar(255) NOT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trans_user_manager`
--

CREATE TABLE IF NOT EXISTS `trans_user_manager` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `manager_id` int(11) DEFAULT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trans_user_manager`
--

INSERT INTO `trans_user_manager` (`id`, `user_id`, `manager_id`, `modificationDate`, `creationDate`) VALUES
(4, 310, NULL, NULL, '2017-04-05 09:08:34'),
(5, 309, 310, NULL, '2017-04-05 09:09:03'),
(6, 311, 309, NULL, '2017-04-05 09:10:10'),
(7, 1, 311, NULL, '2017-04-05 09:10:54'),
(8, 2, 311, NULL, '2017-04-05 09:10:54');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `firstname` varchar(150) DEFAULT NULL,
  `lastname` varchar(150) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `gender` enum('Male','Female') DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `password` varchar(100) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `primaryContact` varchar(20) DEFAULT NULL,
  `secondaryContact` varchar(20) DEFAULT NULL,
  `address` text,
  `pincode` int(6) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(50) DEFAULT NULL,
  `summary` text,
  `picture` varchar(255) DEFAULT NULL,
  `activationKey` varchar(10) DEFAULT NULL,
  `region` int(11) NOT NULL,
  `branch` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `status` int(2) NOT NULL,
  `modificationDate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=312 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `firstname`, `lastname`, `email`, `gender`, `dob`, `password`, `username`, `primaryContact`, `secondaryContact`, `address`, `pincode`, `city`, `state`, `country`, `summary`, `picture`, `activationKey`, `region`, `branch`, `role_id`, `status`, `modificationDate`, `creationDate`) VALUES
(1, 'Siddharth', 'Gopal', 'so1@excalibur.com', 'Male', '1998-04-17', '21232f297a57a5a743894a0e4a801fc3', 'Siddharth', '9988776656', '9716256074', 'new delhi', 122001, 'new delhi', 'new delhi', 'india', 'Sales person', 'NA', 'abc123', 0, 6, 5, 1, '2017-04-04 05:46:52', '2016-11-10 09:59:38'),
(2, 'Feroz', 'Khan', 'so2@excalibur.com', 'Male', '1998-12-10', '21232f297a57a5a743894a0e4a801fc3', 'Feroz', '9988776655', '9999999900', 'new delhi', 345456, 'new delhi', 'new delhi', 'india', 'sales person', NULL, 'abc1234', 0, 6, 5, 1, '2017-04-04 05:10:20', '2016-11-25 12:27:32'),
(306, 'admin', 'admin', 'admin@excalibur.com', 'Male', '2016-12-14', '21232f297a57a5a743894a0e4a801fc3', 'admin', '9988776651', '9988776652', 'KANPUR', 208025, 'kanpur', 'U.P', 'india', 'good admin', NULL, NULL, 0, 6, 1, 1, '2017-01-04 11:18:40', '2016-12-19 07:24:51'),
(309, 'Saurabh', 'kapoor', 'a@a.com', 'Male', NULL, '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 6, 3, 1, '2017-02-20 06:14:08', '2017-02-20 06:14:08'),
(310, 'sid', 'gopal', 'sid@admin.com', 'Male', '2016-12-14', '21232f297a57a5a743894a0e4a801fc3', 'admin', '1234567890', '1234567890', 'KANPUR', 208025, 'kanpur', 'U.P', 'india', 'good admin', NULL, NULL, 4, 0, 2, 1, '2017-01-04 11:18:40', '2016-12-19 07:24:51'),
(311, 'vipin', 'kumar', 'vipin@vipin.com', 'Male', NULL, '21232f297a57a5a743894a0e4a801fc3', NULL, '25689314', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 6, 4, 1, '2017-04-05 09:07:10', '2017-04-05 09:07:10');

-- --------------------------------------------------------

--
-- Table structure for table `user_target`
--

CREATE TABLE IF NOT EXISTS `user_target` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `target_type` int(11) NOT NULL,
  `target` int(11) NOT NULL,
  `target_month` int(11) NOT NULL,
  `created_by` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_target`
--

INSERT INTO `user_target` (`id`, `user_id`, `target_type`, `target`, `target_month`, `created_by`) VALUES
(1, 1, 1, 10, 4, '0000-00-00 00:00:00'),
(2, 1, 2, 5, 4, '0000-00-00 00:00:00'),
(3, 1, 3, 50000, 4, '0000-00-00 00:00:00'),
(4, 1, 4, 5, 4, '0000-00-00 00:00:00'),
(5, 2, 1, 10, 4, '0000-00-00 00:00:00'),
(6, 2, 2, 5, 4, '0000-00-00 00:00:00'),
(7, 2, 3, 50000, 4, '0000-00-00 00:00:00'),
(8, 2, 4, 5, 4, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `category_products`
--
ALTER TABLE `category_products`
  ADD PRIMARY KEY (`id`), ADD KEY `category_id` (`category_id`), ADD KEY `status` (`status`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `device_info`
--
ALTER TABLE `device_info`
  ADD PRIMARY KEY (`deviceId`), ADD KEY `user_id` (`user_id`) USING BTREE;

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`), ADD KEY `added_by` (`added_by`);

--
-- Indexes for table `keys`
--
ALTER TABLE `keys`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `leads`
--
ALTER TABLE `leads`
  ADD PRIMARY KEY (`id`), ADD KEY `category_id` (`category_id`), ADD KEY `product_id` (`product_id`), ADD KEY `added_by` (`added_by`), ADD KEY `follower_id` (`follower_id`), ADD KEY `status` (`status`);

--
-- Indexes for table `meetings`
--
ALTER TABLE `meetings`
  ADD PRIMARY KEY (`id`), ADD KEY `status` (`status`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `m_branch`
--
ALTER TABLE `m_branch`
  ADD PRIMARY KEY (`id`), ADD KEY `status` (`status`);

--
-- Indexes for table `m_category`
--
ALTER TABLE `m_category`
  ADD PRIMARY KEY (`id`), ADD KEY `status` (`status`);

--
-- Indexes for table `m_district`
--
ALTER TABLE `m_district`
  ADD PRIMARY KEY (`id`), ADD KEY `status` (`status`);

--
-- Indexes for table `m_privileges`
--
ALTER TABLE `m_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_roles`
--
ALTER TABLE `m_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_status`
--
ALTER TABLE `m_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_log`
--
ALTER TABLE `status_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transfer_log`
--
ALTER TABLE `transfer_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trans_role_privileges`
--
ALTER TABLE `trans_role_privileges`
  ADD PRIMARY KEY (`id`), ADD KEY `role_id` (`role_id`), ADD KEY `privilege_id` (`privilege_id`), ADD KEY `privilege_id_2` (`privilege_id`);

--
-- Indexes for table `trans_user_category`
--
ALTER TABLE `trans_user_category`
  ADD PRIMARY KEY (`id`), ADD KEY `category_id` (`category_id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `trans_user_manager`
--
ALTER TABLE `trans_user_manager`
  ADD PRIMARY KEY (`id`), ADD KEY `manager_id` (`manager_id`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`), ADD KEY `role_id` (`role_id`), ADD KEY `status` (`status`), ADD KEY `branch` (`branch`);

--
-- Indexes for table `user_target`
--
ALTER TABLE `user_target`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `category_products`
--
ALTER TABLE `category_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT for table `keys`
--
ALTER TABLE `keys`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `leads`
--
ALTER TABLE `leads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `meetings`
--
ALTER TABLE `meetings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_branch`
--
ALTER TABLE `m_branch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `m_category`
--
ALTER TABLE `m_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `m_district`
--
ALTER TABLE `m_district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `m_privileges`
--
ALTER TABLE `m_privileges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `m_roles`
--
ALTER TABLE `m_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `m_status`
--
ALTER TABLE `m_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `status_log`
--
ALTER TABLE `status_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `transfer_log`
--
ALTER TABLE `transfer_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `trans_role_privileges`
--
ALTER TABLE `trans_role_privileges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=303;
--
-- AUTO_INCREMENT for table `trans_user_category`
--
ALTER TABLE `trans_user_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `trans_user_manager`
--
ALTER TABLE `trans_user_manager`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=312;
--
-- AUTO_INCREMENT for table `user_target`
--
ALTER TABLE `user_target`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `category_products`
--
ALTER TABLE `category_products`
ADD CONSTRAINT `category_products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `m_category` (`id`),
ADD CONSTRAINT `category_products_ibfk_2` FOREIGN KEY (`status`) REFERENCES `m_status` (`id`);

--
-- Constraints for table `device_info`
--
ALTER TABLE `device_info`
ADD CONSTRAINT `device_info_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `leads`
--
ALTER TABLE `leads`
ADD CONSTRAINT `leads_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `m_category` (`id`),
ADD CONSTRAINT `leads_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `category_products` (`id`),
ADD CONSTRAINT `leads_ibfk_3` FOREIGN KEY (`added_by`) REFERENCES `user` (`id`),
ADD CONSTRAINT `leads_ibfk_4` FOREIGN KEY (`follower_id`) REFERENCES `user` (`id`),
ADD CONSTRAINT `leads_ibfk_5` FOREIGN KEY (`status`) REFERENCES `m_status` (`id`);

--
-- Constraints for table `meetings`
--
ALTER TABLE `meetings`
ADD CONSTRAINT `meetings_ibfk_1` FOREIGN KEY (`status`) REFERENCES `m_status` (`id`),
ADD CONSTRAINT `meetings_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Constraints for table `m_branch`
--
ALTER TABLE `m_branch`
ADD CONSTRAINT `m_branch_ibfk_1` FOREIGN KEY (`status`) REFERENCES `m_status` (`id`);

--
-- Constraints for table `m_category`
--
ALTER TABLE `m_category`
ADD CONSTRAINT `m_category_ibfk_1` FOREIGN KEY (`status`) REFERENCES `m_status` (`id`);

--
-- Constraints for table `trans_role_privileges`
--
ALTER TABLE `trans_role_privileges`
ADD CONSTRAINT `trans_role_privileges_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `m_roles` (`id`),
ADD CONSTRAINT `trans_role_privileges_ibfk_2` FOREIGN KEY (`privilege_id`) REFERENCES `m_privileges` (`id`);

--
-- Constraints for table `trans_user_category`
--
ALTER TABLE `trans_user_category`
ADD CONSTRAINT `trans_user_category_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
ADD CONSTRAINT `trans_user_category_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `m_category` (`id`);

--
-- Constraints for table `trans_user_manager`
--
ALTER TABLE `trans_user_manager`
ADD CONSTRAINT `trans_user_manager_ibfk_1` FOREIGN KEY (`manager_id`) REFERENCES `user` (`id`),
ADD CONSTRAINT `trans_user_manager_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
