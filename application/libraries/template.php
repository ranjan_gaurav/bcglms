<?php
//require APPPATH."third_party/MX/Controller.php";
class Template  {

    function load($view,$vars = array())
    {
        $CI = &get_instance();
        $CI->load->view('includes/header', $vars);
        $CI->load->view($view);
        $CI->load->view('includes/footer');
    }

}
?>