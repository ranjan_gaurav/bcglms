<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>OBC | LMS</title>

<!-- Bootstrap -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css"
	rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/jquery-ui.css"
	rel="stylesheet">
	<!-- Custom Theme Style -->
<link href="<?php echo base_url(); ?>assets/css/custom.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css"
	rel="stylesheet">
<!-- NProgress -->
<link href="<?php echo base_url(); ?>assets/css/nprogress.css"
	rel="stylesheet">
<!-- Animate.css -->
<link href="<?php echo base_url(); ?>assets/css/animate.min.css"
	rel="stylesheet">
<!-- iCheck -->
<link href="<?php echo base_url(); ?>assets/css/green.css"
	rel="stylesheet">
<!-- bootstrap-progressbar -->
<link
	href="<?php echo base_url(); ?>assets/css/bootstrap-progressbar-3.3.4.min.css"
	rel="stylesheet">
<!-- JQVMap -->
<link href="<?php echo base_url(); ?>assets/css/jqvmap.min.css"
	rel="stylesheet" />
<!-- bootstrap-daterangepicker -->
<link href="<?php echo base_url(); ?>assets/css/daterangepicker.css"
	rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap.min.css"
	rel="stylesheet" />

<link href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css"
	rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/dataTables.fontAwesome.css"
	rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/style.css"
	rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/buttons.bootstrap.min.css"
	rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/select.bootstrap.min.css"
	rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/editor.bootstrap.min.css"
	rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/bootstrap-multiselect.css"
	rel="stylesheet">
<script>
var base_url = "<?php echo base_url(); ?>";
</script>
</head>