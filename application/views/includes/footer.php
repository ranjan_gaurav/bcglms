<!-- footer content -->
<footer>
	<div class="pull-right">
		&copy; LMS <?php echo date('Y'); ?> All Rights Reserved.
	</div>
	<div class="clearfix"></div>
</footer>
<!-- /footer content -->
</div>
</div>
<!-- jQuery -->

<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery-ui.min.js"></script>
<script src="<?php echo base_url();?>assets/js/smartresize.js"></script>
<!-- Custom Theme Scripts -->
<script src="<?php echo base_url();?>assets/js/custom.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.bootpag.min.js"></script>
<script src="<?php echo base_url();?>assets/js/validator.js"></script>
<script src="<?php echo base_url();?>assets/js/validation.js"></script>

<!-- FastClick -->
<script src="<?php echo base_url();?>assets/js/fastclick.js"></script>
<!-- NProgress -->
<script src="<?php echo base_url();?>assets/js/nprogress.js"></script>
<!-- Chart.js -->
<script src="<?php echo base_url();?>assets/js/Chart.min.js"></script>
<!-- gauge.js -->
<script src="<?php echo base_url();?>assets/js/gauge.min.js"></script>
<!-- bootstrap-progressbar -->
<script
	src="<?php echo base_url();?>assets/js/bootstrap-progressbar.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url();?>assets/js/icheck.min.js"></script>
<!-- Skycons -->
<script src="<?php echo base_url();?>assets/js/skycons.js"></script>
<!-- Flot -->
<script src="<?php echo base_url();?>assets/js/jquery.flot.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.flot.pie.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.flot.time.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.flot.stack.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.flot.resize.js"></script>
<!-- Flot plugins -->
<script src="<?php echo base_url();?>assets/js/jquery.flot.orderBars.js"></script>
<script	src="<?php echo base_url();?>assets/js/jquery.flot.spline.min.js"></script>
<script src="<?php echo base_url();?>assets/js/curvedLines.js"></script>
<!-- DateJS -->
<script src="<?php echo base_url();?>assets/js/date.js"></script>
<!-- JQVMap -->
<script src="<?php echo base_url();?>assets/js/jquery.vmap.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.vmap.world.js"></script>
<script	src="<?php echo base_url();?>assets/js/jquery.vmap.sampledata.js"></script>
<!-- bootstrap-daterangepicker -->
<script src="<?php echo base_url();?>assets/js/moment.min.js"></script>
<script src="<?php echo base_url();?>assets/js/daterangepicker.js"></script>
<script src="<?php echo base_url();?>assets/js/ajaxfileupload.js."></script>
<script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-multiselect.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.dataTables.min.js"></script>
<script	src="<?php echo base_url();?>assets/js/dataTables.bootstrap.min.js"></script>
<script	src="<?php echo base_url();?>assets/js/dataTables.responsive.min.js"></script>
<script	src="<?php  echo base_url();?>assets/js/responsive.bootstrap.min.js"></script>
<script	src="<?php echo base_url();?>assets/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>assets/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>assets/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>assets/js/buttons.print.min.js"></script>
<script	src="<?php echo base_url();?>assets/js/dataTables.fixedHeader.min.js"></script>
<script	src="<?php echo base_url();?>assets/js/dataTables.keyTable.min.js"></script>
<script	src="<?php echo base_url();?>assets/js/datatables.scroller.min.js"></script>
<script src="<?php echo base_url();?>assets/js/dataTables.select.min.js"></script>	
<script src="<?php echo base_url();?>assets/js/table.js"></script>
<script src="<?php echo base_url();?>assets/js/auth.js"></script>
<script src="<?php  echo base_url();?>assets/js/admin.js"></script>
<script src="<?php echo base_url();?>assets/js/getUser.js"></script>
<script src="<?php echo base_url();?>assets/js/getPrivilege.js"></script>
<script src="<?php echo base_url();?>assets/js/getRole.js"></script>
<script src="<?php echo base_url();?>assets/js/pagination.js"></script>

</body>
</html>