<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : API management service
 * this service class is responsible of all the application logic 
 * related to API
 */
class Admin_service extends CI_Model {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'admin/Admin_dao' );
		include_once './application/objects/Response.php';
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: login
	 *         Description: validate credentials
	 */
	public function login($email, $password, $deviceType, $deviceId, $fcmRegId) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$isUserExist = $this->isUserExist ( $email );
			if ($isUserExist->getStatus () == 1) {
				
				$user = $isUserExist->getObjArray ()[0];
				if ($user->status == ACTIVE) {
					if (md5 ( $password ) == $user->password) {
						$updateDeviceData = $adminDao->updateDeviceData ( $user->id, $deviceType, $deviceId, $fcmRegId );
						if ($updateDeviceData) {
							$response->setStatus ( 1 );
							$response->setMsg ( "Valid user" );
							$response->setObjArray ( $user );
						} else {
							$response->setStatus ( 0 );
							$response->setMsg ( "Database error" );
							$response->setObjArray ( NULL );
						}
					} else {
						$response->setStatus ( 0 );
						$response->setMsg ( "Invalid credentials" );
						$response->setObjArray ( NULL );
					}
				} else {
					$response->setStatus ( 2 );
					$response->setMsg ( "Inactive user" );
					$response->setObjArray ( NULL );
				}
			} else {
				$response = $isUserExist;
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: isUserExist
	 *         Description: check user exist or not
	 */
	public function isUserExist($email) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->isUserExist ( $email );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: checkEmail
	 *         Description: check Email
	 */
	public function checkEmail($email) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->checkEmail ( $email );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: updatePassword
	 *         Description: update Password
	 */
	public function updatePassword($data) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->updatePassword ( $random_code, $email );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: GetUserProfile
	 *         Description: Get User Profile
	 */
	public function GetUserProfile($userid) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->GetUserProfile ( $userid );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: update_profile
	 *         Description: update profile
	 */
	public function update_profile($data, $userid) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->update_profile ( $data, $userid );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: update_password
	 *         Description: update password
	 */
	public function update_password($data, $userid) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->update_password ( $data, $userid );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant singh
	 *         Date: 12th Nov 2016
	 *         Method: getRoles
	 *         Description: get Roles
	 */
	public function getRoles() {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->getRoles ();
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant singh
	 *         Date: 12th Nov 2016
	 *         Method: getPrivileges
	 *         Description: get Privileges
	 */
	public function getPrivileges() {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->getPrivileges ();
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : nishant singh
	 *         Date: 12th Nov 2016
	 *         Method: editRoles
	 *         Description: edit the existing Roles
	 */
	public function editRoles($id) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->editRoles ( $id );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant SinghS
	 *         Date: 12th Nov 2016
	 *         Method: editPrivileges
	 *         Description: edit the exist Privileges
	 */
	public function editPrivileges($id) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->editPrivileges ( $id );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 12th Nov 2016
	 *         Method: update_role
	 *         Description: update role
	 */
	public function update_role($data,$previlege) {
		
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->update_role ( $data,$previlege );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 12th Nov 2016
	 *         Method: update_privilege
	 *         Description: update privilege
	 */
	public function update_privilege($data) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->update_privilege ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 12th Nov 2016
	 *         Method: new_role
	 *         Description: new role
	 */
	public function new_role($data) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->new_role ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 12th Nov 2016
	 *         Method: new_role
	 *         Description: new role
	 */
	public function new_privilege($data) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->new_privilege ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 12th Nov 2016
	 *         Method: new_user
	 *         Description: new user
	 */
	public function new_user($data) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->new_user ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: getUserDetails
	 *         Description: get all existing User Details
	 */
	public function getUserDetails() {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->getUserDetails ();
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: getImage
	 *         @Description: get image from database
	 * @param
	 *        	eters: limit, start
	 */
	public function getImage($limit, $start) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->getImage ( $limit, $start );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: addImage
	 *         @Description: adding image into gallery
	 * @param
	 *        	eters: filename,description
	 */
	public function addImage($filename, $description) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->addImage ( $filename, $description );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: deletePrivilege
	 *         @Description: deleted privileges from database . it actually inactive the privileges and kept in list as inactive
	 * @param
	 *        	eters: id
	 */
	public function deletePrivilege($id) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->deletePrivilege ( $id );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: deleteRole
	 *         @Description: deleted role from database . it actually inactive the roles and kept in list as inactive
	 * @param
	 *        	eters: id
	 */
	public function deleteRole($id) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->deleteRole ( $id );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: deleteUser
	 *         @Description: deleted user from database . it actually inactive the user and kept in list as inactive
	 * @param
	 *        	eters: id
	 */
	public function deleteUser($id) {
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->deleteUser ( $id );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: update_user
	 *         Description: update_user
	 */
	public function update_user($updateDetails) {
	
		$response = new Response ();
		try {
			$adminDao = new Admin_dao ();
			$response = $adminDao->update_user ( $updateDetails);
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $checkExistingUser->getMsg () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	public function CronJobs($Starttime)
	{
		$query = "SELECT * FROM meetings WHERE meetingTime LIKE  '%$Starttime%'";  //print_r($query); die();
		$result = $this->db->query ( $query );
	}
}
?>