<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : This Dao class is responsible of all the database related operation for API
 */
class Admin_dao extends CI_Model {
	public function __construct() {
		parent::__construct ();
		include_once './application/objects/Response.php';
		date_default_timezone_set('Asia/Calcutta');
		$this->load->helper('date');
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: checkEmail
	 *         Description: check emalid is present or not in our database
	 */
	public function checkEmail($email) {
		$this->db->where ( 'email', $email );
		$result = $this->db->get ( 'user' );
		$rows = $result->num_rows ();
		if ($rows === 1) {
			return true;
		}
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: updatePassword
	 *         Description: update the password
	 */
	public function updatePassword($random_code, $email) {
		$data ['password'] = md5 ( $random_code );
		$this->db->where ( 'email', $email );
		$update = $this->db->update ( 'user', $data );
		
		if ($update) {
			return true;
		} 

		else {
			return false;
		}
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: GetUserProfile
	 *         Description: get the profile of user
	 */
	public function GetUserProfile($userid) {
		$this->db->where ( 'id', $userid );
		$result = $this->db->get ( 'user' );
		$rows = $result->row ();
		return $rows;
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: update_profile
	 *         Description: update the profile of the user
	 */
	public function update_profile($data, $userid) {
		$this->db->where ( 'id', $userid );
		$result = $this->db->update ( 'user', $data );
		if ($result) {
			return true;
		}
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: update_password
	 *         Description: update the password by the registered user
	 */
	public function update_password($data, $userid) {
		$this->db->where ( 'id', $userid );
		$result = $this->db->update ( 'user', $data );
		if ($result) {
			return true;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: getRoles
	 *         Description: get Roles
	 */
	public function getRoles() {
		$role = array();
		$r = array();
		$q = "select id,
					title,
					description, 
					status as status_val, 
					(select title from m_status where m_status.id=m_roles.status) as status  
					from m_roles";
		$query = $this->db->query ( $q );
		if($query->num_rows()>0){
			foreach ($query->result() as $row){
				$r['id'] = $row->id;
				$r['title'] = $row->title;
				$r['description'] = $row->description;
				$r['status'] = $row->status;
				$r['status_val'] = $row->status_val;
				$r['roles_privilage'] = $this->getRolesPrivilege($row->id);
				array_push($role,$r);
			}
			//echo'<pre>';	print_r($role); die();
		}
		return $role;
	}
	
	private function  getRolesPrivilege($roleId){
		$q = "SELECT   t.privilege_id  
				from trans_role_privileges as t
				where t.role_id=".$roleId;
		$query = $this->db->query ( $q );		
		return $query->result_array();
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: getPrivileges
	 *         Description: get Privileges
	 */
	public function getPrivileges() 

	{
		$query = $this->db->query ( "select id,
				title,
				description, 
				status as status_val,
				(select title from m_status where m_status.id=m_privileges.status) as status
				 from m_privileges" );
		$result = $query->result_array ();
		return $result;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: update_role
	 *         Description: update the existing role
	 */
	public function update_role($data, $previlege) {
		$updatedPrivilege = array ();
		$updateprev = array ();
		for($i = 0; $i < count ( $previlege ); $i ++) {
			$updateprev ['updatedRolePrev'] = $previlege [$i] ['updateRolePrivilege'];
			array_push ( $updatedPrivilege, $updateprev );
		}
		$id = $data ['id'];
		$title = $data ['title'];
		$description = $data ['description'];
		$status = $data ['status'];
		$value = array (
				'title' => $title,
				'description' => $description,
				'status' => $status 
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'm_roles', $value );
		
		if ($id) {
			$delete = $this->db->query ( "delete from trans_role_privileges where role_id=" . $this->db->escape ( $id ) );
			if ($delete) {
				for($i = 0; $i < count ( $updatedPrivilege ); $i ++) {
					
					$transPrivilege = array (
							'role_id' => $id,
							'privilege_id' => $updatedPrivilege [$i] ['updatedRolePrev'] 
					);
					$trans_privilege = $this->db->insert ( 'trans_role_privileges', $transPrivilege );
				}
			}
		}
		return $trans_privilege;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: update_privilege
	 *         Description: update the existing Privileges
	 */
	public function update_privilege($data) {
		$id = $data ['id'];
		$title = $data ['title'];
		$description = $data ['description'];
		$status = $data ['status'];
		
		$value = array (
				'title' => $title,
				'description' => $description,
				'status' => $status 
		);
		
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'm_privileges', $value );
		if ($result) {
			return true;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: new_role
	 *         Description:Add new Role
	 */
	public function new_role($data) {
		$title = $data ['title'];
		$description = $data ['description'];
		$status = $data ['status'];
		$privilege = $data ['privilege'];
		$count = count ( $privilege );
		
		$roleData = array (
				'title' => $title,
				'description' => $description,
				'status' => $status 
		);
		
		$role = $this->db->insert ( 'm_roles', $roleData );
		if ($role) {
			$roleId = $this->db->insert_id ();
		}
		if (! empty ( $roleId )) {
			for($i = 0; $i <= $count; $i ++) {
				$transPrivilege = array (
						'role_id' => $roleId,
						'privilege_id' => $privilege [$i] 
				);
				
				$trans_privilege = $this->db->insert ( 'trans_role_privileges', $transPrivilege );
			}
		}
		return true;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: new_privilege
	 *         Description: add new Privileges
	 */
	public function new_privilege($data) {
		$this->db->insert ( 'm_privileges', $data );
		return true;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: new_user
	 *         Description:add new user
	 */
	public function new_user($data) {
		$userId = NULL;
		$user = $this->db->insert ( 'user', $data );
		if ($user) {
			$userId = $this->db->insert_id ();
		}
		return $userId;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: getUserDetails
	 *         Description:geting existing User Details
	 */
	public function getUserDetails() {
		$query = $this->db->query ( "SELECT id, 
				firstname, 
				lastname, 
				email, 
				gender, 
				dob,  
				primaryContact, 
                 city,
				role_id as role_val,
				status as status_val,
				(select title from m_roles where m_roles.id=user.role_id) as role_id, 
				(select title from m_status where m_status.id=user.status) as status, 
                creationDate,
				address,
				pincode,
				state,
				country
				 FROM user" );
		$result = $query->result_array ();
		return $result;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: addImage
	 *         @Description: adding image into gallery
	 * @param
	 *        	eters: filename,description
	 */
	public function addImage($filename, $description) {
		$sql = "INSERT INTO gallery(`picture`,
				`description`,
				`added_by`) 
				VALUES(" . $this->db->escape ( $filename ) . 
		"," . $this->db->escape ( $description ) . 
		",305)";
		$result = $this->db->query ( $sql );
		return $result;
	}
	/**
	 *
	 * @author : Nishant Singh
	 * @Method: count
	 * @Description: count how many picture in gallery
	 */
	public function count() {
		$count = $this->db->get ( 'gallery' )->num_rows ();
		return $count;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: getImage
	 *         @Description: get image from database
	 * @param
	 *        	eters: limit, start
	 */
	public function getImage($limit, $start) {
		$this->db->select ( 'picture,description' );
		$this->db->limit ( $limit, $start );
		$this->db->order_by ( 'creationDate', 'desc' );
		$query = $this->db->get ( 'gallery' );
		if ($query->num_rows () > 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	/**
	 *
	 * @author : Nishant Singh
	 * @Method: deletePrivilege
	 * @Description: deleted privileges from database . it actually inactive the privileges and kept in list as inactive
	 * @param
	 *        	eters: id
	 */
	public function deletePrivilege($id) {
		$value = array (
				'status' => '2' 
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'm_privileges', $value );
		if ($result) {
			return true;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 * @Method: deleteRole
	 * @Description: deleted role from database . it actually inactive the roles and kept in list as inactive
	 * @parameters: id
	 */
	public function deleteRole($id) {
		$value = array (
				'status' => '2' 
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'm_roles', $value );
		
		if ($result) {
			return true;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: deleteUser
	 *         @Description: deleted user from database . it actually inactive the user and kept in list as inactive
	 * @param
	 *        	eters: id
	 */
	public function deleteUser($id) 
	   {
		$value = array (
				'status' => '2' 
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'user', $value );
		if ($result)
		 {
			return true;
		 }
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: detailPrivilege
	 *         @Description:count how many privilege are in list .
	 */
	public function detailPrivilege()
	 {
		$this->db->select ( 'id,title' );
		$this->db->where ( 'status', ACTIVE );
		$count = $this->db->get ( 'm_privileges' );
		$result = $count->result ();
		return $result;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: countRole
	 *         @Description:count how many role are in list .
	 */
	public function countRole() 
	{
		$this->db->select ( 'id,title' );
		
		$count = $this->db->get ( 'm_roles' );
		$result = $count->result ();
		return $result;
	}
/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: update_user
	 *         Description: update the existing user
	 */
	public function update_user($updateDetails) 
	    {
		 $id = $updateDetails ['0']['userUpadtedDetails'];
		$firstName =  $updateDetails ['1']['userUpadtedDetails'];
		$lastName = $updateDetails ['2']['userUpadtedDetails'];
		$email = $updateDetails ['3']['userUpadtedDetails'];
		$gender = $updateDetails ['4']['userUpadtedDetails'];
		$dob =$updateDetails ['5']['userUpadtedDetails'];
		$primaryContact = $updateDetails ['6']['userUpadtedDetails'];
		$address= $updateDetails ['7']['userUpadtedDetails'];
		$pincode=$updateDetails ['8']['userUpadtedDetails'];
		$city=$updateDetails ['9']['userUpadtedDetails'];
		$state=$updateDetails ['10']['userUpadtedDetails'];
		$country = $updateDetails ['11']['userUpadtedDetails'];
		$role= $updateDetails ['12']['userUpadtedDetails'];
		$status= $updateDetails ['13']['userUpadtedDetails'];
		$value = array (
				'firstname' => $firstName,
				'lastname' => $lastName,
				'email' => $email ,
				'gender' => $gender,
				'dob' => $dob,
				'primaryContact' => $primaryContact,
				'address' => $address,
				'pincode' => $pincode,
				'city' => $city,
				'state' => $state,
				'country' => $country,
				'role_id' => $role,
				'status' => $status,
				'modificationDate'=> date('Y-m-d H:i:s',now())
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'user', $value );
	if ($result)
	  {
			return true;
	  }
	}
}