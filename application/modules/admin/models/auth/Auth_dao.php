<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : This Dao class is responsible of all the database related operation for API
 */
class Auth_dao extends CI_Model {
	
	public function __construct() {
		parent::__construct ();
		include_once './application/objects/Response.php';
	}
	
	/**
	 * @author : Anjani Kr. Gupta
	 * @method : isUserExist
	 * @Desc : check user exist or not 
	 * @return : dealers list
	 * Date: 10th Nov 2016
	 */
	public function isUserExist($email) {
		$response = new Response ();
		try {
			$query = "SELECT user.*, roles.title role, branch.title branch  FROM user user INNER JOIN m_roles roles ON user.role_id=roles.id
						LEFT JOIN m_branch branch ON branch.id = user.branch WHERE user.email LIKE ".$this->db->escape($email);
			$result = $this->db->query ( $query );
			$user = $result->result()[0];
			
			if ($result->num_rows () > 0) {
				$response->setStatus ( 1 );
				$response->setMsg ( "User Exist." );
				$user = $result->result()[0];
				$prv = $this->getRolePrivileges($user->role_id);
				$user->privileges = $prv->getObjArray();
				$response->setObjArray ( $user );
				
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "User does not Exist." );
				$response->setObjArray ( NULL );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	public function getRolePrivileges($roleId){
		$response = new Response ();
		$query = "SELECT mp.id, mp.title, mp.description FROM m_privileges mp INNER JOIN trans_role_privileges trp ON mp.id=trp.privilege_id WHERE trp.role_id = ".$this->db->escape($roleId);
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "Privileges found." );
			$response->setObjArray ( $result->result() );
		} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "No privileges assigned to this role." );
				$response->setObjArray ( NULL );
		}
		return $response;
	}
		
	
}
