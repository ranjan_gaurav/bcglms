<!-- disallow browser cache -->
<meta HTTP-EQUIV="Pragma" content="no-cache">
<meta HTTP-EQUIV="Expires" content="-1">
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<!-- sidebar -->
         <?php  $this->view("includes/side_menu"); ?>
        </div>
		 <?php $this->view("includes/top_nav"); ?>
        <!-- page content -->
			<div class="right_col" role="main">
				<!-- page content -->

				<div class="">
					<div class="page-title"></div>
					<div class="clearfix"></div>
					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Add New User</h2>
									<div style="text-align: center; margin-right: 15%"
										class="search-error">
                        <?php //echo $this->session->flashdata('message')?>
               </div>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<br />

									<fieldset>
										<legend>Personal Information</legend>
										<form id="form" method="post" data-toggle="validator"
											class="form-horizontal form-label-left required"
											enctype="multipart/form-data">
											<fieldset>
												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12"
														for="firstname">First Name <span class="">*</span>
													</label>
													<div class="col-md-6 col-sm-6 col-xs-12">

														<input type="text" id="firstname"
															data-error="Please enter firstname field."
															class="form-control col-md-7 col-xs-12 " required>
													</div>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12"
														for="lastname">Last Name <span class="">*</span>
													</label>
													<div class="col-md-6 col-sm-6 col-xs-12">

														<input type="text" id="lastname" name="lastname"
															data-error="Please enter lastname field."
															class="form-control col-md-7 col-xs-12" required>
													</div>
													<div class="help-block with-errors"></div>
												</div>

												<div class="form-group">
													<label for="gender"
														class="control-label col-md-3 col-sm-3 col-xs-12">Gender <span
														class="">*</span></label>
													<div class="col-md-6 col-sm-6 col-xs-12">

														<input type="radio" class="gender" name="gender"
															value="Male">Male <input type="radio" class="gender"
															name="gender" value="Female">Female
													</div>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12"
														for="fileToUpload">picture <span class="">*</span>
													</label>
													<div class="col-md-6 col-sm-6 col-xs-12">


														<input type="file" id="fileToUpload" name="fileToUpload"
															class="form-control col-md-7 col-xs-12"
															data-error="Please enter picture field." required>
													</div>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label class="control-label col-md-3 col-sm-3 col-xs-12">Date
														Of Birth <span class="">*</span>
													</label>
													<div class="col-md-6 col-sm-6 col-xs-12">


														<input id="dob"
															class="date-picker form-control col-md-7 col-xs-12"  
															data-error="Please enter date of birth field."
															type="text" name="dob" required>
															<span><i data-time-icon="icon-time" data-date-icon="icon-calendar">
      </i></span>
													</div>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="primaryContact"
														class="control-label col-md-3 col-sm-3 col-xs-12">Primary
														Contact <span class="">*</span>
													</label>
													<div class="col-md-6 col-sm-6 col-xs-12">


														<input id="primaryContact"
															class="form-control col-md-7 col-xs-12" type="text"
															data-error="Please enter primary Contact field."
															name="primaryContact" required>
													</div>
													<div class="help-block with-errors"></div>
												</div>

												<div class="form-group">
													<label for="address"
														class="control-label col-md-3 col-sm-3 col-xs-12">Address
														<span class="">*</span>
													</label>
													<div class="col-md-6 col-sm-6 col-xs-12">
														<input id="address"
															class="form-control col-md-7 col-xs-12"
															data-error="Please enter address field." type="text"
															name="location" required>
													</div>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="pincode"
														class="control-label col-md-3 col-sm-3 col-xs-12">Pincode
														<span class="">*</span>
													</label>
													<div class="col-md-6 col-sm-6 col-xs-12">
														<input id="pincode"
															class="form-control col-md-7 col-xs-12"
															data-error="Please enter pincode field." type="text"
															name="pincode">
													</div>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="city"
														class="control-label col-md-3 col-sm-3 col-xs-12">City <span
														class="">*</span></label>
													<div class="col-md-6 col-sm-6 col-xs-12">
														<input id="city" class="form-control col-md-7 col-xs-12"
															data-error="Please enter city field." type="text"
															name="city">
													</div>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="state"
														class="control-label col-md-3 col-sm-3 col-xs-12">State <span
														class="">*</span></label>
													<div class="col-md-6 col-sm-6 col-xs-12">
														<input id="state" class="form-control col-md-7 col-xs-12"
															data-error="Please enter state field." type="text"
															name="state">
													</div>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="country"
														class="control-label col-md-3 col-sm-3 col-xs-12">Country
														<span class="">*</span>
													</label>
													<div class="col-md-6 col-sm-6 col-xs-12">
														<input id="country"
															class="form-control col-md-7 col-xs-12"
															data-error="Please enter country field." type="text"
															name="country">
													</div>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="role_Id"
														class="control-label col-md-3 col-sm-3 col-xs-12">Role <span
														class="">*</span></label>
													<div class="col-md-6 col-sm-6 col-xs-12" required>
                                           <?php
													for($i = 0; $i < count ( $countRole ); $i ++) {																			
														?>
						                       <input type="checkbox" class="role_Id"
															name="role[]" value="<?php  echo $countRole[$i]->id ; ?>">
						                       <?php																															
                                                         echo $countRole [$i]->title;
																				}
																				?>
											</div>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="status"
														class="control-label col-md-3 col-sm-3 col-xs-12">Status <span
														class="">*</span></label>
													<div class="col-md-6 col-sm-6 col-xs-12" required>

														<input type="radio" class="status" name="status" value="1">Active
														<input type="radio" class="status" name="status" value="2">Inactive

													</div>
													<div class="help-block with-errors"></div>
												</div>
											</fieldset>
											<fieldset>
												<legend> Account information</legend>
												<div class="form-group">
													<label for="email"
														class="control-label col-md-3 col-sm-3 col-xs-12">Email <span
														class="">*</span></label>
													<div class="col-md-6 col-sm-6 col-xs-12">
														<input id="email" class="form-control col-md-7 col-xs-12"
															data-error="Please enter email field." type="email"
															name="email" required>
													</div>
													<div class="help-block with-errors"></div>
												</div>
												<div class="form-group">
													<label for="password"
														class="control-label col-md-3 col-sm-3 col-xs-12">Password<span
														class="">*</span></label>
													<div class="col-md-6 col-sm-6 col-xs-12">
														<input id="password"
															class="form-control col-md-7 col-xs-12"
															data-minlength="5"
															data-error="Please enter password field." type="password"
															name="password" required>
													</div>
													<div class="help-block with-errors">Minimum 5 chr</div>
												</div>

												<div class="ln_solid"></div>
												<div class="form-group">
													<div style="text-align: center;"
														class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
														<!--    <button type="Button" class="btn btn-primary">Cancel</button> -->
														<input type="submit" id="register" class="btn btn-success"
															value="Register"> <span id="loader"
															style="position: relative; top: 10px; right: 10px;"><i
															class="fa fa-circle-o-notch fa-spin"
															style="font-size: 24px"></i></span>
													</div>
												</div>
											</fieldset>
										</form>
								
								</div>
							</div>
						</div>
					</div>
				</div>

				<div></div>
			</div>
		</div>
	</div>
	<!-- /page content -->