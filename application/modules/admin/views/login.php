<?php error_reporting(0);?>

<?php //print_r(base_url());
    
?>

<head>
<title>LMS</title>

<!-- Bootstrap -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css"
	rel="stylesheet">
<!-- Font Awesome -->
<link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css"
	rel="stylesheet">
<!-- NProgress -->
<link href="<?php echo base_url(); ?>assets/css/nprogress.css"
	rel="stylesheet">
<!-- Animate.css -->
<link href="<?php echo base_url(); ?>assets/css/animate.min.css"
	rel="stylesheet">
<!-- Custom Theme Style -->
<link href="<?php echo base_url(); ?>assets/css/custom.min.css"
	rel="stylesheet">
<link href="<?php echo base_url(); ?>assets/css/style.css"
	rel="stylesheet">
<script>
	var base_url = "<?php echo base_url();?>";
    </script>
</head>

<body class="login">
	<div>
		<a class="hiddenanchor" id="signup"></a> <a class="hiddenanchor"
			id="signin"></a>

		<div class="login_wrapper">
			<div class="animate form login_form">
				<section class="login_content">
					<form id="login">
						<h1>Login</h1>
						<div class="text-left">
							<input type="text" name="username" id="username"
								class="form-control" placeholder="Username"
								value="<?php echo $_COOKIE['username'];?>" />
						</div>
						<div class="text-left">
							<input type="password" name="password" id="password"
								class="form-control" placeholder="Password" value="<?php echo $_COOKIE['password'];?>"/>
						</div>
						<div class="text-left">
							<input type="checkbox" name="remember" id="remember" class=""
								 <?php if($_COOKIE['remember_me'] == '1') {
		echo 'checked="checked"';
	}?> <label for="remember"><span
								style="bottom: 2px; position: relative;">Stay signed in</span></label>
						</div>
						<div>
							<a class="btn btn-default submit" id="loginBtn">Log in</a> <span
								id="loader" style="position: relative; top: 10px; right: 10px;"><i
								class="fa fa-circle-o-notch fa-spin" style="font-size: 24px"></i></span>
							<a class="reset_pass"
								href="<?php echo base_url('admin/forgot_password');?>">Forgot
								password?</a>
						</div>

						<div class="clearfix"></div>

						<div class="separator">
							<!--                 <p class="change_link">New to site?
                  <a href="#signup" class="to_register"> Create Account </a>
                </p> -->

							<div class="clearfix"></div>
							<br />

							<div>
								<h1>
									<i class="fa fa-paw"></i> LMS
								</h1>
								<p>&copy; <?php echo date('Y'); ?> All Rights Reserved.</p>
							</div>
						</div>
					</form>
				</section>
			</div>
		</div>
	</div>
     <div>
		<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
		<script src="<?php echo base_url();?>assets/js/jquery.validate.js"></script>
		<script src="<?php echo base_url();?>assets/js/auth.js"></script>
	</div>
</body>
</html>
