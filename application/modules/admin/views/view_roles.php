<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<!-- sidebar -->
          <?php $this->view("includes/side_menu"); ?>
        </div>
		 <?php $this->view("includes/top_nav"); ?>
				<!-- page content -->

			<div class="right_col" role="main" id="result">
				<div class="">
					<div class="page-title">
						<div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
							</div>
						</div>
					</div>

					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>View Roles</h2>
									<button type="button" class="btn btn-success"
										style="float: right;"
										onclick="location.href='<?php echo base_url();?>admin/admin/add_role'">Add
										New Role</button>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<table id="roleDetails"
										class="display table table-striped table-bordered no-footer dataTable"
										cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>Id.</th>
												<th>Title</th>
												<th>Description</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade bs-roleDetails-modal-lg"  id="myModal" tabindex="-1"
				role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">

						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Edit Role</h4>
						</div>
						<div class="modal-body">
							<form method="post" id="role"
								class="form-horizontal form-label-left" novalidate>


								<div class="search-error" id="search-error"
									style="text-align: center"></div>
								<br>
								<div class="item form-group">

									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="id" class="form-control col-md-7 col-xs-12"
											name="id" placeholder="" required="required" type="hidden">
									</div>
								</div>
								<div class="item form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12"
										for="name">Title <span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="title" class="form-control col-md-7 col-xs-12"
											name="title" placeholder="" required="required" type="text">
									</div>
								</div>
								<div class="item form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12"
										for="name">Description <span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="description"
											class="form-control col-md-7 col-xs-12" name="description"
											placeholder="" required="required" type="text">
									</div>
								</div>
								<div class="item form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12"
										for="name">Status <span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select class="form-control col-md-7 col-xs-12" name="status" id="status" required>
											<option value="">Select</option>
											<option value="1">active</option>
											<option  value="2">inactive</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="privileges"
										class="control-label col-md-3 col-sm-3 col-xs-12">Privileges <span
										class="required">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select id="privileges" multiple="multiple" name="privileges">
											<?php
											for($i = 0; $i < count ( $detailPrivilege ); $i ++) {
												
												?>
						                        <option
												value="<?php  echo $detailPrivilege[$i]->id ; ?>"> 
						                       <?php echo $detailPrivilege[$i]->title;?></option>
						                       <?php
												
											}
											
											?>
                                                </select>

									</div>
								</div>
								<div class="ln_solid"></div>
								<div class="form-group">
									<div class="col-md-6 col-md-offset-3" align="center">

										<button id="updaterole" type="submit" class="btn btn-success">
											Update <span id="loader"
												style="position: relative; top: 10px; right: 10px;"><i
												class="fa fa-circle-o-notch fa-spin" style="font-size: 24px"></i></span>
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>