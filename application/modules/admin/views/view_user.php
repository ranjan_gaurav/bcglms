<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<!-- sidebar -->
         <?php $this->view("includes/side_menu"); ?>
        </div>
		 <?php $this->view("includes/top_nav"); ?>
				<!-- page content -->

			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 item form-group pull-right top_search">
							</div>
						</div>
					</div>

					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>View User Details</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<table id="userDetails"
										class="display table table-striped table-bordered no-footer dataTable"
										cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>First Name</th>
												<th>Last Name</th>
												<th>Email</th>
												<th>Gender</th>
												<th>Date Of Birth</th>
												<th>primary Contact</th>
												<th>City</th>
												<th>Role Id</th>
												<th>Status</th>
												<th>Start Date</th>
												<th>Action</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade bs-userDetails-modal-lg" tabindex="-1" id="myModal"
				role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">

						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Edit User</h4>
						</div>
						<div class="modal-body">
							<form method="post" id="user"
								class="form-horizontal form-label-left" novalidate>


								<div class="search-error" id="search-error"
									style="text-align: center"></div>
								<br>
								<div class="item item form-group">

									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="id" class="form-control col-md-7 col-xs-12"
											name="id" placeholder="" required="required" type="hidden">
									</div>
								</div>
								<div class="item item form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12"
										for="name">First Name <span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="firstname" class="form-control col-md-7 col-xs-12"
											name="firstname" placeholder="" required="required"
											type="text">
									</div>
								</div>
								<div class="item item form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12"
										for="name">Last Name <span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="lastname" class="form-control col-md-7 col-xs-12"
											name="lastname" placeholder="" required="required"
											type="text">
									</div>
								</div>
								<div class="item form-group">
									<label for="email"
										class="control-label col-md-3 col-sm-3 col-xs-12">Email <span
										class="">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="email" class="form-control col-md-7 col-xs-12"
											type="text" name="email">
									</div>
								</div>

								<div class="item form-group">
									<label for="gender"
										class="control-label col-md-3 col-sm-3 col-xs-12">Gender <span
										class="">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<select class="form-control col-md-7 col-xs-12" name="gender"
											id="gender" required>
											<option value="">Select</option>
											<option value="Male">Male</option>
											<option value="Female">Female</option>
										</select>
									</div>
								</div>
								<div class="item form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Date
										Of Birth <span class="">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="dob"
											class="date-picker form-control col-md-7 col-xs-12"
											type="text" name="dob" required>
									</div>
								</div>
								<div class="item form-group">
									<label for="primaryContact"
										class="control-label col-md-3 col-sm-3 col-xs-12">Primary
										Contact <span class="">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="primaryContact"
											class="form-control col-md-7 col-xs-12" type="text"
											name="primaryContact" required>
									</div>
								</div>
								<div class="item form-group">
									<label for="address"
										class="control-label col-md-3 col-sm-3 col-xs-12">Address <span
										class="">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="address" class="form-control col-md-7 col-xs-12"
											type="text" name="location" required>
									</div>
								</div>
								<div class="item form-group">
									<label for="pincode"
										class="control-label col-md-3 col-sm-3 col-xs-12">Pincode <span
										class="">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="pincode" class="form-control col-md-7 col-xs-12"
											type="text" name="pincode">
									</div>
								</div>
								<div class="item form-group">
									<label for="city"
										class="control-label col-md-3 col-sm-3 col-xs-12">City <span
										class="">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="city" class="form-control col-md-7 col-xs-12"
											type="text" name="city">
									</div>
								</div>
								<div class="item form-group">
									<label for="state"
										class="control-label col-md-3 col-sm-3 col-xs-12">State <span
										class="">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="state" class="form-control col-md-7 col-xs-12"
											type="text" name="state">
									</div>
								</div>
								<div class="item form-group">
									<label for="country"
										class="control-label col-md-3 col-sm-3 col-xs-12">Country <span
										class="">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="country" class="form-control col-md-7 col-xs-12"
											type="text" name="country">
									</div>
								</div>
								<div class="item form-group">
									<label for="role_id"
										class="control-label col-md-3 col-sm-3 col-xs-12">Role <span
										class="">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12" required>
										<select class="form-control col-md-7 col-xs-12" name="role_id"
											id="role_id" required>
											<option value="">Select</option>
											<option value="1">Admin</option>
											<option value="2">Sales</option>
											<option value="3">Ho</option>
											<option value="4">CMO</option>
										</select>

									</div>
								</div>
								<div class="item form-group">
									<label for="status"
										class="control-label col-md-3 col-sm-3 col-xs-12">Status <span
										class="">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12" required>
										<select class="form-control col-md-7 col-xs-12" name="status"
											id="status" required>
											<option value="">Select</option>
											<option value="1">Active</option>
											<option value="2">InActive</option>
										</select>
									</div>
								</div>
								<div class="ln_solid"></div>
								<div class="item form-group">
									<div class="col-md-6 col-md-offset-3" align="center">

										<button id="updateUser" type="submit" class="btn btn-success">
											Update <span id="loader"
												style="position: relative; top: 10px; right: 10px;"><i
												class="fa fa-circle-o-notch fa-spin" style="font-size: 24px"></i></span>
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>