<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH."third_party/MX/Controller.php";
class Auth extends MX_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('admin/auth/Auth_service');
		include_once './application/objects/Response.php';
		/* $this->load->helper('string');
		$this->load->library('form_validation');
		$this->load->helper('cookie'); */
	}
	/*
	 * Method: Login
	 * Description: validate dashboard login
	 */
	public function index()
	{
		
		
		/* echo $_POST['remember'];
		die(); */
		// print_r($_POST['userId']); 
		 
		$response = array();
		$userId = $_POST['userId'];	
		$password = $_POST['password'];
		
		if(isset($_POST['remember']))
		{
			//echo 888; //die();
			
			
			setcookie ("username", $userId,time()+3600);
			setcookie ("password", $password,time()+3600);
			setcookie ("remember_me", $_POST['remember'],time()+3600);
		}
		
		
		
		try {
			
			$this->form_validation->set_rules('userId', 'Email', 'trim|required|valid_email');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			
			if( $this->form_validation->run() == TRUE ){
				
				$authService = new Auth_service();
				$result = $authService->login($userId,$password);
				$response['status'] = $result->getStatus();
				$response['msg'] = $result->getMsg();
				$response['data'] = $result->getObjArray();
				echo json_encode($response);
				
			}else{
				
				$response['status'] = $result->getStatus();
				$response['msg'] = "Enter valid details.";
				$response['data'] = NULL;
				echo json_encode($response);
			}
		} catch (Exception $e) {
			$response['status'] = -1;
			$response['msg'] = $e->getMessage();
			echo json_encode($response);
		}
		
	}
	/*
	 * Method: logout
	 * Description: logout
	 */
	public function logout(){
		$this->session->sess_destroy();
		redirect('admin/admin');
	}
	
	
}
