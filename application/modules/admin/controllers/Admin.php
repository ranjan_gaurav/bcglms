<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
require APPPATH . "third_party/MX/Controller.php";
class Admin extends MX_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'admin/Admin_service' );
		$this->load->helper('common');
		include_once './application/objects/Response.php';
	}
	public function index() {
		if (is_loggedin ()) {
			
			redirect ( 'admin/dashboard' );
		} else {
			$this->load->view ( 'admin/login' );
		}
	}
	public function dashboard() {
		if (is_loggedin ()) {
			$data ['metaData'] = 'yes';
			$data ['title'] = 'LMS | Dashboard';
			$data ['keywords'] = '';
			$data ['description'] = '';
			$this->template->load ( 'admin/dashboard', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	public function forgot_password() {
		$this->load->view ( 'admin/forgot_password' );
	}
	public function reset_password() {
		$email = $this->input->post ( 'email' );
		
		$check = $this->Admin_service->checkEmail ( $email );
		
		if ($check) {
			
			$random_code = mt_rand ( 100000, 999999 ); // echo $random_code;
			$update = $this->Admin_service->updatePassword ( $random_code, $email );
			
			if ($update) {
				
				$response ['status'] = 1;
				$response ['msg'] = "Your new password has been sent to your email.";
			}
			
			$this->send_mail ( $random_code, $email );
		} 

		else {
			$response ['status'] = 0;
			$response ['msg'] = "Email id doesn't exist";
		}
		
		echo json_encode ( $response );
	}
	function send_mail($random_code, $email) {
		error_reporting ( 0 );
		
		$message = "<b>Hi, </b>" . '</br>';
		$message .= '<b>Your new temporory password is ' . $random_code . ' </b>' . '</br>';
		$message .= '<b>After login you can change your password</b>' . '</br>';
		
		$this->load->library ( 'email' );
		$this->email->set_mailtype ( 'html' );
		$this->email->from ( 'contact@lms.com' );
		$this->email->to ( $email );
		$this->email->subject ( 'forgot Password' );
		$this->email->message ( $message );
		$this->email->send ();
	}
	public function profile() {
		$userid = $this->session->userdata ( 'id' );
		$data ['user'] = $this->Admin_service->GetUserProfile ( $userid );
		
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/profile', $data );
	}
	public function update_profile() {
		$data = $this->input->post ( 'user' );
		$userid = $this->session->userdata ( 'id' );
		$update = $this->Admin_service->update_profile ( $data, $userid );
		
		if ($update) {
			$response ['status'] = 1;
			$response ['msg'] = "Your Profile has been successfully updated";
		} 

		else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		
		echo json_encode ( $response );
	}
	public function change_password() {
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/change_password', $data );
	}
	public function update_password() {
		$data = $this->input->post ( 'user' );
		$userid = $this->session->userdata ( 'id' );
		if ($data ['password'] != '' || $data ['repassword'] != '') {
			if ($data ['password'] === $data ['repassword']) {
				unset ( $data ['repassword'] );
				$data ['password'] = md5 ( $data ['password'] );
				$update = $this->Admin_service->update_password ( $data, $userid );
				if ($update) {
					$response ['status'] = 1;
					$response ['msg'] = "Your Password has been successfully changed";
				}
			} 

			else {
				$response ['status'] = 0;
				$response ['msg'] = "Password doesn't match , please enter same password";
			}
		} 

		else {
			$response ['status'] = 2;
			$response ['msg'] = "Please Enter the Password";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: view_roles
	 *         Description: view Roles
	 */
	public function view_roles() {
		$data ['detailPrivilege'] = $this->Admin_dao->detailPrivilege ();
		$data ['role'] = $this->Admin_service->getRoles ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_roles', $data );
	}
	public function getRoles() {
		$details ['data'] = $this->Admin_service->getRoles ();
		echo json_encode ( $details );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: view_privileges
	 *         Description: view privileges
	 */
	public function view_privileges() {
		$data ['user'] = $this->Admin_service->getPrivileges ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_privileges', $data );
	}
	public function getPrivileges() {
		$details ['data'] = $this->Admin_service->getPrivileges ();
		echo json_encode ( $details );
	}	 
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: updarte_role
	 *         Description: update role
	 */
	public function update_role() {
		$p=array();
		$previlege= array();
		$getUpdateRole = $_GET ['role'];
		$id = $getUpdateRole [0] ['value'];
		$title = $getUpdateRole [1] ['value'];
		$description = $getUpdateRole [2] ['value'];
		$status = $getUpdateRole [3] ['value'];
		for($i=4;$i<count($getUpdateRole);$i++)
		{
			$p['updateRolePrivilege']=$getUpdateRole[$i]['value'];
			array_push($previlege,$p);
		}
		$data = array (
				'id' => $id,
				'title' => $title,
				'description' => $description,
				'status' => $status 
		);
		$update = $this->Admin_service->update_role ( $data,$previlege );
		
		if ($update) {
			$response ['status'] = 1;
			$response ['msg'] = "Your role has been successfully updated";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: update_privilege
	 *         Description: update privilege
	 */
	public function update_privilege() {
		$getUpdatePrivilege = $_GET ['previlege'];
		$id = $getUpdatePrivilege [0] ['value'];
		$title = $getUpdatePrivilege [1] ['value'];
		$description = $getUpdatePrivilege [2] ['value'];
		$status = $getUpdatePrivilege [3] ['value'];
		$data = array (
				'id' => $id,
				'title' => $title,
				'description' => $description,
				'status' => $status 
		);
		$update = $this->Admin_service->update_privilege ( $data );
		if ($update) {
			$response ['status'] = 1;
			$response ['msg'] = "Your privilege has been successfully updated";
		} 
		else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *       @Date: 26th Nov 2016
	 *       @Method: add_role
	 *        @Description: add new role
	 */
	public function add_role() {
		$count ['detailPrivilege'] = $this->Admin_dao->detailPrivilege ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/add_role', $count );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: new_role
	 *         Description: add new role
	 */
	public function new_role() {
		$data = $this->input->post ( 'user' );
		
		if ($data ['title'] == "" || $data ['description'] == "" || $data ['status'] == "" || $data ['privilege'] == "") {
			
			$response ['status'] = 2;
			$response ['msg'] = "Please Enter some data in mandatory field";
		} else {
			
			$insert = $this->Admin_service->new_role ( $data );
			if ($insert) {
				$response ['status'] = 1;
				$response ['msg'] = "You insert new role successfully";
			} 

			else {
				$response ['status'] = 0;
				$response ['msg'] = "Something went wrong , please try again";
			}
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: add_privilege
	 *         Description: add new privilege
	 */
	public function add_privilege() {
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/add_privilege', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 29th Nov 2016
	 *         Method: new_privilege
	 *         Description: add new privilege
	 */
	public function new_privilege() 

	{
		$data = $this->input->post ( 'user' );
		if ($data ['title'] == "" || $data ['description'] == "") 
		{
			
			$response ['status'] = 2;
			$response ['msg'] = "Please Enter the title or description";
		} else 
		{
			$insert = $this->Admin_service->new_privilege ( $data );
			if ($insert) {
				$response ['status'] = 1;
				$response ['msg'] = "You insert new privilege successfully";
			} 
			else 
			{
				$response ['status'] = 0;
				$response ['msg'] = "Something went wrong , please try again";
			}
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 30th Nov 2016
	 *         Method: add user
	 *         Description: add new user and this we also count how many role are present in role list
	 */
	public function add_user() 
	{
		$data ['countRole'] = $this->Admin_dao->countRole ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/addUser', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 30 Nov 2016
	 *         Method: new_user
	 *         Description: add new user
	 */
	public function new_user() 

	{
		$response = array ();
		$data = $this->input->post ( 'user' );
		
		$addUser = $this->Admin_service->new_user ( $data );

		if ($addUser != NULL) {
			$response ['status'] = 1;
			$response ['msg'] = "You insert new user successfully";
			$response ['userId'] = $addUser;
		} 
		else {
			$response ['status'] = 0;
			$response ['msg'] = "please fill the mandatory filled";
		}
		  
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 1 Dec 2016
	 *         Method: picture
	 *         Description: picture
	 */
	public function picture() 
	{
		$data1 = $this->input->post ( 'user' );
		$config ['upload_path'] = './uploads';
		$config ['allowed_types'] = 'gif|jpg|png';
		$config ['max_size'] = 1024;
		$config ['max_width'] = 1024;
		$config ['max_height'] = 768;
		$this->load->library ( 'upload', $config );
		$sizeImage = getimagesize ( $_FILES ['fileToUpload'] ['tmp_name'] );
		$width = $sizeImage [0];
		$height = $sizeImage [1];
		if ($width > 1024 && $height > 768) 
		{
			$data = 'you upload size greater than allowed value';
			unlink ( $_FILES ['fileToUpload'] ['tmp_name'] );
			echo $data;
		} else {
			if (! $this->upload->do_upload ( 'fileToUpload' )) 
			{
				$data = "somethning is going wrong";
			} else 
			{
				$pic = array (
						'upload_data' => $this->upload->data () 
				);
			}
		}
		echo json_encode ( $pic );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: view_user
	 *         Description: view existing user
	 */
	public function view_user()
	 {
		$data ['user'] = $this->Admin_service->getUserDetails ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_user', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: getUserDetails
	 *         Description: get all existing User Details
	 */
	public function getUserDetails() {
		$details ['data'] = $this->Admin_service->getUserDetails ();
		echo json_encode ( $details );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: view_image
	 *         Description:view existing image
	 */
	public function view_image() {
		$config = array ();
		$count = $this->Admin_dao->count ();
		$config ['base_url'] = base_url () . "admin/view_image";
		$config ['total_rows'] = $count;
		$config ['per_page'] = 8; // data showing perpage
		$config ["uri_segment"] = 3;
		$choice = $config ["total_rows"] / $config ["per_page"];
		$var = $config ['per_page'];
		$config ["num_links"] = round ( $choice );
		$this->pagination->initialize ( $config );
		$page = ($this->uri->segment ( 3 )) ? $this->uri->segment ( 3 ) : 0;
		$data ['files'] = $this->Admin_service->getImage ( $var, $page );
		$data ["links"] = $this->pagination->create_links ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_image', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: upload_image
	 *         Description:upload image
	 */
	public function upload_image() {
		$desc = $_REQUEST ['description'];
		$response = array ();
		$file_element_name = 'fileToUpload';
		$uploadPath = 'uploads/files/';
		$config ['upload_path'] = $uploadPath;
		$config ['allowed_types'] = 'gif|jpg|png';
		$this->load->library ( 'upload', $config );
		if (! $this->upload->do_upload ( $file_element_name )) {
			$response ['status'] = 2;
			$response ['msg'] = $this->upload->display_errors ( '', '' );
		} else {
			$data = $this->upload->data ();
			$file_id = $this->Admin_service->addImage ( $data ['file_name'], $desc );
			
			if ($file_id) {
				$response ['status'] = 1;
				$response ['msg'] = "File successfully uploaded";
			} else {
				unlink ( $data ['full_path'] );
				$response ['status'] = 0;
				$response ['msg'] = "Something went wrong when saving the file, please try again.";
			}
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: deletePrivilege
	 *         Description: delete existing Privilege means inactive the active Privilege
	 */
	public function deletePrivilege() {
		$data = $this->input->post ( 'user' );
		$id = $data ['id'];
		$delete = $this->Admin_service->deletePrivilege ( $id );
		if ($delete) {
			$response ['status'] = 1;
			$response ['msg'] = "Your role has been successfully deleted";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: deleteRole
	 *         Description: delete existing roles means inactive the active role
	 */
	public function deleteRole() {
		$data = $this->input->post ( 'user' );
		$id = $data ['id'];
		$delete = $this->Admin_service->deleteRole ( $id );
		if ($delete) {
			$response ['status'] = 1;
			$response ['msg'] = "Your role has been successfully deleted";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: deleteUser
	 *         Description: delete existing user means inactive the active user
	 */
	public function deleteUser() {
		$data = $this->input->post ( 'user' );
		$id = $data ['id'];
		$delete = $this->Admin_service->deleteUser ( $id );
		if ($delete) {
			$response ['status'] = 1;
			$response ['msg'] = "Your role has been successfully deleted";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
	/**
	 *  @author : Nishant Singh
	 *  @Method: update_user
	 *  @Description: update existing user
	 */
	public function update_user() {
		$user= array();
		$updateDetails=array();
		$getUpdateUser = $_GET ['user'];
		for($i=0;$i<count($getUpdateUser);$i++)
		{
			$user['userUpadtedDetails']=$getUpdateUser[$i]['value'];
			array_push($updateDetails,$user);
		}
		$update = $this->Admin_service->update_user ( $updateDetails );
		if ($update) {
			$response ['status'] = 1;
			$response ['msg'] = "Your role has been successfully updated";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		
		echo json_encode ( $response );
	}
	
	public function CronJob_get()
	{
		date_default_timezone_set ( 'Asia/Kolkata' );
	
		$cur_time = date ( "Y-m-d H:i" );
		$duration = '+30 minutes';
		$Starttime = date ( 'Y-m-d H:i', strtotime ( $duration, strtotime ( $cur_time ) ) );
	
		$this->Admin_service->CronJobs($Starttime);
	
	}
}