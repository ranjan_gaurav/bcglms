<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : This Dao class is responsible of all the database related operation for API
 */
class Api_dao extends CI_Model {
	public function __construct() {
		parent::__construct ();
		include_once './application/objects/Response.php';
		date_default_timezone_set ( 'Asia/Calcutta' );
		$this->load->helper ( 'date' );
		// echo date('Y-m-d H:i:s'); die();
		error_reporting ( 0 );
	}
	
	/**
	 *
	 * @author : Anjani Kr. Gupta
	 * @method : isUserExist
	 *         @Desc : check user exist or not
	 * @return : dealers list
	 *         Date: 10th Nov 2016
	 */
	public function isUserExist($email) {
		$response = new Response ();
		try {
			
			// $user_pic = ($userdata->user_pic != '' && $userdata->user_pic != null ) ? base_url() . '/uploads/users/profile/' . $userdata->user_pic : "";
			$query = "SELECT user.*,roles.title role, branch.title branch, (SELECT b.title FROM m_branch b WHERE b.id=branch.district_id) cluster FROM user user INNER JOIN m_roles roles ON user.role_id=roles.id
						LEFT JOIN m_branch branch ON branch.id = user.branch WHERE user.role_id!=1 and user.email LIKE " . $this->db->escape ( $email ); // print_r($query); die();
			$result = $this->db->query ( $query );
			
			$arr = array ();
			
			foreach ( $result->result () as $row ) {
				$row->picture = ($row->picture != '' && $row->picture != null) ? base_url () . 'uploads/profile/' . $row->picture : "";
				array_push ( $arr, $row );
			}
			
			if ($result->num_rows () > 0) {
				$response->setStatus ( 1 );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "User Exist." );
				$response->setObjArray ( $arr );
			} else {
				$response->setStatus ( 0 );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "User does not Exist." );
				$response->setObjArray ( NULL );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Anjani Kr. Gupta
	 * @method : updateDeviceData
	 *         @Desc : update device data
	 * @return : true/false
	 *         Date: 10th Nov 2016
	 */
	public function updateDeviceData($userId, $deviceType, $deviceId, $fcmRegId, $IosID) {
		$query = "SELECT * FROM device_info WHERE deviceId LIKE " . $this->db->escape ( $deviceId );
		$result = $this->db->query ( $query );
		
		if ($result->num_rows () > 0) {
			// update
			$query = "UPDATE device_info SET fcmId=" . $this->db->escape ( $fcmRegId ) . ", reg_id_ios=" . $this->db->escape ( $IosID ) . ", deviceType=" . $this->db->escape ( $deviceType ) . ", modificationDate=now() WHERE deviceId=" . $this->db->escape ( $deviceId );
			$result = $this->db->query ( $query );
			if ($result) {
				return true;
			} else {
				return false;
			}
		} else {
			// insert
			$query = "INSERT INTO device_info (`deviceId`, `deviceType`, `fcmId`,reg_id_ios, `user_id`)  VALUES (" . $this->db->escape ( $deviceId ) . "," . $this->db->escape ( $deviceType ) . "," . $this->db->escape ( $fcmRegId ) . "," . $this->db->escape ( $IosID ) . "," . $this->db->escape ( $userId ) . ")";
			$result = $this->db->query ( $query ); // echo $this->db->last_query(); die();
			if ($result) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: addLeads
	 *         Description: add leads
	 */
	public function addLeads($data) {
		// error_reporting ( 0 );
		$response = new Response ();
		try {
			$query = "INSERT INTO leads (`name`, `mobile`, `category_id`, `product_id`, `district`, `branch`, `sourcedBy`, `address`, `emailId`, `isExistingCustomer`, `cutomerId`, businessCard,`added_by`, is_assigned,`status`,target_month, `creationDate`)  
					VALUES (" . $this->db->escape ( $data ['name'] ) . "," . $this->db->escape ( $data ['mobile'] ) . "," . $this->db->escape ( $data ['category_id'] ) . "," . $this->db->escape ( $data ['product_id'] ) . "," . $this->db->escape ( $data ['district'] ) . ",
							" . $this->db->escape ( $data ['branch'] ) . "," . $this->db->escape ( $data ['sourcedBy'] ) . "," . $this->db->escape ( $data ['address'] ) . "," . $this->db->escape ( $data ['emailId'] ) . "," . $this->db->escape ( $data ['isExistingCustomer'] ) . "," . $this->db->escape ( $data ['cutomerId'] ) . "," . $this->db->escape ( $data ['businessCard'] ) . "," . $this->db->escape ( $data ['added_by'] ) . "," . $this->db->escape ( $data ['is_assigned'] ) . "," . $this->db->escape ( $data ['status'] ) . "," . $this->db->escape ( $data ['target_month'] ) . ",now())";
			/* print_r($query); die(); */
			$result = $this->db->query ( $query );
			
			if ($result) {
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "Lead added." );
			} else {
				$response->setStatus ( false );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "Error in adding lead." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: getCategory
	 *         Description: get category
	 */
	public function getCategory() {
		$response = new Response ();
		$query = "SELECT * FROM m_category WHERE status=" . ACTIVE;
		// print_r($query); die();
		$result = $this->db->query ( $query );
		
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Category found." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "No category found." );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: getProduct
	 *         Description: get product
	 */
	public function getProduct($catId) {
		$response = new Response ();
		$query = "SELECT * FROM category_products WHERE category_id=" . $this->db->escape ( $catId ) . " AND status=" . ACTIVE;
		$result = $this->db->query ( $query );
		
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Category found." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "No category found." );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: getCluster
	 *         Description: get cluster
	 */
	public function getCluster() {
		$response = new Response ();
		$query = "SELECT id,title FROM m_district where status=" . ACTIVE;
		$result = $this->db->query ( $query );
		
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "district found." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "No district found." );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: getBranch
	 *         Description: get branch
	 */
	public function getBranch($clusterId) {
		$response = new Response ();
		$query = "SELECT id,title FROM m_branch WHERE  district_id=" . $this->db->escape ( $clusterId ) . " AND status=" . ACTIVE;
		$result = $this->db->query ( $query );
		
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Branch found." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "No branch found." );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: getStatus
	 *         Description: get all status
	 */
	public function getStatus() {
		$response = new Response ();
		
		$query = "SELECT * from m_status where id not in (1,2)";
		
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "here is all status." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "here is all status." );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: changeStatus
	 *         Description: get branch
	 */
	public function changeStatus($data) {
		$response = new Response ();
		$query = "UPDATE leads SET status=" . $this->db->escape ( $data ['statusId'] ) . " WHERE id=" . $this->db->escape ( $data ['leadId'] );
		$result = $this->db->query ( $query );
		
		if ($result) {
			
			$query = "INSERT INTO status_log (`lead_id`, `status`, `note`,`changed_by`,`created_date`)
					  VALUES (" . $this->db->escape ( $data ['leadId'] ) . "," . $this->db->escape ( $data ['statusId'] ) . "," . $this->db->escape ( $data ['note'] ) . "," . $this->db->escape ( $data ['changed_by'] ) . ",now())";
			$result = $this->db->query ( $query );
			
			if ($result) {
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "Status successfully changed." );
				$response->setObjArray ( 1 );
			} 

			else {
				$response->setStatus ( 0 );
				$response->setMsg ( "There is some problem in updating status......" );
				
				$response->setObjArray ( 0 );
				
				$response->setObjArray ( $result->result () );
			}
			
			return $response;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: addMeeting
	 *         Description: add meeting
	 */
	public function addMeeting($data) {
		$response = new Response ();
		try {
			$query = "INSERT INTO meetings (`title`, `meetingTime`, `description`,`user_id`,lead_id,`status`,`creationDate`)
					VALUES (" . $this->db->escape ( $data ['title'] ) . "," . $this->db->escape ( $data ['meetingTime'] ) . "," . $this->db->escape ( $data ['description'] ) . "," . $this->db->escape ( $data ['user_id'] ) . "," . $this->db->escape ( $data ['lead_id'] ) . "," . $this->db->escape ( $data ['status'] ) . ",now())";
			$result = $this->db->query ( $query );
			if ($result) {
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "Meeting  added." );
			} else {
				$response->setStatus ( false );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "Error in adding Meeting." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: getAllMeeting
	 *         Description: get all meeting
	 */
	public function getAllMeeting($data) {
		$response = new Response ();
		$query = "SELECT meetings.*,m_status.title as currentStatus,leads.name as leadName  from meetings
				  INNER JOIN m_status on meetings.status = m_status.id 
				  INNER JOIN leads on meetings.lead_id = leads.id 
				  where user_id=" . $data ['userId'] . " order by meetingTime DESC"; // print_r($query); die();
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "Currently no meeting is scheduled" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 22th Nov 2016
	 *         Method: getLeadsSummary
	 *         Description: Here we get how many leads are available and how many leads are processed
	 */
	public function getLeadsSummary() {
		$response = new response ();
		$query = "SELECT (SELECT count(*) FROM leads) as total_counts, m_status.title , COUNT( * ) as count FROM leads
                INNER JOIN m_status ON m_status.id = leads.status
				where leadS.status=" . CONTACTED . " or leads.status=" . QUALIFIED . " or leads.status=" . DEMO . " or leads.status=" . PROPOSAL . "
                GROUP BY leads.status";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "how many loeads are available." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		// echo '<pre>'; print_r($response); die();
		return $response;
	}
	public function getLeadsByuserID($data) {
		$response = new response ();
		$query = "SELECT leads.*,m_category.category as category_name ,
		category_products.product as product_name,
		m_district.title as district_name,m_status.title as status_name,
		m_branch.title as branchTitle FROM leads
		
				INNER JOIN m_status ON m_status.id = leads.status
				INNER JOIN  m_category ON m_category.id = leads.category_id
                INNER JOIN  category_products ON category_products.id = leads.product_id
				INNER JOIN  m_district ON m_district.id = leads.district
				LEFT JOIN  m_branch ON m_branch.id = leads.branch
				WHERE leads.added_by = $data and leads.is_assigned = 1";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Leads Found" );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "No leads Found" );
			$response->setObjArray ( $result->result () );
		}
		// echo '<pre>'; print_r($response); die();
		return $response;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 22th Nov 2016
	 *         Method: getProcessedSummary
	 *         Description: Here we get how many leads are processed
	 */
	public function getProcessedSummary() {
		$response = new response ();
		$query = "SELECT (SELECT count(*) FROM leads) as total_counts, m_status.title , COUNT( * ) as count FROM leads
            INNER JOIN m_status ON m_status.id = leads.status
			where leadS.status=" . CLOSED . " or leads.status=" . CANCELLED . " GROUP BY leads.status";
		$result = $this->db->query ( $query );
		// print_r($result->num_rows ()); die();
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "total processed lead." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 23th Nov 2016
	 *         Method: updateLeadDetails
	 *         Description: Edit or Modify the lead details
	 */
	public function updateLeadDetails($data, $id) {
		$response = new Response ();
		try {
			$this->db->set ( 'modificationDate', date ( 'Y-m-d H:i:s', now () ) );
			$this->db->where ( 'id', $id );
			$result = $this->db->update ( 'leads', $data );
			if ($result) {
				$response->setStatus ( 1 );
				$response->setMsg ( "data updated successfully" );
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "Error in upadating data........." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 24th Nov 2016
	 *         Method: updateUserDetails
	 *         Description: update meeting details
	 */
	public function updateUserDetails($data) {
		$response = new Response ();
		try {
			$this->db->set ( 'modificationDate', date ( 'Y-m-d H:i:s', now () ) );
			$this->db->where ( 'id', $data ['id'] );
			$result = $this->db->update ( 'user', $data );
			
			if ($result) {
				$response->setStatus ( 1 );
				$response->setMsg ( "data updated successfully" );
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "Error in upadating data........." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 31 march 2017
	 *         Method: updateMeetingDetails
	 *         Description: update meeting details
	 */
	public function updateMeetingDetails($data, $id) {
		// print_r($id); die();
		$response = new Response ();
		try {
			$this->db->set ( 'modificationDate', date ( 'Y-m-d H:i:s', now () ) );
			$this->db->where ( 'id', $id );
			$result = $this->db->update ( 'meetings', $data );
			// print_R($result); die();
			if ($result) {
				$this->db->select ( '*' );
				$this->db->where ( 'id', $id );
				$this->db->from ( 'meetings' );
				$res = $this->db->get ();
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "data updated successfully" );
				$response->setObjArray ( $res->result () );
			} else {
				$response->setStatus ( false );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "Error in upadating data........." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant singh
	 * @method : changePassword
	 *         @Desc : update password
	 *         @Date : 24th Nov 2016
	 *        
	 */
	public function changePassword($confirmPassword, $email) {
		try {
			$response = new Response ();
			$password = md5 ( $confirmPassword );
			$query = "UPDATE user SET password=" . $this->db->escape ( $password ) . ", modificationDate=now() WHERE email=" . $this->db->escape ( $email );
			$result = $this->db->query ( $query );
			if ($result) {
				$response->setStatus ( 1 );
			} else {
				$response->setStatus ( 0 );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getCategorywiseUserLeads
	 *         Description: get all categorywise user individual leads
	 */
	public function getCategorywiseUserLeads($data) {
		$response = new response ();
		$query = "SELECT  m_category.category , COUNT( * ) as count
            FROM leads
            INNER JOIN m_category ON m_category.id = leads.category_id where leads.added_by=" . $data ['userId'] . " and leads.category_id=1 or leads.added_by=" . $data ['userId'] . " and leads.category_id=2 or leads.added_by=" . $data ['userId'] . " and leads.category_id=3 or leads.added_by=" . $data ['userId'] . " and leads.category_id=4 
            GROUP BY leads.category_id";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getCategorywiseUserValues
	 *         Description: get all categorywise user individual values
	 */
	public function getCategorywiseUserValues($data) {
		$response = new response ();
		$query = "SELECT  m_category.category , sum(leadValue) as sum_value
            FROM leads
            INNER JOIN m_category ON m_category.id = leads.category_id where leads.added_by=" . $data ['userId'] . " and leads.category_id=1 or leads.added_by=" . $data ['userId'] . " and leads.category_id=2 or leads.added_by=" . $data ['userId'] . " and leads.category_id=3 or leads.added_by=" . $data ['userId'] . " and leads.category_id=4
            GROUP BY leads.category_id";
		// print_r($query); die();
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getProductwiseUserLeads
	 *         Description: get all productwise user individual leads
	 */
	public function getProductwiseUserLeads($data) {
		$response = new response ();
		$query = "SELECT  category_products.product , COUNT( * ) as count
            FROM leads
           INNER JOIN category_products ON category_products.id = leads.product_id where leads.added_by=" . $data ['userId'] . " and leads.product_id=1 or leads.added_by=" . $data ['userId'] . " and leads.product_id=2 or leads.added_by=" . $data ['userId'] . " and leads.product_id=3 or leads.added_by=" . $data ['userId'] . " and leads.product_id=4
           or leads.added_by=" . $data ['userId'] . " and leads.product_id=5 or leads.added_by=" . $data ['userId'] . " and leads.product_id=6 or leads.added_by=" . $data ['userId'] . " and leads.product_id=3 or leads.added_by=" . $data ['userId'] . " and leads.product_id=4 
           or leads.added_by=" . $data ['userId'] . " and leads.product_id=9 or leads.added_by=" . $data ['userId'] . " and leads.product_id=10 or leads.added_by=" . $data ['userId'] . " and leads.product_id=11 or leads.added_by=" . $data ['userId'] . " and leads.product_id=12	
           or leads.added_by=" . $data ['userId'] . " and leads.product_id=13 or leads.added_by=" . $data ['userId'] . " and leads.product_id=14 or leads.added_by=" . $data ['userId'] . " and leads.product_id=15 or leads.added_by=" . $data ['userId'] . " and leads.product_id=16	
  			GROUP BY leads.product_id";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getProductwiseUservalues
	 *         Description: get all productwise user individual Values
	 */
	public function getProductwiseUservalues($data) {
		$response = new response ();
		$query = "SELECT  category_products.product , sum(leadValue) as sum_value
            FROM leads
           INNER JOIN category_products ON category_products.id = leads.product_id where leads.added_by=" . $data ['userId'] . " and leads.product_id=1 or leads.added_by=" . $data ['userId'] . " and leads.product_id=2 or leads.added_by=" . $data ['userId'] . " and leads.product_id=3 or leads.added_by=" . $data ['userId'] . " and leads.product_id=4
           or leads.added_by=" . $data ['userId'] . " and leads.product_id=5 or leads.added_by=" . $data ['userId'] . " and leads.product_id=6 or leads.added_by=" . $data ['userId'] . " and leads.product_id=3 or leads.added_by=" . $data ['userId'] . " and leads.product_id=4
           or leads.added_by=" . $data ['userId'] . " and leads.product_id=9 or leads.added_by=" . $data ['userId'] . " and leads.product_id=10 or leads.added_by=" . $data ['userId'] . " and leads.product_id=11 or leads.added_by=" . $data ['userId'] . " and leads.product_id=12
           or leads.added_by=" . $data ['userId'] . " and leads.product_id=13 or leads.added_by=" . $data ['userId'] . " and leads.product_id=14 or leads.added_by=" . $data ['userId'] . " and leads.product_id=15 or leads.added_by=" . $data ['userId'] . " and leads.product_id=16
  			GROUP BY leads.product_id";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getLeaderboardCatageorywiseLeads
	 *         Description: Leaderboard categorywise leads
	 */
	public function getLeaderboardCatageorywiseLeads($data) {
		$response = new response ();
		$query = "select count(*) as count,u.firstname FROM user u INNER JOIN leads l ON u.id=l.added_by WHERE l.category_id=" . $data ['catId'] . " GROUP BY l.added_by
    order by count desc";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getLeaderboardCategorywiseValues
	 *         Description: Leaderboard categorywise value
	 */
	public function getLeaderboardCategorywiseValues($data) {
		$response = new response ();
		$query = "select sum(leadValue) as sum_value,u.firstname FROM user u INNER JOIN leads l ON u.id=l.added_by WHERE l.category_id=" . $data ['catId'] . " GROUP BY l.added_by
    order by sum_value desc";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 31 march 2017
	 *         Method: searchLeads
	 *         Description: searchLeads
	 */
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 31 march 2017
	 *         Method: searchLeads
	 *         Description: searchLeads
	 */
	public function searchLeads($data) 

	{
		error_reporting ( 0 );
		
		$catId = array ();
		$statusId = array ();
		
		// print_R($data); die();
		$response = new response ();
		if (! $data ['catId'] == "" && ! $data ['status'] == "" && ! $data ['from'] == "" && ! $data ['to'] == "" && ! $data ['user_id'] == "") {
			
			$catId = json_decode ( $data ['catId'] );
			$statusId = json_decode ( $data ['status'] );
			$start_date = $data ['from'];
			$end_date = $data ['to'];
			$this->db->select ( 'leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle' );
			$this->db->where ( 'DATE(leads.creationDate) BETWEEN "' . date ( 'Y-m-d', strtotime ( $start_date ) ) . '" and "' . date ( 'Y-m-d', strtotime ( $end_date ) ) . '"' );
			$this->db->where ( 'added_by', $data ['user_id'] );
			$this->db->where_in ( 'leads.category_id', $catId );
			$this->db->where_in ( 'leads.status', $statusId );
			$this->db->join ( 'm_status', 'leads.status=m_status.id' );
			$this->db->join ( 'm_category', 'leads.category_id=m_category.id' );
			$this->db->join ( 'm_branch', 'leads.branch=m_branch.id' );
			$this->db->from ( 'leads' );
			$res = $this->db->get ();
			// echo $this->db->last_query(); die();
		} else if ($data ['catId'] != "" && $data ['status'] != "" && $data ['user_id'] != "") {
			
			$catId = json_decode ( $data ['catId'] );
			$statusId = json_decode ( $data ['status'] );
			$this->db->select ( 'leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle' );
			$this->db->where_in ( 'leads.category_id', $catId );
			$this->db->where_in ( 'leads.status', $statusId );
			$this->db->where ( 'leads.added_by', $data ['user_id'] );
			$this->db->join ( 'm_status', 'leads.status=m_status.id' );
			$this->db->join ( 'm_category', 'leads.category_id=m_category.id' );
			$this->db->join ( 'm_branch', 'leads.branch=m_branch.id' );
			$this->db->from ( 'leads' );
			$res = $this->db->get ();
			// echo $this->db->last_query(); die();
		} else if (! $data ['status'] == "" && ! $data ['from'] == "" && ! $data ['to'] == "" && ! $data ['user_id'] == "") {
			
			$statusId = json_decode ( $data ['status'] );
			$start_date = $data ['from'];
			$end_date = $data ['to'];
			$this->db->select ( 'leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle' );
			$this->db->where ( 'DATE(leads.creationDate) BETWEEN "' . date ( 'Y-m-d', strtotime ( $start_date ) ) . '" and "' . date ( 'Y-m-d', strtotime ( $end_date ) ) . '"' );
			$this->db->where_in ( 'status', $statusId );
			$this->db->where ( 'leads.added_by', $data ['user_id'] );
			$this->db->join ( 'm_status', 'leads.status=m_status.id' );
			$this->db->join ( 'm_category', 'leads.category_id=m_category.id' );
			$this->db->join ( 'm_branch', 'leads.branch=m_branch.id' );
			$this->db->from ( 'leads' );
			$res = $this->db->get ();
			// echo $this->db->last_query(); die();
		} else if (! $data ['catId'] == "" && ! $data ['from'] == "" && ! $data ['to'] == "" && ! $data ['user_id'] == "") {
			
			$catId = json_decode ( $data ['catId'] );
			$start_date = $data ['from'];
			$end_date = $data ['to'];
			$this->db->select ( 'leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle' );
			$this->db->where_in ( 'category_id', $catId );
			$this->db->where ( 'DATE(leads.creationDate) BETWEEN "' . date ( 'Y-m-d', strtotime ( $start_date ) ) . '" and "' . date ( 'Y-m-d', strtotime ( $end_date ) ) . '"' );
			$this->db->where ( 'leads.added_by', $data ['user_id'] );
			$this->db->join ( 'm_status', 'leads.status=m_status.id' );
			$this->db->join ( 'm_category', 'leads.category_id=m_category.id' );
			$this->db->join ( 'm_branch', 'leads.branch=m_branch.id' );
			$this->db->from ( 'leads' );
			$res = $this->db->get ();
			// echo $this->db->last_query(); die();
		} else if (! $data ['catId'] == "" && ! $data ['user_id'] == "") {
			$catId = json_decode ( $data ['catId'] );
			$this->db->select ( 'leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle' );
			$this->db->where_in ( 'leads.category_id', $catId );
			$this->db->where ( 'leads.added_by', $data ['user_id'] );
			$this->db->join ( 'm_status', 'leads.status=m_status.id' );
			$this->db->join ( 'm_category', 'leads.category_id=m_category.id' );
			$this->db->join ( 'm_branch', 'leads.branch=m_branch.id' );
			$this->db->from ( 'leads' );
			$res = $this->db->get ();
			// echo $this->db->last_query(); die();
		} else if (! $data ['status'] == "" && ! $data ['user_id'] == "") {
			$statusId = json_decode ( $data ['status'] );
			$this->db->select ( 'leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle' );
			$this->db->where_in ( 'leads.status', $statusId );
			$this->db->where ( 'leads.added_by', $data ['user_id'] );
			$this->db->join ( 'm_status', 'leads.status=m_status.id' );
			$this->db->join ( 'm_category', 'leads.category_id=m_category.id' );
			$this->db->join ( 'm_branch', 'leads.branch=m_branch.id' );
			$this->db->from ( 'leads' );
			$res = $this->db->get ();
			// echo $this->db->last_query(); die();
		} else if (! $data ['from'] == "" && ! $data ['to'] == "" && ! $data ['user_id'] == "") {
			
			$start_date = $data ['from'];
			$end_date = $data ['to'];
			$this->db->select ( 'leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle' );
			$this->db->from ( 'leads' );
			$this->db->where ( 'DATE(leads.creationDate) BETWEEN "' . date ( 'Y-m-d', strtotime ( $start_date ) ) . '" and "' . date ( 'Y-m-d', strtotime ( $end_date ) ) . '"' );
			$this->db->where ( 'leads.added_by', $data ['user_id'] );
			$this->db->join ( 'm_status', 'leads.status=m_status.id' );
			$this->db->join ( 'm_category', 'leads.category_id=m_category.id' );
			$this->db->join ( 'm_branch', 'leads.branch=m_branch.id' );
			$res = $this->db->get ();
			// echo $this->db->last_query(); die();
		} else if (! $data ['text'] == "" && ! $data ['user_id'] == "") {
			$this->db->select ( 'leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle' );
			$this->db->like ( 'leads.name', $data ['text'] );
			$this->db->where ( 'leads.added_by', $data ['user_id'] );
			$this->db->join ( 'm_status', 'leads.status=m_status.id' );
			$this->db->join ( 'm_category', 'leads.category_id=m_category.id' );
			$this->db->join ( 'm_branch', 'leads.branch=m_branch.id' );
			$this->db->from ( 'leads' );
			$res = $this->db->get ();
			// echo $this->db->last_query(); die();
		} else if (! $data ['user_id'] == "") {
			$this->db->select ( 'leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle' );
			$this->db->where ( 'leads.added_by', $data ['user_id'] );
			$this->db->join ( 'm_status', 'leads.status=m_status.id' );
			$this->db->join ( 'm_category', 'leads.category_id=m_category.id' );
			$this->db->join ( 'm_branch', 'leads.branch=m_branch.id' );
			$this->db->from ( 'leads' );
			$res = $this->db->get ();
			// echo $this->db->last_query(); die();
		}
		
		if ($res->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Leads Found" );
			$response->setObjArray ( $res->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "Not found any data" );
			$response->setObjArray ( $res->result () );
		}
		
		// echo '<pre>'; print_r($response); die();
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 31 march 2017
	 *         Method: deleteMeeting
	 *         Description: Delete Meetings
	 */
	public function deleteMeeting($data) {
		$response = new response ();
		$this->db->where ( 'id', $data ['id'] );
		$result = $this->db->delete ( 'meetings' );
		
		if ($result) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Deleted Successfully" );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "somethins going wrong" );
		}
		
		return $response;
	}
	public function updateMeetingDetails123($data, $id) {
		// print_r($id); die();
		$response = new Response ();
		try {
			$this->db->set ( 'modificationDate', date ( 'Y-m-d H:i:s', now () ) );
			$this->db->where ( 'id', $id );
			$result = $this->db->update ( 'meetings', $data );
			// print_R($result); die();
			if ($result) {
				$this->db->select ( '*' );
				$this->db->where ( 'id', $id );
				$this->db->from ( 'meetings' );
				$res = $this->db->get ();
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "data updated successfully" );
				$response->setObjArray ( $res->result () );
			} else {
				$response->setStatus ( false );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "Error in upadating data........." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	public function getLeadByID($data) {
		$response = new response ();
		$query = "SELECT leads.*,m_category.category as category_name ,category_products.product as product_name,m_district.title as district_name,m_status.title as status_name,m_branch.title as branchTitle FROM leads
                INNER JOIN m_status ON m_status.id = leads.status
				INNER JOIN  m_category ON m_category.id = leads.category_id
				INNER JOIN  category_products ON category_products.id = leads.product_id
				LEFT JOIN  m_district ON m_district.id = leads.district
				LEFT JOIN  m_branch ON m_branch.id = leads.branch
			
		
			
				where leads.id = " . $data . "
              "; // print_r($query); die();
		$result = $this->db->query ( $query );
		
		$arr = array ();
		foreach ( $result->result () as $row ) {
			// print_r($row);
			$row->status_log = $this->getStatusLog ( $row->id );
		}
		
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Leads Found" );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "Leads not found" );
			$response->setObjArray ( $result->result () );
		}
		// echo '<pre>'; print_r($response); die();
		return $response;
	}
	public function getStatusLog($id) {
		$this->db->where ( 'lead_id', $id );
		$this->db->select ( 'm_status.title as statusname ,status_log.note ,status_log.changed_by,status_log.created_date,concat(user.firstname," ",user.lastname) as changed_by' );
		$this->db->join ( 'm_status', 'm_status.id = status_log.status' );
		$this->db->join ( 'user', 'user.id = status_log.changed_by' );
		$res = $this->db->get ( 'status_log' );
		return $res->result_array ();
	}
	public function transferLead($data) {
		$response = new Response ();
		
		$query = "UPDATE leads SET added_by=" . $this->db->escape ( $data ['receiver_id'] ) . " WHERE id=" . $this->db->escape ( $data ['lead_id'] );
		$result = $this->db->query ( $query );
		
		if ($result) {
			
			$query = "INSERT INTO transfer_log (`lead_id`, `sender_id`, `receiver_id`,`created_date`)
					  VALUES (" . $this->db->escape ( $data ['lead_id'] ) . "," . $this->db->escape ( $data ['sender_id'] ) . "," . $this->db->escape ( $data ['receiver_id'] ) . "," . "now())";
			$result = $this->db->query ( $query );
			
			if ($result) {
				$this->db->where ( 'leads.id', $data ['lead_id'] );
				$this->db->select ( 'concat(user.firstname," ", user.lastname) as name' );
				$this->db->join ( 'user', 'user.id = leads.added_by' );
				$res = $this->db->get ( 'leads' );
				
				$name = $res->row ()->name;
			}
			
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Lead successfully trasferred to " . $name );
			$response->setObjArray ( 1 );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "There is some problem in transferring lead......" );
			$response->setObjArray ( 0 );
		}
		
		return $response;
	}
	public function getAllusers() {
		$response = new response ();
		$this->db->where ( 'role_id!=', 1 );
		$result = $this->db->get ( 'user' );
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "users" );
			$response->setObjArray ( $result->result_array () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result_array () );
		}
		// echo '<pre>'; print_r($response); die();
		return $response;
		
		// return $res->result_array();
	}
	public function ListofImmediateJuniors($data) {
		$response = new response ();
		$query = "select trans_user_manager.user_id,user.*,m_roles.title as roleName,m_branch.title from trans_user_manager 
                  inner join user on user.id =  trans_user_manager.user_id
                  inner join m_roles on m_roles.id  = user.role_id
                  Left join m_branch on m_branch.id = user.branch
                  where trans_user_manager.manager_id = $data";
		$result = $this->db->query ( $query );
		
		$arr = array ();
		foreach ( $result->result_array () as $res ) {
			$res ['targets'] = $this->getTarget ( $res ['user_id'] );
			array_push ( $arr, $res );
		}
		
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "users" );
			$response->setObjArray ( $arr );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "No users avaliable currently" );
			// $response->setObjArray ( $result->result_array () );
		}
		// echo '<pre>'; print_r($response); die();
		return $response;
	}
	public function getTarget($ID) {
		$this->db->where ( 'user_id', $ID );
		$this->db->select ( "user_target.id,user_target.user_id,user_target.target,user_target.target_month,user_target.target_type as type,
    case user_target.target_type
        when '1' then 'Leads generated'
        when '2' then 'Leads converted'
        when '3' then 'Business generated'
    end as target_type" );
		$result = $this->db->get ( 'user_target' );
		
		return $result->result_array ();
	}
	public function closeMeeting($data) {
		$response = new Response ();
		try {
			$this->db->set ( 'close_time', date ( 'Y-m-d H:i:s', now () ) );
			$this->db->where ( 'id', $data ['id'] );
			unset ( $data ['id'] );
			$result = $this->db->update ( 'meetings', $data );
			
			if ($result) {
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "Meeting closed successfully" );
			} else {
				$response->setStatus ( false );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "Error in meeting closing........." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	public function UnassignedList($data) {
		$response = new Response ();
		try {
			
			$query = "SELECT leads.*,m_category.category as category_name ,
			category_products.product as product_name,
			m_district.title as district_name,m_status.title as status_name,
			m_branch.title as branchTitle FROM leads
			
			LEFT JOIN m_status ON m_status.id = leads.status
			LEFT JOIN  m_category ON m_category.id = leads.category_id
			LEFT JOIN  category_products ON category_products.id = leads.product_id
			LEFT JOIN  m_district ON m_district.id = leads.district
			LEFT JOIN  m_branch ON m_branch.id = leads.branch
			WHERE leads.added_by = $data and leads.is_assigned = '0'"; // print_r($query); die();
			$result = $this->db->query ( $query );
			// echo $this->db->last_query(); die();
			
			// $this->db->where ('added_by',$data);
			// $this->db->where ( 'is_assigned', '0' );
			// $res = $this->db->get ( 'leads' );
			// $res->result ();
			
			if ($result->num_rows () > 0) {
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "Unassigned list of leads" );
				$response->setObjArray ( $result->result () );
			} else {
				$response->setStatus ( false );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "There is no unassigned leads" );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	public function assignLeads($data) {
		$response = new Response ();
		try {
			
			// print_r($data); die();
			
			$leadids = json_decode ( $data ['leadId'] ); // print_r($leadids); die();
			unset ( $data ['leadId'] );
			$data ['added_by'] = $data ['receiverid'];
			unset ( $data ['receiverid'] ); // print_r($data); die();
			$data ['target_month'] = $data ['month'];
			unset ( $data ['month'] );
			$data ['is_assigned'] = 1;
			$this->db->set ( 'modificationDate', date ( 'Y-m-d H:i:s', now () ) );
			$this->db->where_in ( 'id', $leadids );
			$result = $this->db->update ( 'leads', $data ); // echo $this->db->last_query(); die();
			$GetDevice = $this->deviceType ( $data ['added_by'] ); // print_r($GetDevice); die();
			                                                       // $msg = $Sendername . " has created new group";
			                                                       // $Type = "LeadsAssign";
			
			foreach ( $GetDevice as $device ) {
				
				if ($device ['deviceType'] == 'android') {
					$devices = $device ['fcmId'];
					$this->Android_notification ( $data ['added_by'], $devices );
				} else {
					
					echo 6868;
					$devices = $device ['reg_id_ios'];
					$this->Ios_notification ( $data ['added_by'], $devices );
				}
			}
			if ($result) {
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "All leads successfully assigned" );
				// $response->setObjArray ( $res->result () );
			} else {
				$response->setStatus ( false );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "Error in assigning data........." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	public function setTarget($data) {
		$response = new Response ();
		try {
			
			$this->db->where ( 'user_id', $data ['user_id'] );
			$this->db->where ( 'target_month', $data ['month'] );
			// $this->db->where ('target_type',$data['target_type']);
			$row = $this->db->get ( 'user_target' );
			
			if ($row->num_rows () == 0) {
				
				$month = $data ['month'];
				// unset($data['month']);
				
				$target = ( array ) json_decode ( $data ['targets'] ); // print_r($target); die();
				
				$userID = $data ['user_id'];
				
				$result;
				foreach ( $target as $tar => $val ) {
					
					$query = "insert into user_target (user_id, target_type , target , target_month , created_by) values ($userID, $tar ,$val,$month,NOW())";
					$result = $this->db->query ( $query );
				}
				if ($result) {
					$response->setStatus ( true );
					$response->setResponseCode ( 1 );
					$response->setMsg ( "All targets successfully assigned" );
					// $response->setObjArray ( $res->result () );
				} else {
					$response->setStatus ( false );
					$response->setResponseCode ( 0 );
					$response->setMsg ( "Error in assigning data........." );
				}
			} 

			else {
				
				// $ID = $row->result(); //print_r($ID); die();
				
				$month = $data ['month'];
				// unset($data['month']);
				
				$target = ( array ) json_decode ( $data ['targets'] ); // print_r($target); die();
				
				$userID = $data ['user_id'];
				
				$result1;
				foreach ( $target as $tar => $val ) {
					
					$query1 = "update user_target SET target_type = $tar , target = $val , created_by = NOW() WHERE user_id = $userID and target_month = $month and target_type = $tar"; // print_r($query); //die();
					$result1 = $this->db->query ( $query1 );
				}
				if ($result1) {
					$response->setStatus ( true );
					$response->setResponseCode ( 1 );
					$response->setMsg ( "All targets successfully updated" );
					// $response->setObjArray ( $res->result () );
				} else {
					$response->setStatus ( false );
					$response->setResponseCode ( 0 );
					$response->setMsg ( "Error in updating data........." );
				}
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	public function getAllProducts() {
		$response = new response ();
		// $this->db->where ( 'role_id!=', 1 );
		$result = $this->db->get ( 'category_products' );
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Products list" );
			$response->setObjArray ( $result->result_array () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "No products avaliable currently" );
			// $response->setObjArray ( $result->result_array () );
		}
		// echo '<pre>'; print_r($response); die();
		return $response;
		
		// return $res->result_array();
	}
	public function deviceType($arr) {
		$this->db->where_in ( 'user_id', $arr );
		$res = $this->db->get ( 'device_info' );
		
		// echo $this->db->last_query(); die();
		return $res->result_array ();
	}
	function Android_notification($arr, $devices) {
		
		// $get = json_decode ( $devices ); print_r($devices); die();
		/*
		 * $data['data'] = array(
		 * 'type' => 'Poke',
		 *
		 * );
		 */
		// print_r($get);
		
		// $notification = GetNonActionNotificationDetails($action);
		// $message = 'Hi i am sending push notification';
		$api_key = 'AIzaSyDTxXS7krs92zYsea0qekYd4mETTNJjkcM';
		
		if (empty ( $api_key ) || count ( $get ) < 0) {
			$result = array (
					'success' => '0',
					'message' => 'api or reg id not found' 
			);
			echo json_encode ( $result );
			die ();
		}
		$registrationIDs = [ 
				"$devices" 
		]; // print_r($registrationIDs); die();
		
		$message = $msg;
		$url = 'https://fcm.googleapis.com/fcm/send';
		$resultData = array (
				"type" => 'LeadAssign',
				"title" => 'Assignment of lead' 
		);
		// "message" => $message,
		// "users" => $arr,
		// "poster_id" => $userid,
		// "group" => $GroupDetails
		
		// print_r($resultData); die();
		
		$fields = array (
				'registration_ids' => $registrationIDs,
				'data' => $resultData 
		);
		// print_r($fields); die();
		$headers = array (
				'Authorization: key=' . $api_key,
				'Content-Type: application/json' 
		);
		
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_POST, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_SSL_VERIFYPEER, false );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, json_encode ( $fields ) );
		$result = curl_exec ( $ch );
		
		// print_r ( $result );
		if ($result) {
			// print_r ( $result );
		} else {
			// die('Curl failed: ' . curl_error($ch));
		}
		curl_close ( $ch );
		
		// die();
	}
	function Ios_notification($arr, $devices) {
		
		// print_r($devices); die();
		
		// echo 777; die();
		set_time_limit ( 0 );
		
		// charset header for output
		header ( 'content-type: text/html; charset: utf-8' );
		
		// this is the pass phrase you defined when creating the key
		$passphrase = 'orange@123';
		
		/*
		 * $item = array (
		 * 'f9c68652e479664827af1c9892e04c2c6b8398561f7e34e3d9f0125f58fc94f1'
		 * );
		 */
		
		// this is where you can customize your notification
		$payload = '{"aps":{
		"alert":" One Lead has been assigned to you",
		"sound":"default",
		"type": "LeadAssign",
		"title":"Assignment of lead",
		"content-available":"1"
}}';
		
		$result = 'Start' . '<br />';
		
		// //////////////////////////////////////////////////////////////////////////////
		// start to create connection
		$ctx = stream_context_create ();
		stream_context_set_option ( $ctx, 'ssl', 'local_cert', 'devpuchcert.pem' );
		stream_context_set_option ( $ctx, 'ssl', 'passphrase', $passphrase );
		
		// echo count ( $deviceIds ) . ' devices will receive notifications.<br />';
		
		// $register_send = $val;
		$value = [ 
				"$devices" 
		]; // print_r($value); die();
		
		foreach ( $value as $val ) {
			
			// $get = json_decode ( $value );
			
			// wait for some time
			sleep ( 1 );
			
			// Open a connection to the APNS server
			$fp = stream_socket_client ( 'ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx );
			
			if (! $fp) {
				// exit ( "Failed to connect: $err $errstr" . '<br />' );
			} else {
				// echo 'Apple service is online. ' . '<br />';
			}
			
			/*
			 * $payload['aps'] = array(
			 * 'alert' => $message,
			 * 'badge' => $badge,
			 * 'sound' => 'default'
			 * );
			 * $body['data'] = $data;
			 */
			
			// Encode the payload as JSON
			// $payload = json_encode($body);
			
			// Build the binary notification
			$msg = chr ( 0 ) . pack ( 'n', 32 ) . pack ( 'H*', $val ) . pack ( 'n', strlen ( $payload ) ) . $payload;
			
			// Send it to the server
			$result = fwrite ( $fp, $msg, strlen ( $msg ) );
			
			if (! $result) {
				// echo 'Undelivered message count: ' . $item . '<br />';
			} else {
				
				// print_r($result); //die();
				// echo 'Delivered message count: ' . $item . '<br />';
			}
			
			if ($fp) {
				fclose ( $fp );
				// echo 'The connection has been closed by the client' . '<br />';
			}
		}
		
		// echo count($deviceIds) . ' devices have received notifications.<br />';
		
		// function for fixing Turkish characters
		/*
		 * function tr_to_utf($text) {
		 * $text = trim ( $text );
		 * $search = array (
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�',
		 * '�'
		 * );
		 * $replace = array (
		 * 'Ü',
		 * 'Ş',
		 * '&#286;�',
		 * 'Ç',
		 * 'İ',
		 * 'Ö',
		 * 'ü',
		 * 'ş',
		 * 'ğ',
		 * 'ç',
		 * 'ı',
		 * 'ö'
		 * );
		 * $new_text = str_replace ( $search, $replace, $text );
		 * return $new_text;
		 * }
		 */
		
		// set time limit back to a normal value
		set_time_limit ( 30 );
	}
	
	public function sumOfTargets($data)
	{
		$date1 = $data['start_date'];
		$date2 = $data['end_date'];
		$m1 = date_parse_from_format("Y-m-d", $date1);
		$m1 = $m1['month'];
		$m2 = date_parse_from_format("Y-m-d", $date2);
		$m2 = $m2['month'];
		$userID = $data['user_id'];
		
		$query = "select sum(user_target.target) as target , target_type from  user_target
                  where user_target.user_id = $userID and user_target.target_month in ($m1,$m2) 
                  group by user_target.target_type"; //print_r($query); die();
		
		$result = $this->db->query ( $query );
		
		return $result->result();
		
	}
	
	
	
	public function performanceFilter($data)
	{
		if($data['product_id'])
		{
		$prodID = json_decode($data['product_id']); //print_r($prodID); die();
		}
		else {
			$prodID = ''; 
		}
		//$prodID = implode(',',$prodID);
		$date1 = $data['start_date'];
		$date2 = $data['end_date'];
		$userID = $data['user_id'];
		
		
		if($date1 == '' && $date2=='')
		{
			$this->db->where('added_by',$userID);
			$this->db->where_in('product_id',$prodID);
		}
		
		else if($prodID == '')
		{
			$this->db->where('added_by',$userID);
			$this->db->where('creationDate >=', $date1 );
			$this->db->where('creationDate <=', $date2 );
			
		}
		
		else {
			$this->db->where('added_by',$userID);
			$this->db->where_in('product_id',$prodID);
			$this->db->where('creationDate >=', $date1 );
			$this->db->where('creationDate <=', $date2 );
				
		}
		$this->db->select("count(leads.id) as NewLeadCount ,sum(leads.leadValue) as TotalSum , leads.status");
		$this->db->from('leads');
		$this->db->group_by('leads.status');
		
		/* $query = "select count(leads.id) as NewLeadCount ,sum(leads.leadValue) as TotalSum , leads.status from leads 
where product_id in ($prodID) and creationDate between '$date1' and '$date2' and added_by = $userID
group by leads.status";
		//print_r($query); die(); */

		$result = $this->db->get(); //echo $this->db->last_query(); die();
		
		return $result->result();
	}
	
}
?>