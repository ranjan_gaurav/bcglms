<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : API management service
 * this service class is responsible of all the application logic 
 * related to API
 */
class Api_service extends CI_Model {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'api/Api_dao' );
		include_once './application/objects/Response.php';
		error_reporting(0);
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: login
	 *         Description: validate credentials
	 */
	public function login($email, $password, $deviceType, $deviceId, $fcmRegId,$IosID) {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$isUserExist = $this->isUserExist ( $email );
			if ($isUserExist->getStatus () == 1) {
				
				$user = $isUserExist->getObjArray () [0];
				if ($user->status == ACTIVE) {
					if (md5 ( $password ) == $user->password) {
						$updateDeviceData = $apiDao->updateDeviceData ( $user->id, $deviceType, $deviceId, $fcmRegId,$IosID);
						if ($updateDeviceData) {
							// echo 8989;
							$response->setStatus ( true );
							$response->setResponseCode ( 1 );
							$response->setMsg ( "Valid user" );
							$response->setObjArray ( $user );
						} else {
							$response->setStatus ( false );
							$response->setResponseCode ( 0 );
							$response->setMsg ( "Database error" );
							$response->setObjArray ( NULL );
						}
					} else {
						$response->setStatus ( false );
						$response->setResponseCode ( 0 );
						$response->setMsg ( "Invalid credentials" );
						$response->setObjArray ( NULL );
					}
				} else {
					$response->setStatus ( 2 );
					$response->setResponseCode ( 0 );
					$response->setMsg ( "Inactive user" );
					$response->setObjArray ( NULL );
				}
			} else {
				$response = $isUserExist;
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	public function isUserExist($email) {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->isUserExist ( $email );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: addLeads
	 *         Description: add leads
	 */
	public function addLeads($data) {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->addLeads ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: getCategory
	 *         Description: get category
	 */
	public function getCategory() {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getCategory ();
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: getProduct
	 *         Description: get product
	 */
	public function getProduct($catId) {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getProduct ( $catId );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: getCluster
	 *         Description: get cluster
	 */
	public function getCluster() {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getCluster ();
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: getBranch
	 *         Description: get branch
	 */
	public function getBranch($clusterId) {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getBranch ( $clusterId );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: getStatus
	 *         Description: All Status shown here
	 */
	public function getStatus() {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getStatus ();
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: changeStatus
	 *         Description: Change Status
	 */
	public function changeStatus($data) {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->changeStatus ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: addMeeting
	 *         Description: add meeting
	 */
	public function addMeeting($data) {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->addMeeting ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: getAllMeeting
	 *         Description: All meeting of a sales person shown here
	 */
	public function getAllMeeting($data) {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getAllMeeting ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 22th Nov 2016
	 *         Method: getLeadsSummary
	 *         Description: Here we get how many leads are available
	 */
	public function getLeadsSummary() {
		$response = new response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getLeadsSummary ();
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	public function getLeadsByuserID($data) {
		$response = new response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getLeadsByuserID ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 22th Nov 2016
	 *         Method: getProcessedSummary
	 *         Description: how many leads are processed
	 */
	public function getProcessedSummary() {
		$response = new response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getProcessedSummary ();
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 22th Nov 2016
	 *         Method: picture()
	 *         Description: adding image
	 */
	public function picture() {
		$config ['upload_path'] = './uploads';
		$config ['allowed_types'] = 'gif|jpg|png';
		$config ['max_size'] = 1024;
		$config ['max_width'] = 1024;
		$config ['max_height'] = 768;
		$this->load->library ( 'upload', $config );
		$sizeImage = getimagesize ( $_FILES ['businessCard'] ['tmp_name'] );
		$width = $sizeImage [0];
		$height = $sizeImage [1];
		if ($width > 1024 && $height > 768) {
			
			echo 'you upload size greater than allowed value';
			unlink ( $_FILES ['businessCard'] ['tmp_name'] );
		} elseif (! $this->upload->do_upload ( 'businessCard' )) {
			
			//echo $this->upload->display_errors ();
			// $error = array('error' => $this->upload->display_errors());
			// echo "somethning is going wrong";
		} 

		else {
			$data = array (
					'upload_data' => $this->upload->data () 
			);
			return $data;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 23th Nov 2016
	 *         Method: updateLeadDetails
	 *         Description: update details leads
	 */
	public function updateLeadDetails($data, $id) {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->updateLeadDetails ( $data, $id );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 24th Nov 2016
	 *         Method: updateMeetingDetails
	 *         Description: update meeting details
	 */
	public function updateMeetingDetails($data, $id) {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->updateMeetingDetails ( $data, $id );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 24th Nov 2016
	 *         Method: updateUserDetails
	 *         Description: update User Details
	 */
	public function updateUserDetails($data) { // print_r($data); die();
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->updateUserDetails ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : nishant singh
	 *         Date: 24th Nov 2016
	 *         Method: chaqgePassword
	 *         Description: validate credentials
	 */
	public function changePassword($password, $email, $newPassword, $confirmPassword) {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$isUserExist = $this->isUserExist ( $email );
			if ($isUserExist->getStatus () == 1) {
				$user = $isUserExist->getObjArray () [0];
				if (md5 ( $password ) == $user->password) {
					$updatePassword = $apiDao->changePassword ( $confirmPassword, $email );
					if ($updatePassword) {
						$response->setStatus ( true );
						$response->setResponseCode ( 1 );
						$response->setMsg ( "successfully password update" );
						$response->setObjArray ( $user );
					} else {
						$response->setStatus ( false );
						$response->setResponseCode ( 0 );
						$response->setMsg ( "Database error" );
						$response->setObjArray ( NULL );
					}
				} else {
					$response->setStatus ( false );
					$response->setResponseCode ( 0 );
					$response->setMsg ( "Invalid credentials" );
					$response->setObjArray ( NULL );
				}
			} else {
				$response = $isUserExist;
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getCategorywiseUserLeads
	 *         Description: get all catageorywise user individual leads
	 */
	public function getCategorywiseUserLeads($data) {
		$response = new response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getCategorywiseUserLeads ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getCategorywiseUserValues
	 *         Description: get all catageorywise user individual values
	 */
	public function getCategorywiseUserValues($data) {
		$response = new response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getCategorywiseUserValues ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getProductwiseUserLeads
	 *         Description:get all productwise user individual leads
	 */
	public function getProductwiseUserLeads($data) {
		$response = new response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getProductwiseUserLeads ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getProductwiseUserValues
	 *         Description:get all productwise user individual values
	 */
	public function getProductwiseUserValues($data) {
		$response = new response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getProductwiseUserValues ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getLeaderboardCatageorywiseLeads
	 *         Description: Leaderboard categorywise
	 */
	public function getLeaderboardCatageorywiseLeads($data) {
		$response = new response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getLeaderboardCatageorywiseLeads ( $data );
		} catch ( Exception $e ) {
			
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getLeaderboardCategorywiseValues
	 *         Description: Leaderboard productwise
	 */
	public function getLeaderboardCategorywiseValues($data) {
		$response = new response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getLeaderboardCategorywiseValues ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	public function deleteLead($leadId) {
		$response = new response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->deleteLead ( $leadId );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	public function CheckUserCredentials($data) {
		$cond = array (
				'email' => $data ['user_email'] 
		);
		$this->db->where ( $cond );
		// $this->db->select('user.*');
		// $this->db->from('user');
		$res = $this->db->get ( 'user' );
		if ($res->num_rows () > 0) {
			return true;
		} else {
			return false;
		}
	}
	public function validate_password($data) {
		error_reporting ( 0 );
		try {
			$this->load->library ( 'form_validation' );
			$config = array (
					array (
							'field' => 'user_email',
							'label' => 'User Email',
							'rules' => 'trim|required|valid_email' 
					) 
			);
			$this->form_validation->set_rules ( $config );
			if ($this->form_validation->run () == false) {
				$errors_array = '';
				foreach ( $config as $row ) {
					$field = $row ['field'];
					$error = strip_tags ( form_error ( $field ) );
					if ($error)
						$errors_array .= $error . ', ';
				}
				$message = array (
						'status' => false,
						'response_code' => '0',
						'message' => rtrim ( $errors_array, ', ' ) 
				);
			} else {
				$randcode = random_string ( 'numeric', 16 );
				$random_code = substr ( $randcode, 0, 5 );
				$pass = $random_code; // $this->generateStrongPassword('9');
				$this->load->library ( 'email' );
				$this->email->from ( 'info@orangemantra.com', 'Orange Mantra' );
				$this->email->to ( $data ['user_email'] );
				$this->email->subject ( 'Forgot Password' );
				$this->email->message ( 'Hi Your password has been changed: New Password is: ' . $pass );
				$this->email->send ();
				$user_email = $data ['user_email'];
				$updtate = array (
						'password' => md5 ( $pass ) 
				);
				$cond = array (
						'email' => $user_email 
				);
				$this->db->where ( $cond );
				$query = $this->db->update ( 'user', $updtate );
				$message = array (
						'status' => true,
						'response_code' => '1',
						'password' => $pass,
						'message' => 'Password has been changed succeessfully.Please Check you mail.Thank You!' 
				);
			}
		} catch ( Exception $ex ) {
			$message = array (
					'status' => false,
					'response_code' => '0',
					'message' => $ex->getMessage () 
			);
		}
		return $message;
	}
	public function closeMeeting($data) {
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->closeMeeting ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	public function getLeadByID ( $data )
	{
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getLeadByID ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	public function transferLead ( $data )
	{
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->transferLead ( $data );
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	public function getAllusers()
	{
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->getAllusers ();
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	public function ListofImmediateJuniors($data)
	{
		$response = new Response ();
		try {
			$apiDao = new Api_dao ();
			$response = $apiDao->ListofImmediateJuniors ($data);
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	public function searchLeads($data)
	{
		$response=new response();
		try {
			$apiDao = new Api_dao();
			$response = $apiDao->searchLeads($data);
		} catch (Exception $e) {
			$response->setStatus(-1);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
	
	public function UnassignedList ($data)
	{
		$response=new response();
		try {
			$apiDao = new Api_dao();
			$response = $apiDao->UnassignedList($data);
		} catch (Exception $e) {
			$response->setStatus(-1);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
	public function assignLeads ($data)
	{
		$response=new response();
		try {
			$apiDao = new Api_dao();
			$response = $apiDao->assignLeads($data);
		} catch (Exception $e) {
			$response->setStatus(-1);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
	
	public function getAlltargets($months,$userID)
	{
		$this->db->where('target_month',$months);
		$this->db->where('target_type',4);
		$this->db->where('user_id',$userID);
		$result = $this->db->get('user_target');
		return $result->result();
	}
	
	public function GetLeadsGeneratedCount($months,$userID)
	{
		$query = "SELECT user_target.target ,(SELECT count(leads.id) as actual   FROM user_target
                  left outer JOIN leads on user_target.user_id = leads.added_by
                  WHERE  
                  user_target.target_type = 1 and leads.added_by = $userID and leads.target_month = $months) as actual FROM user_target
                  left outer JOIN leads on user_target.user_id = leads.added_by 
                  WHERE  
                  user_target.target_type = 1 and leads.added_by = $userID  and leads.target_month = $months"; // print_r($query); die(); 
		$result = $this->db->query ( $query );
		
		if($result->num_rows() > 0 )
		{
		return $result->row();
		}
		
		else {
			return 0;
		}
	}
	
	
	public function GetMeetingCount($months,$userID)
	{
		$query = "select count(meetings.id) as totalmeetings 
                  from meetings
                  INNER JOIN leads on leads.id = meetings.lead_id
                  where meetings.user_id = $userID and meetings.status = 8 and leads.target_month = $months
                  group by leads.id";
		
	
		
		$result = $this->db->query ( $query );

		if($result->num_rows() > 0 )
		{
		return $result->row()->totalmeetings;
		}
		else {
			return 0;
		}
	}
	
	public function GetTotalLeadsValue($months,$userID)
	{
		/* $query = "select sum(leadValue) as TotalLeadValue from leads 
				  where 	target_month = $months and added_by = $userID"; */
		$query = "SELECT user_target.target, sum(leadValue) as actual   FROM user_target
		INNER JOIN leads on user_target.user_id = leads.added_by
		WHERE
		user_target.target_type = 3 and leads.added_by = $userID and leads.target_month = $months
		group by
		user_target.id";
		//print_r($query); die();
		
		$result = $this->db->query ( $query );
		if($result->num_rows() > 0 )
		{
		return $result->row();
		}
		else {
			return 0;
		}
	}
	
	public function GetLeadsConvertedCount($months,$userID)
	{
		$query = "SELECT user_target.target ,(SELECT count(leads.id) as actual   FROM user_target
                  left outer JOIN leads on user_target.user_id = leads.added_by and leads.status = 7
                  WHERE  
                  user_target.target_type = 2 and leads.added_by = $userID  and leads.status =7 and leads.target_month = $months) as actual FROM user_target
                  left outer JOIN leads on user_target.user_id = leads.added_by 
                  WHERE  
                  user_target.target_type = 2 and leads.added_by = $userID  and leads.target_month = $months  "; // print_r($query); die();
		$result = $this->db->query ( $query );
	
		return $result->row();
		
	}
	
	public function GetTotalCount($months,$userID)
	{
		$query  = "select count(leads.id) as count from m_status left join leads on m_status.id = leads.status and leads.added_by = $userID and leads.target_month =$months and leads.status not in(1,2)";
		$result = $this->db->query ( $query );
		if($result->num_rows() > 0 )
		{
		return $result->row()->count;
		}
		else {
			return 0;
		}
	}
	
	public function GetPerformance($months,$userID)
	{
		$query = "select distinct m_status.title, count(leads.id) as count
		from m_status
		left join leads on m_status.id = leads.status and leads.added_by = $userID and leads.target_month =$months
		where m_status.id not in (1,2)
		group by m_status.id";
		
		$result = $this->db->query ( $query );
		
		return $result->result();
		
	}

	public function updateMeetingDetails123($data,$id)
	{
		$response = new Response();
		try {
			$apiDao = new Api_dao();
			$response = $apiDao->updateMeetingDetails123($data,$id);
		} catch (Exception $e) {
			$response->setStatus(-1);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}

	public function GetUpcomingMeeting($userID)
	{
		$currentDateTime = date("Y-m-d h:i:sa");
		$query = "select meetings.*,leads.name as leadName,m_status.title as currentStatus from meetings
		          INNER JOIN leads on leads.id = meetings.lead_id
		          INNER JOIN m_status on meetings.status = m_status.id 
		          where user_id = $userID and meetings.meetingTime > '$currentDateTime' order by meetings.meetingTime desc limit 1";  //print_r($query); die();
		
		$result = $this->db->query ( $query );
		
		return $result->result();
	}
	
	
	public function GetUserRole($userID)
	{
		$query = " select user.*,m_roles.title from user 
    inner join m_roles on  m_roles.id = user.role_id
    where user.id = $userID";  //print_r($query); die();
		$result = $this->db->query ( $query );
		
		return $result->result();
	}
	
	public function GetOthersRole($userID,$roleID)
	{
		$query = "select m_roles.* from trans_user_manager
		INNER JOIN  user on  trans_user_manager.user_id = user.id
		INNER JOIN  m_roles on m_roles.id != user.role_id and m_roles.id!=1
		where trans_user_manager.manager_id = $userID
		UNION
		select m_roles.* from trans_user_manager
		INNER JOIN  user on  trans_user_manager.user_id = user.id
		INNER JOIN  m_roles on m_roles.id = user.role_id and m_roles.id
		where trans_user_manager.manager_id = $userID";
		
		$result = $this->db->query ( $query );
		
		return $result->result();
	}
	
	
	public function GetUserdata($userID,$role)
	{
		$query  = "select distinct user.* from m_roles
                  LEFT JOIN user on user.role_id = m_roles.id
                  LEFT JOIN trans_user_manager on trans_user_manager.manager_id = user.id
                  WHERE m_roles.id = $role";
		
		$result = $this->db->query ( $query );
		
		return $result->result();
		
		
	}
	
	public function updateprofile($id) {
		$file = $this->upload_profile_pic(); // print_r($file); die();
		if ($file['upload_data']['file_name'] != '') {
			$data = array(
					'picture' => $file['upload_data']['file_name']
			);
			$this->db->where('id', $id);
			$query = $this->db->update('user', $data); //echo $this->db->last_query(); die();
			if ($query) {
				$filepath = base_url('uploads/profile').'/'.$file['upload_data']['file_name'];
				return $filepath;
			} else {
				return false;
			}
		} else {
			//echo 5475657;
			return false;
		}
	}
	
	
	public  function upload_profile_pic() {
		
	    $config ['upload_path'] = './uploads/profile/';
		$config ['allowed_types'] = 'gif|jpg|png|jpeg';
		$config ['max_size'] = 4096;
		//$config ['max_width'] = 1024;
		//$config ['max_height'] = 768;
		$this->load->library ( 'upload', $config );
		//$sizeImage = getimagesize ( $_FILES ['businessCard'] ['tmp_name'] );
		//$width = $sizeImage [0];
		//$height = $sizeImage [1];
	     if (! $this->upload->do_upload ( 'user_pic' )) {
			
		//	echo $this->upload->display_errors ();
			// $error = array('error' => $this->upload->display_errors());
			// echo "somethning is going wrong";
		} 

		else {
			$data = array (
					'upload_data' => $this->upload->data () 
			);
			return $data;
		}
	}
	
	public function setTarget($data)
	{
		$response = new Response();
		try {
			$apiDao = new Api_dao();
			$response = $apiDao->setTarget($data);
		} catch (Exception $e) {
			$response->setStatus(-1);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
	public function getAllProducts()
	{
		$response = new Response();
		try {
			$apiDao = new Api_dao();
			$response = $apiDao->getAllProducts();
		} catch (Exception $e) {
			$response->setStatus(-1);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
	public function GetUserRole123 ( $userID )
	{
		$query = "select role_id from user where id = $userID";
		$result = $this->db->query ( $query );
		
		return $result->row()->role_id;
	}
	
	
	public function getUserTree ( $userID )
	{
		$query = "select trans_user_manager.user_id,m_roles.title,user.* from trans_user_manager 
inner join user on user.id =  trans_user_manager.user_id
inner join m_roles on m_roles.id = user.role_id
where trans_user_manager.manager_id = $userID";
		$result = $this->db->query ( $query );
		
		return $result->result_array();
	}
	
	public function getJuniorsList ( $JID )
	{
		
		//print_r($userID); die();
		//$id = $userID['user_id'];
		$query = "select trans_user_manager.user_id,m_roles.title,user.* from trans_user_manager
		inner join user on user.id =  trans_user_manager.user_id
		inner join m_roles on m_roles.id = user.role_id
		where trans_user_manager.manager_id = $JID "; //print_r($query); //die();
	
		$result = $this->db->query ( $query );
		
		
		return $result->result_array();
	}
	
	public function CronJobs($Starttime)
	{
		$query = "SELECT * FROM meetings WHERE meetingTime LIKE  '%$Starttime%'";  //print_r($query); die();
		$result = $this->db->query ( $query );
	}
	
	
	public function performanceFilter ($data)
	{
		$response = new Response();
		try {
			$apiDao = new Api_dao();
			$response = $apiDao->performanceFilter($data);
		} catch (Exception $e) {
			$response->setStatus(-1);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
	
	public function sumOfTargets ($data)
	{
		$response = new Response();
		try {
			$apiDao = new Api_dao();
			$response = $apiDao->sumOfTargets($data);
		} catch (Exception $e) {
			$response->setStatus(-1);
			$response->setMsg($e->getMessage());
			$response->setError($e->getMessage());
			log_message("Error", $e->getMessage() . " :: in file:" . $e->getFile() . ",at line:" . $e->getLine());
		}
		return $response;
	}
	
}


?>