<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : This Dao class is responsible of all the database related operation for API
 */
class Api_dao extends CI_Model {
	public function __construct() {
		parent::__construct ();
		include_once './application/objects/Response.php';
		date_default_timezone_set ( 'Asia/Calcutta' );
		$this->load->helper ( 'date' );
		// echo date('Y-m-d H:i:s'); die();
		error_reporting(0);
	}
	
	/**
	 *
	 * @author : Anjani Kr. Gupta
	 * @method : isUserExist
	 *         @Desc : check user exist or not
	 * @return : dealers list
	 *         Date: 10th Nov 2016
	 */
	public function isUserExist($email) {
		$response = new Response ();
		try {
			$query = "SELECT user.*, roles.title role, branch.title branch, (SELECT b.title FROM m_branch b WHERE b.id=branch.district_id) cluster FROM user user INNER JOIN m_roles roles ON user.role_id=roles.id
						LEFT JOIN m_branch branch ON branch.id = user.branch WHERE user.role_id!=1 and user.email LIKE " . $this->db->escape ( $email ); // print_r($query); die();
			$result = $this->db->query ( $query );
			
			if ($result->num_rows () > 0) {
				$response->setStatus ( 1 );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "User Exist." );
				$response->setObjArray ( $result->result () );
			} else {
				$response->setStatus ( 0 );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "User does not Exist." );
				$response->setObjArray ( NULL );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Anjani Kr. Gupta
	 * @method : updateDeviceData
	 *         @Desc : update device data
	 * @return : true/false
	 *         Date: 10th Nov 2016
	 */
	public function updateDeviceData($userId, $deviceType, $deviceId, $fcmRegId) {
		$query = "SELECT * FROM device_info WHERE deviceId LIKE " . $this->db->escape ( $deviceId );
		$result = $this->db->query ( $query );
		
		if ($result->num_rows () > 0) {
			// update
			$query = "UPDATE device_info SET fcmId=" . $this->db->escape ( $fcmRegId ) . ", deviceType=" . $this->db->escape ( $deviceType ) . ", modificationDate=now() WHERE deviceId=" . $this->db->escape ( $deviceId );
			$result = $this->db->query ( $query );
			if ($result) {
				return true;
			} else {
				return false;
			}
		} else {
			// insert
			$query = "INSERT INTO device_info (`deviceId`, `deviceType`, `fcmId`, `user_id`)  VALUES (" . $this->db->escape ( $deviceId ) . "," . $this->db->escape ( $deviceType ) . "," . $this->db->escape ( $fcmRegId ) . "," . $this->db->escape ( $userId ) . ")";
			$result = $this->db->query ( $query );
			if ($result) {
				return true;
			} else {
				return false;
			}
		}
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: addLeads
	 *         Description: add leads
	 */
	public function addLeads($data) {
		error_reporting ( 0 );
		
		$response = new Response ();
		try {
			$query = "INSERT INTO leads (`name`, `mobile`, `category_id`, `product_id`, `district`, `branch`, `sourcedBy`, `address`, `emailId`, `isExistingCustomer`, `cutomerId`, `added_by`, `status`,target_month, `creationDate`)  
					VALUES (" . $this->db->escape ( $data ['name'] ) . "," . $this->db->escape ( $data ['mobile'] ) . "," . $this->db->escape ( $data ['category_id'] ) . "," . $this->db->escape ( $data ['product_id'] ) . "," . $this->db->escape ( $data ['district'] ) . ",
							" . $this->db->escape ( $data ['branch'] ) . "," . $this->db->escape ( $data ['sourcedBy'] ) . "," . $this->db->escape ( $data ['address'] ) . "," . $this->db->escape ( $data ['emailId'] ) . "," . $this->db->escape ( $data ['isExistingCustomer'] ) . "," . $this->db->escape ( $data ['cutomerId'] ) . "," . $this->db->escape ( $data ['added_by'] ) . "," . $this->db->escape ( $data ['status'] ) . "," . $this->db->escape ( $data ['target_month'] ) . ",now())";
			/* print_r($query); die(); */
			$result = $this->db->query ( $query );
			
			if ($result) {
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "Lead added." );
			} else {
				$response->setStatus ( false );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "Error in adding lead." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: getCategory
	 *         Description: get category
	 */
	public function getCategory() {
		$response = new Response ();
		$query = "SELECT * FROM m_category WHERE status=" . ACTIVE;
		// print_r($query); die();
		$result = $this->db->query ( $query );
		
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Category found." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "No category found." );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: getProduct
	 *         Description: get product
	 */
	public function getProduct($catId) {
		$response = new Response ();
		$query = "SELECT * FROM category_products WHERE category_id=" . $this->db->escape ( $catId ) . " AND status=" . ACTIVE;
		$result = $this->db->query ( $query );
		
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Category found." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "No category found." );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: getCluster
	 *         Description: get cluster
	 */
	public function getCluster() {
		$response = new Response ();
		$query = "SELECT id,title FROM m_district where status=" . ACTIVE;
		$result = $this->db->query ( $query );
		
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "district found." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "No district found." );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: getBranch
	 *         Description: get branch
	 */
	public function getBranch($clusterId) {
		$response = new Response ();
		$query = "SELECT id,title FROM m_branch WHERE  district_id=" . $this->db->escape ( $clusterId ) . " AND status=" . ACTIVE;
		$result = $this->db->query ( $query );
		
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Branch found." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "No branch found." );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: getStatus
	 *         Description: get all status
	 */
	public function getStatus() {
		$response = new Response ();
		
		$query = "SELECT * from m_status where id not in (1,2)";
		
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "here is all status." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "here is all status." );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: changeStatus
	 *         Description: get branch
	 */
	public function changeStatus($data) {
		$response = new Response ();
		$query = "UPDATE leads SET status=" . $this->db->escape ( $data ['statusId'] ) . " WHERE id=" . $this->db->escape ( $data ['leadId'] );
		$result = $this->db->query ( $query );
		
		if ($result) {
			
			$query = "INSERT INTO status_log (`lead_id`, `status`, `note`,`changed_by`,`created_date`)
					  VALUES (" . $this->db->escape ( $data ['leadId'] ) . "," . $this->db->escape ( $data ['statusId'] ) . "," . $this->db->escape ( $data ['note'] ) . "," . $this->db->escape ( $data ['changed_by'] ) . ",now())";
			$result = $this->db->query ( $query );
			
			if ($result) {
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "Status successfully changed." );
				$response->setObjArray ( 1 );
			} 

			else {
				$response->setStatus ( 0 );
				$response->setMsg ( "There is some problem in updating status......" );
				
				$response->setObjArray ( 0 );
				
				$response->setObjArray ( $result->result () );
			}
			
			return $response;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: addMeeting
	 *         Description: add meeting
	 */
	public function addMeeting($data) {
		$response = new Response ();
		try {
			$query = "INSERT INTO meetings (`title`, `meetingTime`, `description`,`user_id`,lead_id,`status`,`creationDate`)
					VALUES (" . $this->db->escape ( $data ['title'] ) . "," . $this->db->escape ( $data ['meetingTime'] ) . "," . $this->db->escape ( $data ['description'] ) . "," . $this->db->escape ( $data ['user_id'] ) ."," . $this->db->escape ( $data ['lead_id'] ) . "," . $this->db->escape ( $data ['status'] ) . ",now())";
			$result = $this->db->query ( $query );
			if ($result) {
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "Meeting  added." );
			} else {
				$response->setStatus ( false );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "Error in adding Meeting." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: getAllMeeting
	 *         Description: get all meeting
	 */
	public function getAllMeeting($data) {
		$response = new Response ();
		$query = "SELECT meetings.*,m_status.title as currentStatus,leads.name as leadName  from meetings
				  INNER JOIN m_status on meetings.status = m_status.id 
				  INNER JOIN leads on meetings.lead_id = leads.id 
				  where user_id=" . $data ['userId'] ." order by meetingTime DESC"; //print_r($query); die();
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
				$response->setResponseCode ( 0 );
			$response->setMsg ( "something went wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 22th Nov 2016
	 *         Method: getLeadsSummary
	 *         Description: Here we get how many leads are available and how many leads are processed
	 */
	public function getLeadsSummary() {
		$response = new response ();
		$query = "SELECT (SELECT count(*) FROM leads) as total_counts, m_status.title , COUNT( * ) as count FROM leads
                INNER JOIN m_status ON m_status.id = leads.status
				where leadS.status=" . CONTACTED . " or leads.status=" . QUALIFIED . " or leads.status=" . DEMO . " or leads.status=" . PROPOSAL . "
                GROUP BY leads.status";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "how many loeads are available." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		// echo '<pre>'; print_r($response); die();
		return $response;
	}
	public function getLeadsByuserID($data) {
		$response = new response ();
		$query = "SELECT leads.*,m_category.category as category_name ,
		category_products.product as product_name,
		m_district.title as district_name,m_status.title as status_name,
		m_branch.title as branchTitle FROM leads
		
				INNER JOIN m_status ON m_status.id = leads.status
				INNER JOIN  m_category ON m_category.id = leads.category_id
                INNER JOIN  category_products ON category_products.id = leads.product_id
				INNER JOIN  m_district ON m_district.id = leads.district
				INNER JOIN  m_branch ON m_branch.id = leads.branch";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Leads Found" );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		// echo '<pre>'; print_r($response); die();
		return $response;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 22th Nov 2016
	 *         Method: getProcessedSummary
	 *         Description: Here we get how many leads are processed
	 */
	public function getProcessedSummary() {
		$response = new response ();
		$query = "SELECT (SELECT count(*) FROM leads) as total_counts, m_status.title , COUNT( * ) as count FROM leads
            INNER JOIN m_status ON m_status.id = leads.status
			where leadS.status=" . CLOSED . " or leads.status=" . CANCELLED . " GROUP BY leads.status";
		$result = $this->db->query ( $query );
		// print_r($result->num_rows ()); die();
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			$response->setMsg ( "total processed lead." );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 23th Nov 2016
	 *         Method: updateLeadDetails
	 *         Description: Edit or Modify the lead details
	 */
	public function updateLeadDetails($data, $id) {
		$response = new Response ();
		try {
			$this->db->set ( 'modificationDate', date ( 'Y-m-d H:i:s', now () ) );
			$this->db->where ( 'id', $id );
			$result = $this->db->update ( 'leads', $data );
			if ($result) {
				$response->setStatus ( 1 );
				$response->setMsg ( "data updated successfully" );
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "Error in upadating data........." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 24th Nov 2016
	 *         Method: updateUserDetails
	 *         Description: update meeting details
	 */
	public function updateUserDetails($data) {
		$response = new Response ();
		try {
			$this->db->set ( 'modificationDate', date ( 'Y-m-d H:i:s', now () ) );
			$this->db->where ( 'id', $data ['id'] );
			$result = $this->db->update ( 'user', $data );
			
			if ($result) {
				$response->setStatus ( 1 );
				$response->setMsg ( "data updated successfully" );
			} else {
				$response->setStatus ( 0 );
				$response->setMsg ( "Error in upadating data........." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 31 march 2017
	 *         Method: updateMeetingDetails
	 *         Description: update meeting details
	 */
	public function updateMeetingDetails($data, $id) {
		// print_r($id); die();
		$response = new Response ();
		try {
			$this->db->set ( 'modificationDate', date ( 'Y-m-d H:i:s', now () ) );
			$this->db->where ( 'id', $id );
			$result = $this->db->update ( 'meetings', $data );
			// print_R($result); die();
			if ($result) {
				$this->db->select ( '*' );
				$this->db->where ( 'id', $id );
				$this->db->from ( 'meetings' );
				$res = $this->db->get ();
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "data updated successfully" );
				$response->setObjArray ( $res->result () );
			} else {
				$response->setStatus ( false );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "Error in upadating data........." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant singh
	 * @method : changePassword
	 *         @Desc : update password
	 *         @Date : 24th Nov 2016
	 *        
	 */
	public function changePassword($confirmPassword, $email) {
		try {
			$response = new Response ();
			$password = md5 ( $confirmPassword );
			$query = "UPDATE user SET password=" . $this->db->escape ( $password ) . ", modificationDate=now() WHERE email=" . $this->db->escape ( $email );
			$result = $this->db->query ( $query );
			if ($result) {
				$response->setStatus ( 1 );
			} else {
				$response->setStatus ( 0 );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getCategorywiseUserLeads
	 *         Description: get all categorywise user individual leads
	 */
	public function getCategorywiseUserLeads($data) {
		$response = new response ();
		$query = "SELECT  m_category.category , COUNT( * ) as count
            FROM leads
            INNER JOIN m_category ON m_category.id = leads.category_id where leads.added_by=" . $data ['userId'] . " and leads.category_id=1 or leads.added_by=" . $data ['userId'] . " and leads.category_id=2 or leads.added_by=" . $data ['userId'] . " and leads.category_id=3 or leads.added_by=" . $data ['userId'] . " and leads.category_id=4 
            GROUP BY leads.category_id";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getCategorywiseUserValues
	 *         Description: get all categorywise user individual values
	 */
	public function getCategorywiseUserValues($data) {
		$response = new response ();
		$query = "SELECT  m_category.category , sum(leadValue) as sum_value
            FROM leads
            INNER JOIN m_category ON m_category.id = leads.category_id where leads.added_by=" . $data ['userId'] . " and leads.category_id=1 or leads.added_by=" . $data ['userId'] . " and leads.category_id=2 or leads.added_by=" . $data ['userId'] . " and leads.category_id=3 or leads.added_by=" . $data ['userId'] . " and leads.category_id=4
            GROUP BY leads.category_id";
		// print_r($query); die();
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getProductwiseUserLeads
	 *         Description: get all productwise user individual leads
	 */
	public function getProductwiseUserLeads($data) {
		$response = new response ();
		$query = "SELECT  category_products.product , COUNT( * ) as count
            FROM leads
           INNER JOIN category_products ON category_products.id = leads.product_id where leads.added_by=" . $data ['userId'] . " and leads.product_id=1 or leads.added_by=" . $data ['userId'] . " and leads.product_id=2 or leads.added_by=" . $data ['userId'] . " and leads.product_id=3 or leads.added_by=" . $data ['userId'] . " and leads.product_id=4
           or leads.added_by=" . $data ['userId'] . " and leads.product_id=5 or leads.added_by=" . $data ['userId'] . " and leads.product_id=6 or leads.added_by=" . $data ['userId'] . " and leads.product_id=3 or leads.added_by=" . $data ['userId'] . " and leads.product_id=4 
           or leads.added_by=" . $data ['userId'] . " and leads.product_id=9 or leads.added_by=" . $data ['userId'] . " and leads.product_id=10 or leads.added_by=" . $data ['userId'] . " and leads.product_id=11 or leads.added_by=" . $data ['userId'] . " and leads.product_id=12	
           or leads.added_by=" . $data ['userId'] . " and leads.product_id=13 or leads.added_by=" . $data ['userId'] . " and leads.product_id=14 or leads.added_by=" . $data ['userId'] . " and leads.product_id=15 or leads.added_by=" . $data ['userId'] . " and leads.product_id=16	
  			GROUP BY leads.product_id";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getProductwiseUservalues
	 *         Description: get all productwise user individual Values
	 */
	public function getProductwiseUservalues($data) {
		$response = new response ();
		$query = "SELECT  category_products.product , sum(leadValue) as sum_value
            FROM leads
           INNER JOIN category_products ON category_products.id = leads.product_id where leads.added_by=" . $data ['userId'] . " and leads.product_id=1 or leads.added_by=" . $data ['userId'] . " and leads.product_id=2 or leads.added_by=" . $data ['userId'] . " and leads.product_id=3 or leads.added_by=" . $data ['userId'] . " and leads.product_id=4
           or leads.added_by=" . $data ['userId'] . " and leads.product_id=5 or leads.added_by=" . $data ['userId'] . " and leads.product_id=6 or leads.added_by=" . $data ['userId'] . " and leads.product_id=3 or leads.added_by=" . $data ['userId'] . " and leads.product_id=4
           or leads.added_by=" . $data ['userId'] . " and leads.product_id=9 or leads.added_by=" . $data ['userId'] . " and leads.product_id=10 or leads.added_by=" . $data ['userId'] . " and leads.product_id=11 or leads.added_by=" . $data ['userId'] . " and leads.product_id=12
           or leads.added_by=" . $data ['userId'] . " and leads.product_id=13 or leads.added_by=" . $data ['userId'] . " and leads.product_id=14 or leads.added_by=" . $data ['userId'] . " and leads.product_id=15 or leads.added_by=" . $data ['userId'] . " and leads.product_id=16
  			GROUP BY leads.product_id";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getLeaderboardCatageorywiseLeads
	 *         Description: Leaderboard categorywise leads
	 */
	public function getLeaderboardCatageorywiseLeads($data) {
		$response = new response ();
		$query = "select count(*) as count,u.firstname FROM user u INNER JOIN leads l ON u.id=l.added_by WHERE l.category_id=" . $data ['catId'] . " GROUP BY l.added_by
    order by count desc";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getLeaderboardCategorywiseValues
	 *         Description: Leaderboard categorywise value
	 */
	public function getLeaderboardCategorywiseValues($data) {
		$response = new response ();
		$query = "select sum(leadValue) as sum_value,u.firstname FROM user u INNER JOIN leads l ON u.id=l.added_by WHERE l.category_id=" . $data ['catId'] . " GROUP BY l.added_by
    order by sum_value desc";
		$result = $this->db->query ( $query );
		if ($result->num_rows () > 0) {
			$response->setStatus ( 1 );
			
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		
		return $response;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 31 march 2017
	 *         Method: searchLeads
	 *         Description: searchLeads
	 */

/**
 * @author : Nishant Singh
 * Date: 31 march 2017
 * Method: searchLeads
 * Description: searchLeads
 */
public function searchLeads($data)

{ error_reporting(0);
	
	$catId= array();
    $statusId=array();
	
	
	//print_R($data); die();
	$response = new response();
	if(!$data['catId']=="" && !$data['status']==""&& !$data['from']=="" && !$data['to']=="" && !$data['user_id']=="")
	{ 
		
		$catId=json_decode($data['catId']);
		$statusId=json_decode($data['status']);
		$start_date=$data['from'];
		$end_date=$data['to'];
		$this->db->select('leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle');
		$this->db->where('DATE(leads.creationDate) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
		$this->db->where('added_by',$data['user_id']);
		$this->db->where_in('leads.category_id',$catId);
		$this->db->where_in('leads.status',$statusId);
		$this->db->join('m_status','leads.status=m_status.id');
		$this->db->join('m_category','leads.category_id=m_category.id');
		$this->db->join('m_branch','leads.branch=m_branch.id');
		$this->db->from('leads');
		$res=$this->db->get();
		//echo $this->db->last_query(); die();
	}
	 else if($data['catId']!=""  && $data['status']!="" && $data['user_id']!="")
	{
		
		$catId=json_decode($data['catId']);
		$statusId=json_decode($data['status']);
		$this->db->select('leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle');
		$this->db->where_in('leads.category_id',$catId);
		$this->db->where_in('leads.status',$statusId);
		$this->db->where('leads.added_by',$data['user_id']);
		$this->db->join('m_status','leads.status=m_status.id');
		$this->db->join('m_category','leads.category_id=m_category.id');
		$this->db->join('m_branch','leads.branch=m_branch.id');
		$this->db->from('leads');
		$res=$this->db->get();
		//echo $this->db->last_query(); die();
	} 
	else if(!$data['status']==""&& !$data['from']=="" && !$data['to']=="" && !$data['user_id']=="")
	{
		
		$statusId=json_decode($data['status']);
		$start_date=$data['from'];
		$end_date=$data['to'];
		$this->db->select('leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle');
	    $this->db->where('DATE(leads.creationDate) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
		$this->db->where_in('status',$statusId);
		$this->db->where('leads.added_by',$data['user_id']);
		$this->db->join('m_status','leads.status=m_status.id');
		$this->db->join('m_category','leads.category_id=m_category.id');
		$this->db->join('m_branch','leads.branch=m_branch.id');
		$this->db->from('leads');
		$res=$this->db->get();
		//echo $this->db->last_query(); die();
	}
	 else if(!$data['catId']=="" && !$data['from']=="" && !$data['to']==""&& !$data['user_id']=="") {
	 	
	 	$catId=json_decode($data['catId']);
	 	$start_date=$data['from'];
	 	$end_date=$data['to'];
		$this->db->select('leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle');
		$this->db->where_in('category_id',$catId);
		$this->db->where('DATE(leads.creationDate) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
		$this->db->where('leads.added_by',$data['user_id']);
		$this->db->join('m_status','leads.status=m_status.id');
		$this->db->join('m_category','leads.category_id=m_category.id');
		$this->db->join('m_branch','leads.branch=m_branch.id');
		$this->db->from('leads');
		$res=$this->db->get();
		//echo $this->db->last_query(); die();
	}  
	else if(!$data['catId']==""&& !$data['user_id']=="") {
		$catId=json_decode($data['catId']);
		$this->db->select('leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle');
		$this->db->where_in('leads.category_id',$catId);
		$this->db->where('leads.added_by',$data['user_id']);
		$this->db->join('m_status','leads.status=m_status.id');
		$this->db->join('m_category','leads.category_id=m_category.id');
		$this->db->join('m_branch','leads.branch=m_branch.id');
		$this->db->from('leads');
		$res=$this->db->get();
		//echo $this->db->last_query(); die();
	}
	 else if(!$data['status']==""&& !$data['user_id']=="") {
	 	$statusId=json_decode($data['status']);
		$this->db->select('leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle');
		$this->db->where_in('leads.status',$statusId);
		$this->db->where('leads.added_by',$data['user_id']);
		$this->db->join('m_status','leads.status=m_status.id');
		$this->db->join('m_category','leads.category_id=m_category.id');
		$this->db->join('m_branch','leads.branch=m_branch.id');
		$this->db->from('leads');
		$res=$this->db->get();
		//echo $this->db->last_query(); die();
	} 
	else if(!$data['from']=="" && !$data['to']==""&& !$data['user_id']=="") {
		
		$start_date=$data['from'];
		$end_date=$data['to'];
		$this->db->select('leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle');
		$this->db->from('leads');
		$this->db->where('DATE(leads.creationDate) BETWEEN "'. date('Y-m-d', strtotime($start_date)). '" and "'. date('Y-m-d', strtotime($end_date)).'"');
		$this->db->where('leads.added_by',$data['user_id']);
		$this->db->join('m_status','leads.status=m_status.id');
		$this->db->join('m_category','leads.category_id=m_category.id');
		$this->db->join('m_branch','leads.branch=m_branch.id');
		$res=$this->db->get();
	//	echo $this->db->last_query(); die();
	}
	else if(!$data['text']==""&& !$data['user_id']=="") {
		$this->db->select('leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle');
		$this->db->like('leads.name',$data['text']);
		$this->db->where('leads.added_by',$data['user_id']);
		$this->db->join('m_status','leads.status=m_status.id');
		$this->db->join('m_category','leads.category_id=m_category.id');
		$this->db->join('m_branch','leads.branch=m_branch.id');
		$this->db->from('leads');
		$res=$this->db->get();
		//echo $this->db->last_query(); die();
	}else if(!$data['user_id']=="") {
		$this->db->select('leads.*,m_status.title as status_name,m_category.category as category_name,m_branch.title as branchTitle');
		$this->db->where('leads.added_by',$data['user_id']);
		$this->db->join('m_status','leads.status=m_status.id');
		$this->db->join('m_category','leads.category_id=m_category.id');
		$this->db->join('m_branch','leads.branch=m_branch.id');
		$this->db->from('leads');
		$res=$this->db->get();
		//echo $this->db->last_query(); die();
	}
		
		
               
	if ($res->num_rows () > 0) {
		$response->setStatus ( true );
		$response->setResponseCode(1);
		$response->setMsg ( "Leads Found" );
		$response->setObjArray ( $res->result() );
	} else {
		$response->setStatus ( false );
		$response->setResponseCode(0);
		$response->setMsg ( "Not found any data" );
		$response->setObjArray ( $res->result() );
	}
	
	
	//	echo '<pre>'; print_r($response); die();
	return $response;
}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 31 march 2017
	 *         Method: deleteMeeting
	 *         Description: Delete Meetings
	 */
	public function deleteMeeting($data) {
		$response = new response ();
		$this->db->where ( 'id', $data ['id'] );
		$result = $this->db->delete ( 'meetings' );
		
		if ($result) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Deleted Successfully" );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "somethins going wrong" );
		}
		
		return $response;
	}
	public function updateMeetingDetails123($data, $id) {
		// print_r($id); die();
		$response = new Response ();
		try {
			$this->db->set ( 'modificationDate', date ( 'Y-m-d H:i:s', now () ) );
			$this->db->where ( 'id', $id );
			$result = $this->db->update ( 'meetings', $data );
			// print_R($result); die();
			if ($result) {
				$this->db->select ( '*' );
				$this->db->where ( 'id', $id );
				$this->db->from ( 'meetings' );
				$res = $this->db->get ();
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "data updated successfully" );
				$response->setObjArray ( $res->result () );
			} else {
				$response->setStatus ( false );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "Error in upadating data........." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
	public function getLeadByID($data) {
		$response = new response ();
		$query = "SELECT leads.*,m_category.category as category_name ,category_products.product as product_name,m_district.title as district_name,m_status.title as status_name,m_branch.title as branchTitle FROM leads
                INNER JOIN m_status ON m_status.id = leads.status
				INNER JOIN  m_category ON m_category.id = leads.category_id
				INNER JOIN  category_products ON category_products.id = leads.product_id
				INNER JOIN  m_district ON m_district.id = leads.district
				INNER JOIN  m_branch ON m_branch.id = leads.branch
			
		
			
				where leads.id = " . $data . "
              "; // print_r($query); die();
		$result = $this->db->query ( $query );
		
		$arr = array ();
		foreach ( $result->result () as $row ) {
			// print_r($row);
			$row->status_log = $this->getStatusLog ( $row->id );
		}
		
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Leads Found" );
			$response->setObjArray ( $result->result () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result () );
		}
		// echo '<pre>'; print_r($response); die();
		return $response;
	}
	public function getStatusLog($id) {
		$this->db->where ( 'lead_id', $id );
		$this->db->select ( 'm_status.title as statusname ,status_log.note ,status_log.changed_by,status_log.created_date,concat(user.firstname," ",user.lastname) as changed_by' );
		$this->db->join ( 'm_status', 'm_status.id = status_log.status' );
		$this->db->join ( 'user', 'user.id = status_log.changed_by' );
		$res = $this->db->get ( 'status_log' );
		return $res->result_array ();
	}
	public function transferLead($data) {
		$response = new Response ();
		
		$query = "UPDATE leads SET added_by=" . $this->db->escape ( $data ['receiver_id'] ) . " WHERE id=" . $this->db->escape ( $data ['lead_id'] );
		$result = $this->db->query ( $query );
		
		if ($result) {
			
			$query = "INSERT INTO transfer_log (`lead_id`, `sender_id`, `receiver_id`,`created_date`)
					  VALUES (" . $this->db->escape ( $data ['lead_id'] ) . "," . $this->db->escape ( $data ['sender_id'] ) . "," . $this->db->escape ( $data ['receiver_id'] ) . "," . "now())";
			$result = $this->db->query ( $query );
			
			if ($result) {
				$this->db->where ( 'leads.id', $data ['lead_id'] );
				$this->db->select ( 'concat(user.firstname," ", user.lastname) as name' );
				$this->db->join ( 'user', 'user.id = leads.added_by' );
				$res = $this->db->get ( 'leads' );
				
				$name = $res->row ()->name;
			}
			
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "Lead successfully trasferred to " . $name );
			$response->setObjArray ( 1 );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "There is some problem in transferring lead......" );
			$response->setObjArray ( 0 );
		}
		
		return $response;
	}
	public function getAllusers() {
		$response = new response ();
		$this->db->where ( 'role_id!=', 1 );
		$result = $this->db->get ( 'user' );
		if ($result->num_rows () > 0) {
			$response->setStatus ( true );
			$response->setResponseCode ( 1 );
			$response->setMsg ( "users" );
			$response->setObjArray ( $result->result_array () );
		} else {
			$response->setStatus ( false );
			$response->setResponseCode ( 0 );
			$response->setMsg ( "somethins going wrong" );
			$response->setObjArray ( $result->result_array () );
		}
		// echo '<pre>'; print_r($response); die();
		return $response;
		
		// return $res->result_array();
	}
	public function closeMeeting($data) {
		$response = new Response ();
		try {
			$this->db->set ( 'close_time', date ( 'Y-m-d H:i:s', now () ) );
			$this->db->where ( 'id', $data ['id'] );
			unset ( $data ['id'] );
			$result = $this->db->update ( 'meetings', $data );
			
			if ($result) {
				$response->setStatus ( true );
				$response->setResponseCode ( 1 );
				$response->setMsg ( "Meeting closed successfully" );
			} else {
				$response->setStatus ( false );
				$response->setResponseCode ( 0 );
				$response->setMsg ( "Error in meeting closing........." );
			}
		} catch ( Exception $e ) {
			$response->setStatus ( - 1 );
			$response->setMsg ( $e->getMessage () );
			$response->setError ( $e->getMessage () );
			log_message ( "Error", $e->getMessage () . " :: in file:" . $e->getFile () . ",at line:" . $e->getLine () );
		}
		return $response;
	}
}
?>