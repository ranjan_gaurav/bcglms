<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
require APPPATH . '/libraries/REST_Controller.php';

/**
 *
 * @author Anjani Kr. Gupta
 *         @date: 10th Nov 2016
 *         Description : This controller serve the request from mobile application
 */
class Api extends REST_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'api/Api_service' );
		include_once './application/objects/Response.php';
		$this->load->helper ( 'string' );
		$this->load->library ( array (
				'form_validation',
				'excel' 
		) );
		$this->load->model ( 'Api_service' );
		// error_reporting ( 0 );
	}
	public function index_get() {
		$x = "test data";
		$this->set_response ( array (
				'status' => '1',
				'message' => 'test',
				'jsonData' => $x 
		), REST_Controller::HTTP_OK );
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: login
	 *         Description: validate credentials
	 */
	public function login_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			
			$email = $this->post ( 'email' );
			$password = $this->post ( 'password' );
			$deviceType = $this->post ( 'deviceType' );
			$deviceId = $this->post ( 'deviceId' );
			$fcmRegId = $this->post ( 'fcmRegId' );
			$IosID = $this->post ( 'reg_id_ios' );
			
			try {
				
				$this->form_validation->set_rules ( 'email', 'email', 'trim|required' );
				$this->form_validation->set_rules ( 'password', 'password', 'trim|required' );
				$this->form_validation->set_rules ( 'deviceType', 'deviceType', 'trim|required' );
				$this->form_validation->set_rules ( 'deviceId', 'deviceId', 'trim|required' );
				// $this->form_validation->set_rules('fcmRegId', 'FCM regitration Id', 'trim|required');
				
				if ($this->form_validation->run () == TRUE) {
					$apiService = new Api_service ();
					$response = $apiService->login ( $email, $password, $deviceType, $deviceId, $fcmRegId, $IosID );
					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray () 
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray () 
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => false,
							'response_code' => 0,
							'message' => 'Requested data not found' 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function changeprofilepic_post() {
		$user_id = ( int ) $this->post ( 'user_id' );
		if (isset ( $user_id ) && $user_id != '' && $user_id != null) {
			$update = $this->Api_service->updateprofile ( $user_id );
			if ($update) {
				$message = [ 
						'status' => true,
						'response_code' => '1',
						'message' => 'Success',
						'user_pic' => $update 
				];
				
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			} else {
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => 'Not Updated' 
				];
				
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			}
		} else {
			$message = [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Invalid User' 
			];
			
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 12th Nov 2016
	 *         Method: addLeads
	 *         Description: add leads
	 */
	public function addLeads_post() {
		// error_reporting ( 0 );
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			$apiService = new Api_service ();
			$file = $apiService->picture (); // print_r($file); die();
			if ($file != '') {
				$filename = $file ['upload_data'] ['file_name'];
			}
			$data ['name'] = $this->post ( 'name' );
			$data ['mobile'] = $this->post ( 'mobile' );
			$data ['category_id'] = $this->post ( 'category_id' );
			$data ['product_id'] = $this->post ( 'product_id' );
			$data ['branch'] = $this->post ( 'branch' );
			$data ['district'] = $this->post ( 'district' );
			$data ['sourcedBy'] = $this->post ( 'sourcedBy' );
			$data ['address'] = $this->post ( 'address' );
			$data ['emailId'] = $this->post ( 'emailId' );
			$data ['target_month'] = $this->post ( 'month' );
			
			// print_r($data); die();
			
			$data ['isExistingCustomer'] = $this->post ( 'isExistingCustomer' );
			if ($data ['isExistingCustomer'] == 1) {
				$data ['cutomerId'] = $this->post ( 'cutomerId' );
				$data ['Note'] = $this->post ( 'note' );
			} else {
				$data ['cutomerId'] = '';
				$data ['Note'] = '';
			}
			
			$data ['businessCard'] = $filename;
			$data ['added_by'] = $this->post ( 'added_by' );
			
			$data ['status'] = 3;
			$data ['leadValue'] = $this->post ( 'lead_value' ); // print_r($data); die();
			$GetRole = $apiService->GetUserRole123 ( $data ['added_by'] );
			if ($GetRole != 5) {
				$data ['is_assigned'] = '0';
			}
			// print_r($data); die();
			try {
				
				$this->form_validation->set_rules ( 'name', 'name', 'trim|required' );
				$this->form_validation->set_rules ( 'mobile', 'mobile', 'trim|required' );
				$this->form_validation->set_rules ( 'category_id', 'category_id', 'trim|required' );
				$this->form_validation->set_rules ( 'product_id', 'product_id', 'trim|required' );
				$this->form_validation->set_rules ( 'branch', 'branch', 'trim|required' );
				$this->form_validation->set_rules ( 'district', 'district', 'trim|required' );
				$this->form_validation->set_rules ( 'sourcedBy', 'sourcedBy', 'trim|required' );
				$this->form_validation->set_rules ( 'address', 'address', 'trim|required' );
				$this->form_validation->set_rules ( 'emailId', 'emailId', 'trim|required' );
				$this->form_validation->set_rules ( 'isExistingCustomer', 'isExistingCustomer', 'trim|required' );
				// $this->form_validation->set_rules('cutomerId', 'Customer', 'trim|required');
				$this->form_validation->set_rules ( 'added_by', 'added_by', 'trim|required' );
				// $this->form_validation->set_rules('status', 'Status', 'trim|required');
				
				if ($this->form_validation->run () == TRUE) {
					// print_r($data); die();
					$response = $apiService->addLeads ( $data );
					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg () 
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg () 
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found' 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function Category_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$apiService = new Api_service ();
				$response = $apiService->getCategory ();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function product_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			$catId = $this->get ( 'catId' );
			
			try {
				
				if ($catId) {
					$apiService = new Api_service ();
					$response = $apiService->getProduct ( $catId );
					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray () 
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray () 
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found' 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function getAlldistrict_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			
			try {
				
				$apiService = new Api_service ();
				$response = $apiService->getCluster ();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function branch_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			$clusterId = $this->get ( 'district_id' );
			try {
				if ($clusterId) {
					$apiService = new Api_service ();
					$response = $apiService->getBranch ( $clusterId );
					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray () 
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray () 
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => 0,
							'message' => 'Requested data not found' 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: addMeeting
	 *         Description: add meetings
	 */
	public function addMeeting_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			$data ['title'] = $this->post ( 'title' );
			$data ['meetingTime'] = $this->post ( 'meetingTime' );
			$data ['description'] = $this->post ( 'description' );
			$data ['user_id'] = $this->post ( 'user_id' );
			$data ['status'] = $this->post ( 'status' );
			$data ['creationDate'] = $this->post ( 'creationDate' );
			$data ['lead_id'] = $this->post ( 'leadId' );
			// $data['close_time'] = $this->post('closingTime');
			
			try {
				
				$apiService = new Api_service ();
				$response = $apiService->addMeeting ( $data );
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: status
	 *         Description: get all status
	 */
	public function status_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$apiService = new Api_service ();
				$response = $apiService->getStatus ();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: changeStatus
	 *         Description: change status
	 */
	public function changeStatus_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			$data ['leadId'] = $this->post ( 'leadId' );
			$data ['statusId'] = $this->post ( 'statusId' );
			$data ['note'] = $this->post ( 'note' );
			$data ['changed_by'] = $this->post ( 'user_id' );
			try {
				$apiService = new Api_service ();
				$response = $apiService->changeStatus ( $data );
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 21th Nov 2016
	 *         Method: getAllMeeting
	 *         Description: see all the details of meeting
	 */
	public function getAllMeeting_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$data ['userId'] = $this->get ( 'user_id' );
				if ($data ['userId'] != '') {
					$apiService = new Api_service ();
					$response = $apiService->getAllMeeting ( $data );
					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray () 
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray () 
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => false,
							'response_code' => 0,
							'message' => 'Please Enter user ID' 
					), 
							// 'jsonData' => $response->getObjArray ()
							REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function closeMeeting_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			$data ['close_time'] = $this->post ( 'close_time' );
			$data ['Meeting_action_point'] = $this->post ( 'Note' );
			$data ['id'] = $this->post ( 'meeting_id' );
			$data ['status'] = 8;
			$data ['latitude'] = $this->post ( 'latitude' );
			$data ['longitude'] = $this->post ( 'longitude' );
			if ($data ['latitude'] != '' && $data ['longitude'] != '') {
				$data ['address'] = $this->GetAddress ( $data );
			}
			try {
				$apiService = new Api_service ();
				$response = $apiService->closeMeeting ( $data );
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function GetAddress($data) {
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim ( $data ['latitude'] ) . ',' . trim ( $data ['longitude'] ) . '&sensor=false';
		$json = @file_get_contents ( $url );
		$data = json_decode ( $json ); // print_r($data); die();
		$status = $data->status;
		if ($status == "OK")
			return $data->results [0]->formatted_address;
		else
			return false;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 22th Nov 2016
	 *         Method: getLeadsSummary
	 *         Description: see all the details of leads
	 */
	public function getLeadsSummary_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				
				$apiService = new Api_service ();
				$response = $apiService->getLeadsSummary ();
				// echo '<pre>';print_r(); die();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	
	/*
	 * Author : gaurav R
	 *
	 *
	 *
	 */
	public function getLeadsByuserID_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$data = $this->get ( 'user_id' );
				$apiService = new Api_service ();
				$response = $apiService->getLeadsByuserID ( $data );
				// echo '<pre>';print_r(); die();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => $response->getStatus (),
						'response_code' => $response->getResponseCode (),
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 22th Nov 2016
	 *         Method: getProcessedSummary
	 *         Description: see all the details of leads
	 */
	public function getProcessedSummary_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				
				$apiService = new Api_service ();
				$response = $apiService->getProcessedSummary ();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 23th Nov 2016
	 *         Method: updateLeadDetails
	 *         Description: modify leads details
	 */
	public function updateLeadDetails_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			$apiService = new Api_service ();
			$data = $this->post ();
			unset ( $data ['0'] );
			$id = $this->input->post ( 'id' );
			if ($id != '') {
				try {
					$response = $apiService->updateLeadDetails ( $data, $id );
					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg () 
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg () 
						), REST_Controller::HTTP_OK );
					}
				} catch ( Exception $e ) {
					$this->set_response ( array (
							'status' => 0,
							'message' => $e->getMessage () 
					), REST_Controller::HTTP_BAD_REQUEST );
				}
			} else {
				$this->set_response ( array (
						'status' => false,
						'response_code' => 0,
						'message' => 'Please enter lead id' 
				), REST_Controller::HTTP_OK );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'response_code' => $response->getResponseCode (),
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 24th Nov 2016
	 *         Method: updateMeetingDetails
	 *         Description: modify meeting the details
	 */
	public function updateMeetingDetails_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			$apiService = new Api_service ();
			$data = $this->post ();
			// print_r($data); die();
			unset ( $data ['0'] );
			$id = $this->input->post ( 'id' );
			try {
				$response = $apiService->updateMeetingDetails ( $data, $id );
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 24th Nov 2016
	 *         Method: updateUserDetails
	 *         Description: modify user details
	 */
	public function updateUserDetails_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			$apiService = new Api_service ();
			$data ['id'] = $this->post ( 'id' );
			$data ['firstname'] = $this->post ( 'firstname' );
			$data ['lastname'] = $this->post ( 'lastname' );
			$data ['gender'] = $this->post ( 'gender' );
			$data ['email'] = $this->post ( 'email' );
			$data ['dob'] = $this->post ( 'dob' );
			$data ['primaryContact'] = $this->post ( 'primaryContact' );
			try {
				$response = $apiService->updateUserDetails ( $data );
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 24th Nov 2016
	 *         Method: changePassword
	 *         Description: change password
	 */
	public function changePassword_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			$apiService = new Api_service ();
			$password = $this->post ( 'password' );
			$newPassword = $this->post ( 'newPassword' );
			$confirmPassword = $this->post ( 'confirmPassword' );
			$email = $this->post ( 'email' );
			try {
				$this->form_validation->set_rules ( 'password', 'password', 'trim|required' );
				$this->form_validation->set_rules ( 'newPassword', 'trim|required' );
				$this->form_validation->set_rules ( 'confirmPassword', 'trim|required|matches[newPassword]' );
				if ($this->form_validation->run () == TRUE) {
					$response = $apiService->changePassword ( $password, $email, $newPassword, $confirmPassword );
					
					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg () 
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg () 
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => false,
							'response_code' => 0,
							'message' => 'Requested data not found' 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => false,
						'response_code' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'response_code' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getCategorywiseUserLeads
	 *         Description: get all categorywise user individual leads
	 */
	public function getCategorywiseUserLeads_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$data ['userId'] = $this->get ( 'userId' );
				
				$apiService = new Api_service ();
				$response = $apiService->getCategorywiseUserLeads ( $data );
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getCategorywiseUserValues
	 *         Description: get all categorywise user individual values
	 */
	public function getCategorywiseUserValues_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$data ['userId'] = $this->get ( 'userId' );
				
				$apiService = new Api_service ();
				$response = $apiService->getCategorywiseUserValues ( $data );
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getProductwiseUserLeads
	 *         Description: get all productwise user individual leads
	 */
	public function getProductwiseUserLeads_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$data ['userId'] = $this->get ( 'userId' );
				
				$apiService = new Api_service ();
				$response = $apiService->getProductwiseUserLeads ( $data );
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getProductwiseUserValues
	 *         Description: get all productwise user individual leads
	 */
	public function getProductwiseUserValues_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$data ['userId'] = $this->get ( 'userId' );
				
				$apiService = new Api_service ();
				$response = $apiService->getProductwiseUserValues ( $data );
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getLeaderboardCatageorywiseLeads
	 *         Description: Leaderboard categorywise
	 */
	public function getLeaderboardCatageorywiseLeads_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$data ['catId'] = $this->get ( 'catId' );
				
				$apiService = new Api_service ();
				$response = $apiService->getLeaderboardCatageorywiseLeads ( $data );
				// echo'<pre>'; print_r($response); die();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 25th Nov 2016
	 *         Method: getLeaderboardCategorywiseValues
	 *         Description: Leaderboard productwise
	 */
	public function getLeaderboardCategorywiseValues_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$data ['catId'] = $this->get ( 'catId' );
				
				$apiService = new Api_service ();
				$response = $apiService->getLeaderboardCategorywiseValues ( $data );
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function deleteLead_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			try {
				
				$leadId = $this->input->post ( 'leadId' );
				$apiService = new Api_service ();
				$response = $apiService->deleteLead ( $leadId );
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function forgotpassword_post() {
		foreach ( (($this->post ())) as $key => $value ) {
			log_message ( 'info', 'data=' . $key . ' =>' . $value );
		}
		
		$data = array (
				'user_email' => $this->post ( 'user_email' ) 
		);
		$checkCredentails = $this->Api_service->CheckUserCredentials ( $data );
		if ($checkCredentails) {
			$create = $this->Api_service->validate_password ( $data );
			if ($create ['status']) {
				$status = REST_Controller::HTTP_OK;
				$this->set_response ( $create, $status );
			} else {
				
				$message = [ 
						'status' => false,
						'response_code' => '0',
						'message' => $create ['message'] 
				];
				$this->set_response ( $message, REST_Controller::HTTP_OK );
			}
		} else {
			$message = [ 
					'status' => false,
					'response_code' => '0',
					'message' => 'Sorry! Invalid Credentails.Email Not found!' 
			];
			$this->set_response ( $message, REST_Controller::HTTP_OK );
		}
	}
	public function getLeadByID_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$data = $this->get ( 'lead_id' );
				$apiService = new Api_service ();
				$response = $apiService->getLeadByID ( $data );
				// echo '<pre>';print_r(); die();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function transferLead_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			try {
				$data ['lead_id'] = $this->post ( 'leadId' );
				$data ['sender_id'] = $this->post ( 'sender_id' );
				$data ['receiver_id'] = $this->post ( 'receiver_id' );
				
				$apiService = new Api_service ();
				$response = $apiService->transferLead ( $data );
				// echo '<pre>';print_r(); die();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function getAllusers_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				// $data = $this->get ( 'lead_id' );
				$apiService = new Api_service ();
				$response = $apiService->getAllusers ();
				// echo '<pre>';print_r(); die();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function searchLeads_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$data = $this->get ();
				
				$apiService = new Api_service ();
				$response = $apiService->searchLeads ( $data );
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function getUserTree_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			
			$data = $this->get ();
			
			$apiService = new Api_service ();
			$userID = $this->get ( 'user_id' );
			if ($userID != '') {
				$GetRole = $this->Api_service->GetUserRole ( $userID ); // print_r($GetRole); die();
				                                                        
				// echo 5657;
				$GetList = $this->Api_service->getUserTree ( $userID ); // print_r($GetList); die();
				
				$arr = array ();
				foreach ( $GetList as $list ) {
					
					$x = $this->Api_service->getUserTree ( $list ['id'] ); // print_r($x);
					
					$role = $x [0] ['title']; // print_r($role);
					
					$list [$role] = $x;
					
					$z = array ();
					
					// print_r($list); //die();
					
					foreach ( $list [$role] as $val ) {
						
						// print_r($val); //die();
						
						$y = $this->Api_service->getUserTree ( $val ['id'] ); // print_r($y);
						
						$r = $y [0] ['title'];
						$val [$r] = $y;
						// print_r($val); //die();
						array_push ( $z, $val );
					}
					
					$list [$role] = $z;
					// array_push($list,$z);
					
					// print_r($); //die(); */
					
					array_push ( $arr, $list );
					
					// print_r($x);
					
					// $arr[] = $list;
					
					// array_push($arr , $list);
				}
				// print_r($arr);
				// print_r($list); //die();
				// $GetRole[$GetList[0]->] =
				$this->set_response ( array (
						'status' => true,
						'response_code' => 1,
						'role' => $GetRole [0]->title,
						'name' => $GetRole [0]->firstname . ' ' . $GetRole [0]->lastname,
						'user_id' => $GetRole [0]->id,
						$GetList [0] ['title'] => $arr 
				), REST_Controller::HTTP_OK );
			}
			/*
			 * $this->set_response ( array (
			 * 'status' => true,
			 * 'response_code' => 1,
			 * 'jsonData' => array (
			 * 'LeadsGenerated' => array (
			 * 'target' => $targets [0]->target,
			 * 'actual' => $getLeadsCount,
			 * 'percentage' => ( int ) $getLeadPercentage
			 * ),
			 *
			 * )
			 * ), REST_Controller::HTTP_OK );
			 */
		}
	}
	public function getAllRole_get() {
		$userID = $this->get ( 'user_id' );
		// $roleID = $this->get('role_id');
		if ($userID != '') {
			$GetRole = $this->Api_service->GetUserRole ( $userID, $roleID );
			
			$GetallRoles = $this->Api_service->GetOthersRole ( $userID, $roleID ); // print_r($GetallRoles); die();
			
			if ($GetallRoles) {
				
				$arr = array ();
				
				foreach ( $GetallRoles as $role ) {
					if ($role->id < $GetRole) {
						continue;
					}
					
					$role->data = $this->Api_service->GetUserdata ( $userID, $role->id );
					
					array_push ( $arr, $role );
				}
				$this->set_response ( array (
						'status' => true,
						'response_code' => 1,
						'JsonData' => $arr 
				), REST_Controller::HTTP_OK );
			}
		}
	}
	public function performance_post() {
		$months = $this->post ( 'month' );
		// $months = 4;
		$userID = $this->post ( 'user_id' );
		if ($months != '' && $userID) {
			$targets = $this->Api_service->getAlltargets ( $months, $userID ); // print_r($targets); die();
			
			$getLeadsCount = $this->Api_service->GetLeadsGeneratedCount ( $months, $userID ); // print_r($getLeadsCount); die();
			$getLeadPercentage = ($getLeadsCount->actual / $getLeadsCount->target) * 100; // print_r($getLeadPercentage); die();
			
			if ($getLeadPercentage > 100) {
				$getLeadPercentage = 100;
			}
			
			if ($getLeadsCount == '') {
				$getLeadsCount = array (
						'target' => 0,
						'actual' => 0,
						'percentage' => 0 
				);
			}
			
			// $getLeadsCount = new stdClass();
			$getLeadsCount->percentage = ( int ) $getLeadPercentage;
			
			$getMeetingCount = $this->Api_service->GetMeetingCount ( $months, $userID );
			$getMeetingPercentage = ($getMeetingCount / $targets [0]->target) * 100;
			
			if ($getMeetingPercentage > 100) {
				$getMeetingPercentage = 100;
			}
			
			if ($targets [0]->target == ' ' || $targets [0]->target == NULL) {
				$getMeetingCount = array (
						'target' => 0,
						'actual' => 0,
						'percentage' => 0 
				);
			} 

			else {
				$getMeetingCount = array (
						'target' => $targets [0]->target,
						'actual' => $getMeetingCount,
						'percentage' => $getMeetingPercentage 
				);
			}
			
			$getleadsValue = $this->Api_service->GetTotalLeadsValue ( $months, $userID ); // print_r($getleadsValue); die();
			$getleadvaluePercentage = ($getleadsValue->actual / $getleadsValue->target) * 100;
			
			if ($getleadvaluePercentage > 100) {
				$getleadvaluePercentage = 100;
			}
			
			if ($getleadsValue == '') {
				$getleadsValue = array (
						'target' => 0,
						'actual' => 0,
						'percentage' => 0 
				);
			}
			
			$getleadsValue->percentage = ( int ) $getleadvaluePercentage;
			
			$getleadsConvertedCount = $this->Api_service->GetLeadsConvertedCount ( $months, $userID ); // print_r($getleadsConvertedCount); die();
			$getleadconvertedPercentage = ($getleadsConvertedCount->actual / $getleadsConvertedCount->target) * 100;
			
			if ($getleadconvertedPercentage > 100) {
				$getleadconvertedPercentage = 100;
			}
			
			if ($getleadsConvertedCount == '') {
				$getleadsConvertedCount = array (
						'target' => 0,
						'actual' => 0,
						'percentage' => 0 
				);
			}
			
			$getleadsConvertedCount->percentage = ( int ) $getleadconvertedPercentage;
			
			$getTotalCount = $this->Api_service->GetTotalCount ( $months, $userID );
			
			$performnace = $this->Api_service->GetPerformance ( $months, $userID );
			
			$upcomingMeeting = $this->Api_service->GetUpcomingMeeting ( $userID );
			
			$this->set_response ( array (
					'status' => true,
					'response_code' => 1,
					'jsonData' => array (
							'LeadsGenerated' => $getLeadsCount,
							'MeetingConducted' => $getMeetingCount,
							'BusinessGenerated' => $getleadsValue,
							'LeadsConverted' => $getleadsConvertedCount,
							
							'TotalPoint' => 10,
							'TotalCount' => $getTotalCount,
							
							'performance' => $performnace,
							'UpcomingMeeting' => $upcomingMeeting 
					) 
			), REST_Controller::HTTP_OK );
		} 

		else {
			$this->set_response ( array (
					'status' => false,
					'response_code' => 0,
					'message' => 'Please Enter userid and month' 
			), REST_Controller::HTTP_OK );
		}
	}
	public function updateMeetingDetails123_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			$apiService = new Api_service ();
			$data = $this->post ();
			
			unset ( $data ['0'] );
			$id = $this->input->post ( 'id' );
			try {
				
				$data = $this->get ();
				
				if ($id) {
					$response = $apiService->updateMeetingDetails123 ( $data, $id );
					
					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray () 
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg (),
								'jsonData' => $response->getObjArray () 
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => false,
							'response_code' => 0,
							'message' => 'provide relevant data' 
					), REST_Controller::HTTP_BAD_REQUEST );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		}
	}
	public function UnassignedLeads_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$data = $this->get ( 'user_id' );
				$apiService = new Api_service ();
				$response = $apiService->UnassignedList ( $data );
				// echo '<pre>';print_r(); die();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function ListofImmediateJuniors_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				$data = $this->get ( 'user_id' );
				$apiService = new Api_service ();
				$response = $apiService->ListofImmediateJuniors ( $data );
				// echo '<pre>';print_r(); die();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function assignLeads_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			$apiService = new Api_service ();
			$data = $this->post ();
			try {
				
				// $data = $this->get();
				
				if ($data) {
					$response = $apiService->assignLeads ( $data );
					
					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg () 
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode () 
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => false,
							'response_code' => 0,
							'message' => 'provide relevant data' 
					), REST_Controller::HTTP_BAD_REQUEST );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		}
	}
	public function setTarget_post() {
		if ($_SERVER ['REQUEST_METHOD'] == "POST") {
			$apiService = new Api_service ();
			$data = $this->post ();
			try {
				
				// $data = $this->get();
				
				if ($data) {
					$response = $apiService->setTarget ( $data );
					
					if ($response->getStatus ()) {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode (),
								'message' => $response->getMsg () 
						), REST_Controller::HTTP_OK );
					} else {
						$this->set_response ( array (
								'status' => $response->getStatus (),
								'response_code' => $response->getResponseCode () 
						), REST_Controller::HTTP_OK );
					}
				} else {
					$this->set_response ( array (
							'status' => false,
							'response_code' => 0,
							'message' => 'provide relevant data' 
					), REST_Controller::HTTP_BAD_REQUEST );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		}
	}
	public function getJuniorsList_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			
			$data = $this->get ();
			
			$apiService = new Api_service ();
			$userID = $this->get ( 'user_id' );
			$type = $this->get ( 'type' );
			if ($userID != '') {
				
				if ($type == 0) {
					$GetRole = $this->Api_service->GetUserRole ( $userID ); // print_r($GetRole); die();
					
					$this->set_response ( array (
							'status' => true,
							'response_code' => 1,
							'jsonData' => array (
									'role' => $GetRole [0]->title,
									'name' => $GetRole [0]->firstname . ' ' . $GetRole [0]->lastname,
									'user_id' => $GetRole [0]->id 
							) 
					), 
							// $GetList [0] ['title'] => $GetList
							// $x [0] ['title'] => $x
							REST_Controller::HTTP_OK );
				}
				
				if ($type == 1) {
					
					$GetRole = $this->Api_service->GetUserRole ( $userID );
					$GetList = $this->Api_service->getJuniorsList ( $userID ); // print_r($GetList); die();
					
					$this->set_response ( array (
							'status' => true,
							'response_code' => 1,
							'jsonData' => array (
									'role' => $GetRole [0]->title,
									'name' => $GetRole [0]->firstname . ' ' . $GetRole [0]->lastname,
									'user_id' => $GetRole [0]->id,
									'users' => $GetList 
							) 
					), 
							// $x [0] ['title'] => $x
							REST_Controller::HTTP_OK );
					
					// die();
				}
				
				if ($type == 2) {
					
					$GetRole = $this->Api_service->GetUserRole ( $userID );
					$GetList2 = $this->Api_service->getJuniorsList ( $userID ); // print_r($GetList); die();
					
					$arr = array ();
					foreach ( $GetList2 as $List ) {
						
						// print_r($List['id']);
						
						$x = $this->Api_service->getJuniorsList ( $List ['id'] ); // print_r($x);
						$arr [] = $x [0];
					}
					// print_r($arr[0]); die();
					$this->set_response ( array (
							'status' => true,
							'response_code' => 1,
							'jsonData' => array (
									'role' => $GetRole [0]->title,
									'name' => $GetRole [0]->firstname . ' ' . $GetRole [0]->lastname,
									'user_id' => $GetRole [0]->id,
									'users' => $arr 
							) 
					), 
							// $x [0] ['title'] => $x
							REST_Controller::HTTP_OK );
					
					// die();
				}
				
				if ($type == 3) {
					
					$data = $this->get (); // print_r($data); die();
					$userID = $this->get ( 'user_id' );
					$GetRole = $this->Api_service->GetUserRole ( $data ['user_id'] );
					$GetList3 = $this->Api_service->getJuniorsList ( $userID ); // print_r($GetList3); die();
					
					$arr = array ();
					foreach ( $GetList3 as $List2 ) {
						
						// print_r($List['id']);
						
						$x = $this->Api_service->getJuniorsList ( $List2 ['id'] ); // print_r($x);
						$arr [] = $x [0];
					}
					
					// print_r($arr); die();
					
					$val = '';
					foreach ( $arr as $key =>$row2 ) {
						if (empty ( $row2 )) {
							continue;
						}
						
					//print_r ( $row2 ); // die();
						$y[$key] = $this->Api_service->getJuniorsList ( $row2 ['id'] ); //print_r($y[$key]);
						
						
						array_push($y[$key] ,$y[$key][1]);
						
						//print_r($y[1]); 
						
						
						$val[] = $y[$key] ;
						
						//array_push();
					}
					
					//$val3 = array_merge($val[0],$val[1]);
				//	print_r($val); die();
					// print_r($val); die();
					$this->set_response ( array (
							'status' => true,
							'response_code' => 1,
							'jsonData' => array (
									'role' => $GetRole [0]->title,
									'name' => $GetRole [0]->firstname . ' ' . $GetRole [0]->lastname,
									'user_id' => $GetRole [0]->id,
									'users' => $val[0]
							) 
					), 
							// $x [0] ['title'] => $x
							REST_Controller::HTTP_OK );
					
					// die();
				}
			}
		}
	}
	public function getAllProducts_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			try {
				// $data = $this->get ( 'user_id' );
				$apiService = new Api_service ();
				$response = $apiService->getAllProducts ();
				// echo '<pre>';print_r(); die();
				if ($response->getStatus ()) {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				} else {
					$this->set_response ( array (
							'status' => $response->getStatus (),
							'response_code' => $response->getResponseCode (),
							'message' => $response->getMsg (),
							'jsonData' => $response->getObjArray () 
					), REST_Controller::HTTP_OK );
				}
			} catch ( Exception $e ) {
				$this->set_response ( array (
						'status' => 0,
						'message' => $e->getMessage () 
				), REST_Controller::HTTP_BAD_REQUEST );
			}
		} else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
	public function performanceFilter_get() {
		if ($_SERVER ['REQUEST_METHOD'] == "GET") {
			
			$data ['product_id'] = $this->get ( 'product_id' );
			$data ['start_date'] = $this->get ( 'start_date' );
			$data ['end_date'] = $this->get ( 'end_date' );
			$data ['user_id'] = $this->get ( 'user_id' );
			
			$apiService = new Api_service ();
			$targetsSum = $apiService->sumOfTargets ( $data );
			print_r ( $targetsSum ); // die();
			$response = $apiService->performanceFilter ( $data );
			// print_r ( $response ); die ();
			// $getLeadsCount = $this->Api_service->GetLeadsGeneratedCount ( $months, $userID ); // print_r($getLeadsCount); die();
			$getLeadPercentage = ($response [0]->NewLeadCount / $targetsSum [0]->target) * 100; // print_r($getLeadPercentage); die();
			
			if ($getLeadPercentage > 100) {
				$getLeadPercentage = 100;
			}
			
			if ($response [0]->NewLeadCount == '' || $response [0]->NewLeadCount == null) {
				$getLeadsCount = array (
						'target' => 0,
						'actual' => 0,
						'percentage' => 0 
				);
			} 

			else {
				$getLeadsCount = array (
						'target' => $targetsSum [0]->target,
						'actual' => $response [0]->NewLeadCount,
						'percentage' => 0 
				);
			}
			
			// $getLeadsCount = new stdClass();
			$getLeadsCount->percentage = ( int ) $getLeadPercentage;
			
			$getMeetingCount = $this->Api_service->GetMeetingCount ( $months, $userID );
			$getMeetingPercentage = ($getMeetingCount / $targets [0]->target) * 100;
			
			if ($getMeetingPercentage > 100) {
				$getMeetingPercentage = 100;
			}
			
			if ($targets [0]->target == ' ' || $targets [0]->target == NULL) {
				$getMeetingCount = array (
						'target' => 0,
						'actual' => 0,
						'percentage' => 0 
				);
			} 

			else {
				$getMeetingCount = array (
						'target' => $targets [0]->target,
						'actual' => $getMeetingCount,
						'percentage' => $getMeetingPercentage 
				);
			}
			
			$getleadsValue = $this->Api_service->GetTotalLeadsValue ( $months, $userID ); // print_r($getleadsValue); die();
			$getleadvaluePercentage = ($getleadsValue->actual / $getleadsValue->target) * 100;
			
			if ($getleadvaluePercentage > 100) {
				$getleadvaluePercentage = 100;
			}
			
			if ($getleadsValue == '') {
				$getleadsValue = array (
						'target' => 0,
						'actual' => 0,
						'percentage' => 0 
				);
			}
			
			$getleadsValue->percentage = ( int ) $getleadvaluePercentage;
			
			$getleadsConvertedCount = $this->Api_service->GetLeadsConvertedCount ( $months, $userID ); // print_r($getleadsConvertedCount); die();
			$getleadconvertedPercentage = ($getleadsConvertedCount->actual / $getleadsConvertedCount->target) * 100;
			
			if ($getleadconvertedPercentage > 100) {
				$getleadconvertedPercentage = 100;
			}
			
			if ($getleadsConvertedCount == '') {
				$getleadsConvertedCount = array (
						'target' => 0,
						'actual' => 0,
						'percentage' => 0 
				);
			}
			
			$getleadsConvertedCount->percentage = ( int ) $getleadconvertedPercentage;
			
			$getTotalCount = $this->Api_service->GetTotalCount ( $months, $userID );
			
			$performnace = $this->Api_service->GetPerformance ( $months, $userID );
			
			$upcomingMeeting = $this->Api_service->GetUpcomingMeeting ( $userID );
			
			$this->set_response ( array (
					'status' => true,
					'response_code' => 1,
					'jsonData' => array (
							'LeadsGenerated' => $getLeadsCount,
							'MeetingConducted' => $getMeetingCount,
							'BusinessGenerated' => $getleadsValue,
							'LeadsConverted' => $getleadsConvertedCount,
							
							'TotalPoint' => 10,
							'TotalCount' => $getTotalCount,
							
							'performance' => $performnace,
							'UpcomingMeeting' => $upcomingMeeting 
					) 
			), REST_Controller::HTTP_OK );
		} 

		else {
			$this->set_response ( array (
					'status' => 0,
					'message' => 'This HTTP method is not allowed' 
			), REST_Controller::HTTP_METHOD_NOT_ALLOWED );
		}
	}
}
